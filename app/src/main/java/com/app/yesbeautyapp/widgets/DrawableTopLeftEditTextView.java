package com.app.yesbeautyapp.widgets;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;

/**
 * Created by Codeslay-03 on 1/5/2018.
 */

public class DrawableTopLeftEditTextView extends BitmapDrawable {

    public DrawableTopLeftEditTextView(Resources res, Bitmap bitmap) {
        super(res, bitmap);
    }

    @Override
    public void draw(Canvas canvas) {
        int halfCanvas = canvas.getHeight() / 2;
        int halfDrawable = getIntrinsicHeight() / 2;
        canvas.save();
        canvas.translate(0, (-halfCanvas + halfDrawable) + 55);
        super.draw(canvas);
        canvas.restore();
    }
}
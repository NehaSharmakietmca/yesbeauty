package com.app.yesbeautyapp.dao;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class AplusoneSQLiteHelper extends SQLiteOpenHelper implements com.app.yesbeautyapp.constants.DbConstants {

    public static final String TAG = "SQ";

    public AplusoneSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        try {


            try {
                dropTable(database, TABLE_CART_DETAILS);
                database.execSQL(CREATE_TABLE_CART_DETAILS);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    private void dropTable(SQLiteDatabase db, String tableName) {
        String query;
        try {
            query = "DROP TABLE IF EXISTS " + tableName;
            db.execSQL(query);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    // Table creation sql statement
    private static final String CREATE_TABLE_CART_DETAILS = "CREATE TABLE IF NOT EXISTS " + TABLE_CART_DETAILS + "(" +
            "id                     INTEGER PRIMARY KEY ASC, " +
            "serviceId              INTEGER, " +
            "serviceName            TEXT, " +
            "serviceImage           TEXT, " +
            "serviceTypeLabel       TEXT, " +
            "serviceDate            TEXT, " +
            "serviceTime            TEXT, " +
            "quantity               TEXT, " +
            "phoneNumber            TEXT, " +
            "emailId                TEXT, " +
            "address                TEXT, " +
            "comments               TEXT," +
            "totalAmount            TEXT)";

}

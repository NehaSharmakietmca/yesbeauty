package com.app.yesbeautyapp.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import com.app.yesbeautyapp.constants.DbConstants;
import com.app.yesbeautyapp.model.CartModel;

/**
 * Created by Codeslay-03 on 6/13/2017.
 */

public class CartDataSource implements DbConstants {

    protected AplusoneSQLiteHelper dbHelper;

    protected Context context;
    public static final String TAG = "CartDataSource";

    public CartDataSource(Context context) {
        dbHelper = new AplusoneSQLiteHelper(context);
        dbHelper.getWritableDatabase();
    }

    public boolean addServiceInToCart(CartModel cartModel) {
        if (cartModel == null) return false;
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();

            values.put(COLUMN_SERVICE_ID, cartModel.getServiceId());
            values.put(COLUMN_SERVICE_NAME, cartModel.getServiceName());
            values.put(COLUMN_SERVICE_IMAGE, cartModel.getServiceImage());
            values.put(COLUMN_SERVICE_TYPE_LABEL, cartModel.getServiceTypeLabel());
            values.put(COLUMN_SERVICE_DATE, cartModel.getServiceDate());
            values.put(COLUMN_SERVICE_TIME, cartModel.getServiceTime());
            values.put(COLUMN_QUANTITY, cartModel.getQuantity());
            values.put(COLUMN_PHONE_NUMBER, cartModel.getPhoneNumber());
            values.put(COLUMN_EMAIL_ID, cartModel.getEmailId());
            values.put(COLUMN_ADDRESS, cartModel.getAddress());
            values.put(COLUMN_COMMENTS, cartModel.getComments());
            values.put(COLUMN_TOTAL_AMT, cartModel.getTotalAmount());

            database.insert(TABLE_CART_DETAILS, null, values);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            database.close();
        }
        return true;
    }

    public ArrayList<CartModel> getCartDetailList() {

        ArrayList<CartModel> cartModelArrayList = new ArrayList<>();
        SQLiteDatabase database = null;
        String selectQueryQues = "SELECT id, serviceId, serviceName, serviceImage, serviceTypeLabel, serviceDate, serviceTime, " +
                "phoneNumber, emailId, address, comments,totalAmount, quantity FROM " + TABLE_CART_DETAILS;

        try {
            database = dbHelper.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQueryQues, null);
            if (cursor.moveToFirst()) {
                do {
                    CartModel cartModel = new CartModel();
                    cartModel.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                    cartModel.setServiceId(cursor.getString(cursor.getColumnIndex(COLUMN_SERVICE_ID)));
                    cartModel.setServiceName(cursor.getString(cursor.getColumnIndex(COLUMN_SERVICE_NAME)));
                    cartModel.setServiceImage(cursor.getString(cursor.getColumnIndex(COLUMN_SERVICE_IMAGE)));
                    cartModel.setServiceTypeLabel(cursor.getString(cursor.getColumnIndex(COLUMN_SERVICE_TYPE_LABEL)));
                    cartModel.setServiceDate(cursor.getString(cursor.getColumnIndex(COLUMN_SERVICE_DATE)));
                    cartModel.setServiceTime(cursor.getString(cursor.getColumnIndex(COLUMN_SERVICE_TIME)));
                    cartModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(COLUMN_PHONE_NUMBER)));
                    cartModel.setEmailId(cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL_ID)));
                    cartModel.setAddress(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
                    cartModel.setComments(cursor.getString(cursor.getColumnIndex(COLUMN_COMMENTS)));
                    cartModel.setTotalAmount(cursor.getString(cursor.getColumnIndex(COLUMN_TOTAL_AMT)));
                    cartModel.setQuantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
                    cartModelArrayList.add(cartModel);

                } while (cursor.moveToNext());
            } else {
                return cartModelArrayList;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            database.close();
        }
        return cartModelArrayList;
    }

    public boolean clearCartItems() {
        SQLiteDatabase database = null;
        try {
            database = dbHelper.getWritableDatabase();

            database.delete(TABLE_CART_DETAILS, null, null);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            database.close();
        }
        return true;
    }
    public boolean deleteCart() {
        SQLiteDatabase database = null;
        try {
            database = dbHelper.getWritableDatabase();



            database.delete(TABLE_CART_DETAILS,null,null);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            database.close();
        }
        return true;
    }
    public boolean deleteCartItems(CartModel cartModel) {
        if (cartModel == null) return false;
        SQLiteDatabase database = null;
        try {
            database = dbHelper.getWritableDatabase();

            String where = "id=?";
            String[] whereArgs = new String[]{cartModel.getId() + ""};

            database.delete(TABLE_CART_DETAILS, where, whereArgs);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            database.close();
        }
        return true;
    }

  public boolean updateCartDetails(CartModel cartModel) {
        if (cartModel == null) return false;
        SQLiteDatabase database = null;
        try {
            database = dbHelper.getWritableDatabase();

            String where = "serviceId=?";
            String[] whereArgs = new String[]{cartModel.getServiceId() + ""};
            ContentValues values = new ContentValues();

            values.put(COLUMN_SERVICE_ID, cartModel.getServiceId());
            values.put(COLUMN_SERVICE_NAME, cartModel.getServiceName());
            values.put(COLUMN_SERVICE_IMAGE, cartModel.getServiceImage());
            values.put(COLUMN_SERVICE_TYPE_LABEL, cartModel.getServiceTypeLabel());
            values.put(COLUMN_SERVICE_DATE, cartModel.getServiceDate());
            values.put(COLUMN_SERVICE_TIME, cartModel.getServiceTime());
            values.put(COLUMN_PHONE_NUMBER, cartModel.getPhoneNumber());
            values.put(COLUMN_EMAIL_ID, cartModel.getEmailId());
            values.put(COLUMN_ADDRESS, cartModel.getAddress());
            values.put(COLUMN_COMMENTS, cartModel.getComments());
            values.put(COLUMN_TOTAL_AMT, cartModel.getTotalAmount());

            database.update(TABLE_CART_DETAILS, values, where, whereArgs);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            database.close();
        }
        return true;
    }
  /*
    public boolean isServiceInCart(CartModel cartModel) {
        boolean result = false;
        if (cartModel == null) return result;
        SQLiteDatabase database = null;
        String selectQueryQues = "SELECT serviceId FROM " + TABLE_CART_DETAILS + " where serviceId = " + cartModel.getServiceId();
        try {
            database = dbHelper.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQueryQues, null);
            if (cursor.moveToFirst()) {
                result = true;
            } else {
                result = false;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            result = false;
        } finally {
            database.close();
        }
        return result;
    }*/

}

package com.app.yesbeautyapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.content.Context.WIFI_SERVICE;


/**
 * Created by admin on 30-11-2016.
 */

public class DeviceUtils {

    public static String getDeviceUUID(Context context) {
        String deviceUUID = "";

        try {
            deviceUUID = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return deviceUUID;
    }

    public static String getDeviceIpAddress(Context context) {
        String deviceIp = "";
        int state = checkNetworkStatus(context);
        if (state == 1) {
            deviceIp = getWifiIPAddress(context);
        } else if (state == 2) {
            deviceIp = getMobileIPAddress();
        }
        return deviceIp;
    }

    public static String getDeviceKey() {
        String deviceKey = "ca2c91980b497c222abb71d469ab87eb";
        return deviceKey;
    }

    private static String getWifiIPAddress(Context context) {
        String wiFiIp = "";
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();
            wiFiIp = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return wiFiIp;
    }

    private static String getMobileIPAddress() {
        String mobileIp = "";
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        mobileIp = addr.getHostAddress();
                        return mobileIp;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mobileIp;
    }

    private static int checkNetworkStatus(final Context context) {

        int networkStatus = 0;
        // Get connect mangaer
        final ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //Check Wifi
        final android.net.NetworkInfo wifi = manager.getActiveNetworkInfo();
        //Check for mobile data
        final android.net.NetworkInfo mobile = manager.getActiveNetworkInfo();
        if (wifi.getType() == ConnectivityManager.TYPE_WIFI) {
            networkStatus = 1;
        } else if (mobile.getType() == ConnectivityManager.TYPE_MOBILE) {
            networkStatus = 2;
        } else {
            networkStatus = 3;
        }
        return networkStatus;
    }

}

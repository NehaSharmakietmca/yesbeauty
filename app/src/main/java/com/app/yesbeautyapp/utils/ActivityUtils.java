package com.app.yesbeautyapp.utils;

import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;

import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.activity.CartDetailsActivity;
import com.app.yesbeautyapp.activity.ReviewOrderDetailsActivity;
import com.app.yesbeautyapp.model.CartModel;

import static com.app.yesbeautyapp.constants.AppConstants.ANIMATION_SLIDE_UP;
import static com.app.yesbeautyapp.constants.AppConstants.CART_MODEL_OBJ_ARRAY;
import static com.app.yesbeautyapp.constants.AppConstants.REQUEST_TAG_NO_RESULT;


/**
 * Created by Ravi on 2/13/2018.
 */

public class ActivityUtils {

    public static void toOpenBookNowActivity(Activity activity, CartModel cartModel) {
        Bundle bundle = new Bundle();
        ArrayList<CartModel> cartModelArrayList = new ArrayList<>();
        cartModelArrayList.add(cartModel);
        bundle.putBoolean("isBookNow", true);
        bundle.putParcelableArrayList(CART_MODEL_OBJ_ARRAY, cartModelArrayList);
        ((BaseActivity) activity).startActivity(activity, ReviewOrderDetailsActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
    }

    public static void toOpenCartDetailActivity(Activity activity) {
        Bundle bundle = new Bundle();
        ((BaseActivity) activity).startActivity(activity, CartDetailsActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
    }
}

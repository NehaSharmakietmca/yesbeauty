package com.app.yesbeautyapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.activity.BookingHistoryDetailActivity;
import com.app.yesbeautyapp.adapter.BookingAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingHistoryFragment extends BaseFragment implements RecyclerClickListner {
    private RecyclerView bookingHistoryRecyclerView;
    private BookingAdapter bookingHistoryAdapter;
    private List<BookingModel> bookingModelList = new ArrayList<>();
    private View view;

    public BookingHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public void alertOkClicked() {

    }

    @Override
    protected void initViews() {
        bookingHistoryRecyclerView = view.findViewById(R.id.recyler_booking_history);
        bookingHistoryRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        bookingHistoryRecyclerView.setLayoutManager(mLayoutManager);
        bookingHistoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
     //   getBookingHistoryList();
        bookingHistoryAdapter = new BookingAdapter(currentActivity, bookingModelList);
        bookingHistoryRecyclerView.setAdapter(bookingHistoryAdapter);
        bookingHistoryAdapter.setOnItemClickListner(this);

    }

    @Override
    protected void initContext() {
        currentActivity = getActivity();
        context = getActivity();
    }

    @Override
    protected void initListners() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_order_history, container, false);
        return view;
    }

    @Override
    public void onClick(View v) {
        bookingHistoryAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {

            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

    }
    private void getBookingHistoryList()  {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject bookingHistory = new JSONObject();
        try {
            bookingHistory.put("user_id", SharedPreferenceUtils.getInstance(getContext()).getString(AppConstants.USER_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_BOOKING_HISTORY, bookingHistory, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successfull fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        Gson gson = new Gson();
                        List<BookingModel> bookingModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(MESSAGE).toString(), BookingModel[].class));
                        bookingModelList.clear();
                        bookingModelList.addAll(bookingModelTempList);
                        bookingHistoryAdapter.notifyDataSetChanged();

                    } else {
                        cancelProgressDialog();
                        ((BaseActivity) currentActivity).logTesting("fetch Service error", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onItemClick(int position, View v) {
        BookingModel bookingModel = bookingModelList.get(position);
        Intent intent = new Intent(getContext(), BookingHistoryDetailActivity.class);
        intent.putExtra(MODEL_OBJ,bookingModel);
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(int position, View v) {

    }

    @Override
    public void onResume() {
        super.onResume();
        //getBookingHistoryList();
    }
}

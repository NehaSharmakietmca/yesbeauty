package com.app.yesbeautyapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.activity.Dashboard;
import com.app.yesbeautyapp.activity.ProductDetailActivity;
import com.app.yesbeautyapp.activity.SubServiceActivity;
import com.app.yesbeautyapp.adapter.ProductAdapter;
import com.app.yesbeautyapp.adapter.SubServiceAdapter;
import com.app.yesbeautyapp.adapter.SubSubServiceAdapter;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.ProductAddedToCartListener;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.model.SubServiceModel;
import com.app.yesbeautyapp.model.SubSubServiceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubServiceFragment extends BaseFragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SERVICE_MODEL = "service_model";
    private int sectionNumber;
    private SubServiceModel serviceModel;
    private RecyclerView recyclerProductList;
    private RecyclerView recyclerSubSubService;
    private ProductAdapter productAdapter;
    private SubSubServiceAdapter subSubServiceAdapter;
    private List<ProductModel> productModelList = new ArrayList<>();
    private List<SubSubServiceModel> subSubServiceModels = new ArrayList<>();
    private ProductAddedToCartListener cartListener;


    public SubServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void alertOkClicked() {

    }

    @Override
    protected void initViews() {
        sectionNumber = getArguments() != null ? getArguments().getInt(ARG_SECTION_NUMBER) : 1;
        serviceModel = getArguments() != null ? (SubServiceModel) getArguments().getParcelable(ARG_SERVICE_MODEL) : null;
        if (serviceModel.getProductModels() != null) {
            productModelList.addAll(serviceModel.getProductModels());
            subSubServiceModels.addAll(serviceModel.getSubSubServiceModels());
        }
        recyclerProductList = view.findViewById(R.id.recyclerProduct);
        recyclerSubSubService = view.findViewById(R.id.recyclerSubSubService);
        setServiceRecyclerAdapter();
        setProductRecyclerAdapter();
    }

    @Override
    protected void initContext() {
        currentActivity = getActivity();
        context = getActivity();
    }

    @Override
    protected void initListners() {
        productAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                switch (v.getId()) {
                    case R.id.addToCartButton: {

                        Button addTOCartBtn = (Button) v;
                        if (addTOCartBtn.getText().toString().equalsIgnoreCase(AppConstants.ADD_TO_CART_BUTTON)) {
                            addTOCartBtn.setText(AppConstants.ADDED);
                        } else if (addTOCartBtn.getText().toString().equalsIgnoreCase(AppConstants.ADDED)) {
                            addTOCartBtn.setText(AppConstants.ADD_TO_CART_BUTTON);
                        }

                        ProductModel productModel = productModelList.get(position);
                        if (cartListener != null) {
                            cartListener.productAddedToCart(productModel);
                        }

                    }
                    break;
                    case R.id.lin_product: {
                        ProductModel productModel = productModelList.get(position);
                        bundle = new Bundle();
                        bundle.putParcelable(MODEL_OBJ, productModel);
                        CartProductModel.getInstance().setProductModelArrayList(((SubServiceActivity) currentActivity).productModelArrayList);
                        ((BaseActivity) currentActivity).startActivity(currentActivity, ProductDetailActivity.class, bundle, true, REQUEST_TAG_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_UP);
                    }
                    break;
                }
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

        subSubServiceAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                if (subSubServiceModels != null && subSubServiceModels.size() > position) {

                    for (int i = 0; i < subSubServiceModels.size(); i++) {
                        SubSubServiceModel subSubServiceModel = subSubServiceModels.get(i);
                        if (subSubServiceModel != null) {
                            subSubServiceModel.setSelected(false);
                        }
                    }

                    SubSubServiceModel subSubServiceModel = subSubServiceModels.get(position);
                    if (subSubServiceModel != null) {
                        subSubServiceModel.setSelected(true);
                        productModelList.clear();
                        productModelList.addAll(subSubServiceModel.getProductModels());
                        productAdapter.notifyDataSetChanged();
                    }
                    subSubServiceAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sub, container, false);
        return view;
    }

    private void setServiceRecyclerAdapter() {
        if (subSubServiceModels != null && subSubServiceModels.size() > 0) {
            SubSubServiceModel subSubServiceModel = subSubServiceModels.get(0);
            if (subSubServiceModel != null) {
                subSubServiceModel.setSelected(true);
                productModelList.clear();
                productModelList.addAll(serviceModel.getProductModels());
            }
        }else {
            recyclerSubSubService.setVisibility(View.GONE);
        }
        subSubServiceAdapter = new SubSubServiceAdapter(currentActivity, subSubServiceModels);
        recyclerSubSubService.setAdapter(subSubServiceAdapter);
    }

    private void setProductRecyclerAdapter() {
        productAdapter = new ProductAdapter(currentActivity, productModelList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(currentActivity);
        recyclerProductList.setLayoutManager(linearLayoutManager);
        recyclerProductList.setAdapter(productAdapter);
    }

    public static SubServiceFragment newInstance(int sectionNumber, SubServiceModel serviceModel) {
        SubServiceFragment fragment = new SubServiceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putParcelable(ARG_SERVICE_MODEL, serviceModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof ProductAddedToCartListener) {
            cartListener = (ProductAddedToCartListener) getActivity();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        productAdapter.notifyDataSetChanged();
    }
}

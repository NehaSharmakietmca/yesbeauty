package com.app.yesbeautyapp.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.activity.Dashboard;
import com.app.yesbeautyapp.activity.NotificationActivity;
import com.app.yesbeautyapp.activity.SearchServiceActivity;
import com.app.yesbeautyapp.adapter.HomeViewPagerAdapter;
import com.app.yesbeautyapp.adapter.TestimonialAdapter;
import com.app.yesbeautyapp.interfaces.ProductAddedToCartListener;
import com.app.yesbeautyapp.model.BannerImageModel;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.model.SubServiceModel;
import com.app.yesbeautyapp.model.TestimonialModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.LoginUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.activity.ServiceDetailsActivity;
import com.app.yesbeautyapp.activity.SubServiceActivity;
import com.app.yesbeautyapp.adapter.ServiceAdapter;
import com.app.yesbeautyapp.adapter.SlidingImageAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ServiceModel;
import com.app.yesbeautyapp.utils.Validator;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class MainBoard extends BaseFragment {
    View view;
    private EditText searchETV;
    private ImageView imgNotification;
    private TextView testimonialTV;
    private CardView testimonialCV;
    private RecyclerView recyclerService;
    private ServiceAdapter serviceAdapter;
    private List<ServiceModel> serviceModelList = new ArrayList<>();
    private List<TestimonialModel> testimonialModelList = new ArrayList<>();
    private List<BannerImageModel> bannerImageModelList = new ArrayList<>();
    private ViewPager mPager;
    private LinearLayout bannerLL;
    private CirclePageIndicator indicator;
    private MenuItem mSearch;
    private SlidingImageAdapter slidingImageAdapter;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private RecyclerView testmonialRecyclerView;
    private HomeViewPagerAdapter viewPagerAdapter;

    private FusedLocationProviderClient mFusedLocationClient;
    protected static long MIN_UPDATE_INTERVAL = 30 * 1000;
    LocationRequest locationRequest;
    Location currentLocation = null;
    private TestimonialAdapter testimonialAdapter;

    private Dialog offersAndDiscountDialog = null;
    private AppCompatEditText dateOfbirthEditText;
    private AppCompatEditText dateOfAnniversaryEditText;
    private String dateOfBirth;
    private String anniverDate;
    private Dialog ratingDialog;
    private RatingBar ratingBar;
    private TabLayout productTabLayout;
    private ViewPager productPager;
    ArrayList<SubServiceModel> subServiceModelArrayList = new ArrayList<>();
    int serviceClickedPos;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_board, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void alertOkClicked() {
        ServiceModel serviceModel = serviceModelList.get(serviceClickedPos);
        getSubService(serviceModel);
        ((Dashboard) currentActivity).productModelArrayList.clear();
        ((Dashboard) currentActivity).setCount(context, String.valueOf(0));
    }

    @Override
    protected void initViews() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(currentActivity);
        imgNotification = view.findViewById(R.id.imgNotification);
        searchETV = view.findViewById(R.id.searchETV);
        testimonialCV = view.findViewById(R.id.testimonialCV);
        testimonialTV = view.findViewById(R.id.testimonialTV);
        recyclerService = view.findViewById(R.id.recyclerService);
        testmonialRecyclerView = view.findViewById(R.id.recycler_testmonials);
        mPager = view.findViewById(R.id.viewPager);
        bannerLL = view.findViewById(R.id.bannerLL);
        indicator = view.findViewById(R.id.circleIndicator);
        productTabLayout = view.findViewById(R.id.product_tabs);
        productPager = view.findViewById(R.id.product_pager);

        FontUtils.changeFont(context, testimonialTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        setServiceRecyclerAdapter();
        setTestimonialRecyclerView();
        if (Validator.isNetworkAvailable(currentActivity)) {
            getBannerList();
            getServiceList();
            getTestimonialList();
            getHomeSubService();
        } else {
            alert(currentActivity, getString(R.string.alert_message_no_network), getString(R.string.alert_message_no_network), getString(R.string.labelOk), getString(R.string.labelCancel), false, false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
        if (TextUtils.isEmpty(address)) {
            address = "Locating...";
        }
        ((BaseActivity) currentActivity).settingSubTitle(address);
        checkForLocationRequest();
        checkForLocationSettings();
    }

    @Override
    protected void initContext() {
        currentActivity = getActivity();
        context = getActivity();
    }

    @Override
    protected void initListners() {
        searchETV.setOnClickListener(this);
        imgNotification.setOnClickListener(this);
        serviceAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                serviceClickedPos = position;
                if (!((Dashboard) currentActivity).productModelArrayList.isEmpty()) {
                    alert(currentActivity, "Cart will be cleared", "Going next will clear your cart and you will have to start again.", getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true);
                    return;
                }
                ServiceModel serviceModel = serviceModelList.get(position);
                getSubService(serviceModel);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgNotification: {
                ((BaseActivity) currentActivity).startActivity(currentActivity, NotificationActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_LEFT);
                break;
            }
            case R.id.searchETV: {
                ((BaseActivity) currentActivity).startActivity(currentActivity, SearchServiceActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_LEFT);
                break;
            }
        }
    }

    private void startLocationPermissionRequest(int requestCode) {
        ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    private void requestPermissions(final int requestCode) {
        startLocationPermissionRequest(requestCode);
    }

    public void checkForLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    //Check for location settings.
    public void checkForLocationSettings() {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(currentActivity);

            settingsClient.checkLocationSettings(builder.build())
                    .addOnSuccessListener(currentActivity, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            callCurrentLocation();
                        }
                    })
                    .addOnFailureListener(currentActivity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {


                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(currentActivity, 99);
                                    } catch (IntentSender.SendIntentException sie) {
                                        sie.printStackTrace();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    Toast.makeText(currentActivity, "Setting change is not available.Try in another device.", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callCurrentLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(RECEIVE_LOCATION_PERMISSION_REQUEST);
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    currentLocation = locationResult.getLastLocation();
                    //toast(context, "Your Location : " + getCompleteAddressString());
                    String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
                    if (TextUtils.isEmpty(address)) {
                        address = getCompleteAddressString();
                    }
                    ((BaseActivity) currentActivity).settingSubTitle(address);
                }
            }, Looper.myLooper());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getCompleteAddressString() {
        String strAdd = "";
        if (currentLocation == null) return strAdd;
        Geocoder geocoder = new Geocoder(currentActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress == null) return strAdd;
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    if (!TextUtils.isEmpty(returnedAddress.getAddressLine(i)))
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void setSliderImageAdapter() {
        slidingImageAdapter = new SlidingImageAdapter(currentActivity, bannerImageModelList);
        mPager.setAdapter(slidingImageAdapter);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5.5f * density);

        NUM_PAGES = bannerImageModelList.size();
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

    }

    private void setServiceRecyclerAdapter() {
        serviceAdapter = new ServiceAdapter(currentActivity, serviceModelList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(currentActivity, 2);
        recyclerService.setLayoutManager(gridLayoutManager);
        recyclerService.setAdapter(serviceAdapter);
    }

    private void setTestimonialRecyclerView() {
        testimonialAdapter = new TestimonialAdapter(currentActivity, testimonialModelList);
        testmonialRecyclerView.setAdapter(testimonialAdapter);
    }

    public void getServiceList() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_GET_ALL_SERVICE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                cancelProgressDialog();
                try {
                    Gson gson = new Gson();
                    List<ServiceModel> serviceModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), ServiceModel[].class));
                    serviceModelList.clear();
                    serviceModelList.addAll(serviceModelTempList);
                    serviceAdapter.updateAdapter(serviceModelList);
                    boolean isDialogShow = SharedPreferenceUtils.getInstance(context).getBoolean("isDisplayAnvDilog");
                    if (!isDialogShow && LoginUtils.isLogin(context)) {
                        showOfferAndDiscountDialog();
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }


    private void getSubService(final ServiceModel serviceModel) {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;

        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.PARENT_ID, serviceModel.getId());
            Log.e("jsonGetAllService", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_SUB_SERVICE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                cancelProgressDialog();
                try {
                    Gson gson = new Gson();
                    List<SubServiceModel> serviceModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), SubServiceModel[].class));
                    if (serviceModelTempList != null && serviceModelTempList.size() > 0) {
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        subServiceModelArrayList = new ArrayList<>(serviceModelTempList);
                        bundle.putParcelable(MODEL_OBJ, serviceModel);
                        bundle.putParcelableArrayList(MODEL_OBJ_ARRAY, subServiceModelArrayList);
                        ((BaseActivity) currentActivity).startActivity(currentActivity, SubServiceActivity.class, bundle, true, REQUEST_TAG_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_UP);
                    } else {
                        toast(context, "No Service Available.");
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    public void getBannerList() {
        JSONObject jsons = null;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_GET_BANNERIMG, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    Gson gson = new Gson();
                    List<BannerImageModel> bannerImageModels = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), BannerImageModel[].class));
                    if (bannerImageModels != null && !bannerImageModels.isEmpty()) {
                        bannerImageModelList.clear();
                        bannerImageModelList.addAll(bannerImageModels);
                        bannerLL.setVisibility(View.VISIBLE);
                        setSliderImageAdapter();
                    } else {
                        bannerLL.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    public void getTestimonialList() {
        JSONObject jsons = null;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_GET_TESTIMONIAL, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    Gson gson = new Gson();
                    List<TestimonialModel> testimonialModels = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), TestimonialModel[].class));
                    testimonialModelList.clear();
                    testimonialModelList.addAll(testimonialModels);
                    testimonialAdapter.notifyDataSetChanged();
                    if (testimonialModelList.isEmpty()) {
                        testimonialCV.setVisibility(View.GONE);
                    } else {
                        testimonialCV.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }


    private void toOpenServiceDetailActivity(ServiceModel serviceModel) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putParcelable(MODEL_OBJ, serviceModel);
        ((BaseActivity) currentActivity).startActivity(currentActivity, ServiceDetailsActivity.class, bundle, true, REQUEST_TAG_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_UP);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        mSearch = menu.findItem(R.id.action_search);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void showOfferAndDiscountDialog() {
        final Calendar dateOfBirthCalender = Calendar.getInstance();
        final Calendar anniversaryCalender = Calendar.getInstance();
        offersAndDiscountDialog = new Dialog(context);
        offersAndDiscountDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.dialog_special_offer_discount, null);
        dateOfbirthEditText = v.findViewById(R.id.et_date_of_birth);
        dateOfAnniversaryEditText = v.findViewById(R.id.et_annivarsary);
        final AppCompatButton skipButton = v.findViewById(R.id.btn_skip);
        final AppCompatButton submitButton = v.findViewById(R.id.btn_submit);
        dateOfAnniversaryEditText = v.findViewById(R.id.et_annivarsary);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                dateOfBirthCalender.set(Calendar.YEAR, year);
                dateOfBirthCalender.set(Calendar.MONTH, monthOfYear);
                dateOfBirthCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateOfbirthLabel(dateOfBirthCalender);
            }

        };
        dateOfbirthEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, dateOfBirthCalender
                        .get(Calendar.YEAR), dateOfBirthCalender.get(Calendar.MONTH),
                        dateOfBirthCalender.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        final DatePickerDialog.OnDateSetListener anniversaryDate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                anniversaryCalender.set(Calendar.YEAR, year);
                anniversaryCalender.set(Calendar.MONTH, monthOfYear);
                anniversaryCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateAnniversaryLabel(anniversaryCalender);
            }

        };
        dateOfAnniversaryEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, anniversaryDate, anniversaryCalender
                        .get(Calendar.YEAR), anniversaryCalender.get(Calendar.MONTH),
                        anniversaryCalender.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        offersAndDiscountDialog.setCancelable(false);
        offersAndDiscountDialog.setCanceledOnTouchOutside(false);
        offersAndDiscountDialog.setContentView(v);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceUtils.getInstance(context).putBoolean("isDisplayAnvDilog", true);
                offersAndDiscountDialog.dismiss();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(dateOfbirthEditText.getText().toString())) {
                    Toast.makeText(context, "Please select Date of Birth", Toast.LENGTH_LONG).show();
                    return;
                } else if (TextUtils.isEmpty(dateOfAnniversaryEditText.getText().toString())) {
                    Toast.makeText(context, "Please select Anniversary", Toast.LENGTH_LONG).show();
                    return;
                }

                dateOfBirth = dateOfbirthEditText.getText().toString();
                anniverDate = dateOfAnniversaryEditText.getText().toString();
                saveUserBirthAnyvversory();
            }
        });

        WindowManager m = currentActivity.getWindowManager();
        Display d = m.getDefaultDisplay();
        Window dialogWindow = offersAndDiscountDialog.getWindow();// Access to the screen width, height
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // Gets the current values of the Parameters dialog box
        //p.height = (int) (d.getHeight() * 0.75); // Height is set to screen 0.6
        p.width = (int) (d.getWidth() * 0.9); // Width is set to screen 0.65
        dialogWindow.setAttributes(p);
        offersAndDiscountDialog.show();
    }

    private void updateDateOfbirthLabel(Calendar calendar) {
        String myFormat = "dd-MMMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dateOfbirthEditText.setText(sdf.format(calendar.getTime()));
    }

    private void updateAnniversaryLabel(Calendar calendar) {
        String myFormat = "dd-MMMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dateOfAnniversaryEditText.setText(sdf.format(calendar.getTime()));
    }

    public void saveUserBirthAnyvversory() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            jsons.put("date_birth", dateOfBirth);
            jsons.put("date_anniversary", anniverDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_BIRTHANIV, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                toast(context, "Your Information Updated Sucessfully.");
                                if (offersAndDiscountDialog != null && offersAndDiscountDialog.isShowing())
                                    offersAndDiscountDialog.dismiss();
                                SharedPreferenceUtils.getInstance(context).putBoolean("isDisplayAnvDilog", true);
                            } else {
                                toast(context, "Failed to Updated Your Information.");
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    private void showRatingDialog() {
        ratingDialog = new Dialog(context);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.dialog_rating, null);
        TextView ratingHeading = v.findViewById(R.id.text_heading_rate);
        TextView contentRating = v.findViewById(R.id.text_content_rating);
        ratingBar = v.findViewById(R.id.rating_bar);
        final Button submitButton = v.findViewById(R.id.btn_submit);
        ratingDialog.setCancelable(true);
        ratingDialog.setCanceledOnTouchOutside(false);
        FontUtils.changeFont(context, ratingHeading, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, submitButton, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, contentRating, FONT_OPEN_SANS_REGULAR_TTF);
        ratingDialog.setContentView(v);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rating = String.valueOf(ratingBar.getRating());
                if (TextUtils.isEmpty(rating)) {
                    Toast.makeText(getContext(), rating, Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });

        WindowManager m = currentActivity.getWindowManager();
        Display d = m.getDefaultDisplay();
        Window dialogWindow = ratingDialog.getWindow();// Access to the screen width, height
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // Gets the current values of the Parameters dialog box
        p.height = (int) (d.getHeight() * 0.75); // Height is set to screen 0.6
        p.width = (int) (d.getWidth() * 0.9); // Width is set to screen 0.65
        dialogWindow.setAttributes(p);
        ratingDialog.show();
    }

    private void getHomeSubService() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;

        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.PARENT_ID, 1);
            Log.e("jsonGetAllService", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_SUB_SERVICE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                cancelProgressDialog();
                try {
                    Gson gson = new Gson();
                    List<SubServiceModel> serviceModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), SubServiceModel[].class));
                    if (serviceModelTempList != null && serviceModelTempList.size() > 0) {
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        subServiceModelArrayList = new ArrayList<>(serviceModelTempList);
                        setProductTabLayout();
                    } else {
                        toast(context, "No Service Available.");
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    private void setProductTabLayout() {
        ArrayList<SubServiceModel> localSubServiceModel = new ArrayList<>();
        if (subServiceModelArrayList != null && !subServiceModelArrayList.isEmpty())
            if (getActivity() != null) {
                int count = subServiceModelArrayList.size();
                if (count > 3) {
                    count = 3;
                }
                for (int i = 0; i < count; i++) {
                    SubServiceModel subSubServiceModel = subServiceModelArrayList.get(i);
                    localSubServiceModel.add(subSubServiceModel);
                }
                viewPagerAdapter = new HomeViewPagerAdapter(getActivity().getSupportFragmentManager(), localSubServiceModel);
                productPager.setAdapter(viewPagerAdapter);
                productTabLayout.setupWithViewPager(productPager);
            }

    }
}

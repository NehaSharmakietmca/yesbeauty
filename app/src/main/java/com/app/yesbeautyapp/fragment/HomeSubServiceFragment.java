package com.app.yesbeautyapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.activity.ProductDetailActivity;
import com.app.yesbeautyapp.activity.SubServiceActivity;
import com.app.yesbeautyapp.adapter.HomeProductListAdapter;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.ProductAddedToCartListener;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.model.SubServiceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeSubServiceFragment extends BaseFragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SERVICE_MODEL = "service_model";
    private int sectionNumber;
    private SubServiceModel serviceModel;
    private RecyclerView recyclerProductList;
    private HomeProductListAdapter homeProductListAdapter;
    private List<ProductModel> productModelList = new ArrayList<>();
    private ProductAddedToCartListener cartListener;


    public HomeSubServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void alertOkClicked() {

    }

    @Override
    protected void initViews() {
        sectionNumber = getArguments() != null ? getArguments().getInt(ARG_SECTION_NUMBER) : 1;
        serviceModel = getArguments() != null ? (SubServiceModel) getArguments().getParcelable(ARG_SERVICE_MODEL) : null;
        if (serviceModel.getProductModels() != null) {
            productModelList.addAll(serviceModel.getProductModels());
        }
        recyclerProductList = view.findViewById(R.id.recyclerProduct);
        setProductRecyclerAdapter();
    }

    @Override
    protected void initContext() {
        currentActivity = getActivity();
        context = getActivity();
    }

    @Override
    protected void initListners() {
        homeProductListAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                switch (v.getId()) {
                    case R.id.addToCartButton: {

                        Button addTOCartBtn = (Button) v;
                        if (addTOCartBtn.getText().toString().equalsIgnoreCase(AppConstants.ADD_TO_CART_BUTTON)) {
                            addTOCartBtn.setText(AppConstants.ADDED);
                        } else if (addTOCartBtn.getText().toString().equalsIgnoreCase(AppConstants.ADDED)) {
                            addTOCartBtn.setText(AppConstants.ADD_TO_CART_BUTTON);
                        }

                        ProductModel productModel = productModelList.get(position);
                        if (cartListener != null) {
                            cartListener.productAddedToCart(productModel);
                        }

                    }
                    break;
                    case R.id.lin_product: {
                        ProductModel productModel = productModelList.get(position);
                        bundle = new Bundle();
                        bundle.putParcelable(MODEL_OBJ, productModel);
                        CartProductModel.getInstance().setProductModelArrayList(((SubServiceActivity) currentActivity).productModelArrayList);
                        ((BaseActivity) currentActivity).startActivity(currentActivity, ProductDetailActivity.class, bundle, true, REQUEST_TAG_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_UP);
                    }
                    break;
                }
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home_sub_service, container, false);
        return view;
    }

    private void setProductRecyclerAdapter() {
        homeProductListAdapter = new HomeProductListAdapter(currentActivity, productModelList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(currentActivity, LinearLayoutManager.HORIZONTAL, false);
        recyclerProductList.setLayoutManager(linearLayoutManager);
        recyclerProductList.setAdapter(homeProductListAdapter);
    }

    public static HomeSubServiceFragment newInstance(int sectionNumber, SubServiceModel serviceModel) {
        HomeSubServiceFragment fragment = new HomeSubServiceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putParcelable(ARG_SERVICE_MODEL, serviceModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof ProductAddedToCartListener) {
            cartListener = (ProductAddedToCartListener) getActivity();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        homeProductListAdapter.notifyDataSetChanged();
    }
}

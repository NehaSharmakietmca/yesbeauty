package com.app.yesbeautyapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.AppointmentHistoryDetailActivity;
import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.activity.BookingHistoryDetailActivity;
import com.app.yesbeautyapp.activity.MyAppointmentsActivity;
import com.app.yesbeautyapp.adapter.AppointmentHistoryAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.model.OrderModel;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppointmentHistoryFragment extends BaseFragment {
    private View view;
    private RecyclerView orderRecyclerView;
    private ArrayList<OrderModel> orderModelArrayList = new ArrayList<>();
    private AppointmentHistoryAdapter appointmentHistoryAdapter;


    public AppointmentHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public void alertOkClicked() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void initViews() {
        setOrderRecyclerView();

    }

    @Override
    public void onResume() {
        super.onResume();
        getBookingHistoryList();
    }

    @Override
    protected void initContext() {
        context = getActivity();
        currentActivity = getActivity();
    }

    @Override
    protected void initListners() {
        appointmentHistoryAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                OrderModel orderModel = orderModelArrayList.get(position);
                if (bundle == null) {
                    bundle = new Bundle();
                }
                bundle.putParcelable(AppConstants.MODEL_OBJ, orderModel);
                ((BaseActivity) currentActivity).startActivity(currentActivity, AppointmentHistoryDetailActivity.class, bundle, true, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_LEFT);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_appointment_history, container, false);
        orderRecyclerView = view.findViewById(R.id.recycler_appointment_history);
        return view;
    }


    private void getBookingHistoryList() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject bookingHistory = new JSONObject();
        try {
            bookingHistory.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_BOOKING_HISTORY, bookingHistory, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                cancelProgressDialog();
                try {
                    Gson gson = new Gson();
                    List<OrderModel> bookingModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), OrderModel[].class));
                    orderModelArrayList.clear();
                    orderModelArrayList.addAll(bookingModelTempList);
                    appointmentHistoryAdapter.notifyDataSetChanged();
                    if (orderModelArrayList.size() == 0) {
                        orderRecyclerView.setVisibility(View.GONE);
                    }else {
                        orderRecyclerView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onClick(View v) {

    }

    private void setOrderRecyclerView() {
        appointmentHistoryAdapter = new AppointmentHistoryAdapter(context, orderModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        orderRecyclerView.setLayoutManager(mLayoutManager);
        orderRecyclerView.setItemAnimator(new DefaultItemAnimator());
        orderRecyclerView.setAdapter(appointmentHistoryAdapter);
    }
}

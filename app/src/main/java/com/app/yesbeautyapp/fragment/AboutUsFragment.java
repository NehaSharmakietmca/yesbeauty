package com.app.yesbeautyapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.utils.FontUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends BaseFragment {

    private TextView lblWhoTV;
    private TextView lblWhatTV;
    private TextView descWhoTV;
    private TextView descWhatTV;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return view = inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void alertOkClicked() {

    }

    @Override
    protected void initViews() {
        lblWhoTV = (TextView) view.findViewById(R.id.lblWhoTV);
        lblWhatTV = (TextView) view.findViewById(R.id.lblWhatTV);
        descWhoTV = (TextView) view.findViewById(R.id.descWhoTV);
        descWhatTV = (TextView) view.findViewById(R.id.descWhatTV);

        FontUtils.changeFont(context, lblWhoTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, lblWhatTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, descWhoTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, descWhatTV, FONT_OPEN_SANS_REGULAR_TTF);
    }

    @Override
    protected void initContext() {
        context = getActivity();
        currentActivity = getActivity();
    }

    @Override
    protected void initListners() {

    }

    @Override
    public void onClick(View v) {

    }

    /*public void onStartTransaction() {
        PaytmPGService Service = PaytmPGService.getStagingService();
        Map<String, String> paramMap = new HashMap<String, String>();

        paramMap.put("MID", "WorldP64425807474247");
        paramMap.put("ORDER_ID", "TestMerchant000111007");
        paramMap.put("CUST_ID", "mohit.aggarwal@paytm.com");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", "1");
        paramMap.put("WEBSITE", "http://vyaandairy.com");
        paramMap.put("EMAIL", "araj100003@gmail.com");
        paramMap.put("MOBILE_NO", "8010722646");
        PaytmOrder Order = new PaytmOrder(paramMap);

        PaytmMerchant Merchant = new PaytmMerchant(
                "https://pguat.paytm.com/paytmchecksum/paytmCheckSumGenerator.jsp",
                "https://pguat.paytm.com/paytmchecksum/paytmCheckSumVerify.jsp");

        Service.initialize(Order, Merchant, null);
        try {
            Service.startPaymentTransaction(context, true, true,
                    new PaytmPaymentTransactionCallback() {
                        @Override
                        public void someUIErrorOccurred(String inErrorMessage) {

                        }

                        @Override
                        public void onTransactionSuccess(Bundle inResponse) {
                            Log.d("LOG", "Payment Transaction is successful " + inResponse);
                            Toast.makeText(context, "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onTransactionFailure(String inErrorMessage,
                                                         Bundle inResponse) {
                            Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                            Toast.makeText(context, "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void networkNotAvailable() {

                        }

                        @Override
                        public void clientAuthenticationFailed(String inErrorMessage) {

                        }

                        @Override
                        public void onErrorLoadingWebPage(int iniErrorCode,
                                                          String inErrorMessage, String inFailingUrl) {

                        }

                        @Override
                        public void onBackPressedCancelTransaction() {

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

}

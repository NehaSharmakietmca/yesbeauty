package com.app.yesbeautyapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yesbeautyapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyPolicyFragment extends BaseFragment {


    public PrivacyPolicyFragment() {
        // Required empty public constructor
    }


    @Override
    public void alertOkClicked() {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initContext() {

    }

    @Override
    protected void initListners() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false);
    }

    @Override
    public void onClick(View v) {

    }
}

package com.app.yesbeautyapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.yesbeautyapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingReasonOtherIssueFragment extends Fragment {


    public BookingReasonOtherIssueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booking_reason_other_issue, container, false);
    }

}

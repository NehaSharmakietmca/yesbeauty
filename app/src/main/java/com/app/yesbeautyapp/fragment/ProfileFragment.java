package com.app.yesbeautyapp.fragment;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.BaseActivity;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.tracking.LocationTracker;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {
    private View view;
    private EditText nameEditText;
    private EditText emailEditText;
    private EditText mobileNumberEditText;
    private EditText addressEditText;
    private Button saveButton;
    private ImageView gpsImageView;
    private LocationTracker locationTracker;
    private int bytesAvailable;
    private int bytesRead;
    private String mimeType;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void alertOkClicked() {

    }

    @Override
    protected void initViews() {
        locationTracker = new LocationTracker(context);
        nameEditText = (EditText)view.findViewById(R.id.nameEditText);
        gpsImageView = (ImageView) view.findViewById(R.id.image_gps);
        emailEditText = (EditText)view.findViewById(R.id.emailIdEditText);
        mobileNumberEditText = (EditText)view.findViewById(R.id.edtMobileNumber);
        addressEditText = (EditText)view.findViewById(R.id.editTextAddress);
        saveButton = (Button)view.findViewById(R.id.saveButton);
        FontUtils.changeFont(context, nameEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, emailEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, mobileNumberEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, addressEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, saveButton, FONT_OPEN_SANS_REGULAR_TTF);
        getUserDetails();

    }

    @Override
    protected void initContext() {
        currentActivity = getActivity();
        context = getActivity();
    }

    @Override
    protected void initListners() {
        saveButton.setOnClickListener(this);
        gpsImageView.setOnClickListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_gps:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!checkLocationPermission()) {
                        requestPermission();
                    } else {
                        if(locationTracker.canGetLocation()){
                            getAddress(locationTracker.getLatitude(),locationTracker.getLongitude());
                        }else{
                            locationTracker.showSettingsAlert();
                        }
                    }
                } else {
                    if(locationTracker.canGetLocation()){
                        getAddress(locationTracker.getLatitude(),locationTracker.getLongitude());
                    }else{
                        locationTracker.showSettingsAlert();
                    }
                }
                break;
            case R.id.saveButton:
                if(isMandatoryFields()) {
                    saveUserDetails();
                }
                break;
        }

    }

    public void saveUserDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.KEY_ID,Integer.parseInt(SharedPreferenceUtils.getInstance(getContext()).getString(USER_ID)));
            jsons.put(AppConstants.KEY_EMAIL,emailEditText.getText().toString().trim());
            jsons.put(AppConstants.keyUserNumber,mobileNumberEditText.getText().toString().trim());
           // jsons.put(AppConstants.USER_IMAGE,"");
            jsons.put(AppConstants.USER_ADDRESS,addressEditText.getText().toString().trim());

            Log.e("jsonGetAllService", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_PROFILE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successfull fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        Gson gson = new Gson();

                        //  setSliderImageAdapter();

                    } else {
                        cancelProgressDialog();
                        ((BaseActivity) currentActivity).logTesting("fetch Service error", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }
    private void getUserDetails(){
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.KEY_ID,Integer.parseInt(SharedPreferenceUtils.getInstance(getContext()).getString(USER_ID)));
            Log.e("GetUserDetails", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_PROFILE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successfull fetch User", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        JSONObject messageObj = response.getJSONObject(RESPONCE_MESSAGE);
                      String id =  messageObj.getString(KEY_ID);
                      String name = messageObj.getString(keyName);
                      String phone = messageObj.getString(keyUserNumber);
                      String email = messageObj.getString(KEY_EMAIL);
                      String userProfileImage = messageObj.getString(USER_PROFILE_IMAGE);
                      String address = messageObj.getString(USER_ADDRESS);
                      mobileNumberEditText.setText(phone);
                      nameEditText.setText(name);
                      emailEditText.setText(email);
                      addressEditText.setText(address);

                    } else {
                        cancelProgressDialog();
                        ((BaseActivity) currentActivity).logTesting("fetch Service User", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }
    private boolean checkLocationPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, RECEIVE_LOCATION_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RECEIVE_LOCATION_PERMISSION_REQUEST:
                break;
        }
    }
    public  void getAddress( double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("Address", strReturnedAddress.toString());
            } else {
                Log.w("Address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Address", "Canont get Address!");
        }
        addressEditText.setText(strAdd);
        return ;
    }

    private boolean isMandatoryFields() {
        nameEditText.setError(null);
        emailEditText.setError(null);
        mobileNumberEditText.setError(null);
        addressEditText.setError(null);
        if (nameEditText.getText().toString().isEmpty()) {
            nameEditText.setError(getResources().getString(R.string.error_name_empty));
            nameEditText.requestFocus();
            return false;
        } else if (!Validator.getInstance().validatePhoneNumber(context, mobileNumberEditText.getText().toString()).equals("")) {
            String numberError = Validator.getInstance().validatePhoneNumber(context, mobileNumberEditText.getText().toString());
            mobileNumberEditText.setError(numberError);
            mobileNumberEditText.requestFocus();
            return false;

        } else if (!Validator.getInstance().isValidEmail(context, emailEditText.getText().toString()).equals("")) {
            String emailError = Validator.getInstance().isValidEmail(context, emailEditText.getText().toString());
            emailEditText.setError(emailError);
            emailEditText.requestFocus();
            return false;
        } else if (addressEditText.getText().toString().isEmpty()) {
            addressEditText.setError(getResources().getString(R.string.edit_text_address));
            addressEditText.requestFocus();
            return false;
        }
        return true;
    }
}

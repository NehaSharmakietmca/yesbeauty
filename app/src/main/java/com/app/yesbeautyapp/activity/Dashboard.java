package com.app.yesbeautyapp.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.yesbeautyapp.interfaces.ProductAddedToCartListener;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.squareup.picasso.Picasso;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.SideMenuListAdapter;
import com.app.yesbeautyapp.dao.CartDataSource;
import com.app.yesbeautyapp.fragment.BaseFragment;
import com.app.yesbeautyapp.fragment.BookingHistoryFragment;
import com.app.yesbeautyapp.fragment.ContactUsFragment;
import com.app.yesbeautyapp.fragment.MainBoard;
import com.app.yesbeautyapp.fragment.PrivacyPolicyFragment;
import com.app.yesbeautyapp.fragment.TermsAndServiceFragment;
import com.app.yesbeautyapp.utils.ActivityUtils;
import com.app.yesbeautyapp.utils.CountDrawable;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.LoginUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.widgets.CircularImageView;

import java.util.ArrayList;
import java.util.Iterator;


public class Dashboard extends BaseActivity implements ProductAddedToCartListener {
    DrawerLayout drawerLayout;
    LinearLayout fragmentContainerWithToolbar;
    FrameLayout fragmentContainer;
    TextView textUserName;
    TextView textMobileNumber;
    RecyclerView recyclerNavigationDrawer;
    ActionBarDrawerToggle actionBarDrawerToggle;
    boolean isExitable;
    BaseFragment frag;
    LinearLayoutManager managerSideMenu;
    int fragmentType = 0;
    LinearLayout containerImage;
    private SideMenuListAdapter sideMenuListAdapter;
    private boolean isUserLogin;
    private CircularImageView circleImageDrawerProfile;
    private Menu menu;
    private CartDataSource cartDataSource;
    public ArrayList<ProductModel> productModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);
        isUserLogin();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void initViews() {
        cartDataSource = new CartDataSource(context);
        toolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(toolbar);
        applyFontForToolbarTitle();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        fragmentContainerWithToolbar = (LinearLayout) findViewById(R.id.fragmentContainerWithToolbar);
        fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
        textUserName = (TextView) findViewById(R.id.textUserName);
        textMobileNumber = (TextView) findViewById(R.id.textMobileNumber);
        recyclerNavigationDrawer = (RecyclerView) findViewById(R.id.recyclerNavigationDrawer);
        containerImage = (LinearLayout) findViewById(R.id.containerImage);
        containerImage.setOnClickListener(this);
        circleImageDrawerProfile = (CircularImageView) findViewById(R.id.circleImageDrawerProfile);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.app_name, R.string.app_name) {

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                drawerLayout.openDrawer(Gravity.LEFT);
                return true;
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                toHideKeyboard();

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                if (newState == DrawerLayout.STATE_DRAGGING) {
                    toHideKeyboard();

                }
            }

        };
        actionBarDrawerToggle.syncState();
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        });
        sideMenuListAdapter = new SideMenuListAdapter(currentActivity);
        managerSideMenu = new LinearLayoutManager(currentActivity);

        recyclerNavigationDrawer.setLayoutManager(managerSideMenu);
        recyclerNavigationDrawer.setAdapter(sideMenuListAdapter);
        setSelection(fragmentType);
        FontUtils.changeFont(this, textUserName, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, textMobileNumber, FONT_OPEN_SANS_SEMIBOLD_TTF);

    }

    public void applyFontForToolbarTitle() {
        try {
            for (int i = 0; i < toolbar.getChildCount(); i++) {
                View view = toolbar.getChildAt(i);
                if (view instanceof TextView) {
                    TextView titleTV = (TextView) view;
                    if (titleTV.getText().equals(getTitle())) {
                        titleTV.setTextSize(14);
                        FontUtils.changeFont(this, titleTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
                        //break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initContext() {
        currentActivity = Dashboard.this;
        context = Dashboard.this;
    }

    @Override
    protected void initListners() {

    }

    @Override
    protected boolean isActionBar() {
        return false;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }


    @Override
    public void onAlertClicked(int alertType) {
        if (alertType == ALERT_TYPE_LOGOUT) {
            logout();
        }
    }


    public void logout() {
        if (cartDataSource != null)
            cartDataSource.clearCartItems();
        SharedPreferenceUtils.getInstance(context).clearALl();
        startActivity(currentActivity, LoginActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
        finish();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.containerImage: {
                setSelection(PROFILE);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        displayProfile();
        sideMenuListAdapter.notifyDataSetChanged();
        setCount(Dashboard.this, String.valueOf(productModelArrayList.size()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cart:
                if (productModelArrayList.isEmpty()) {
                    alert(currentActivity, "Alert", "Your cart is empty, Please add services in cart and proceed booking.", getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), false, true, ALERT_TYPE_NO_NETWORK);
                } else {
                    bundle.putString("title", "Beauty");
                    CartProductModel.getInstance().setProductModelArrayList(productModelArrayList);
                    startActivity(currentActivity, CartDetailsActivity.class, bundle, true, REQUEST_TAG_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_UP);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //if (cartDataSource == null) return true;
        setCount(Dashboard.this, String.valueOf(productModelArrayList.size()));
        return true;
    }

    public void setCount(Context context, String count) {
        if (menu == null) return;
        MenuItem menuItem = menu.findItem(R.id.menu_cart);
        LayerDrawable icon = (LayerDrawable) menuItem.getIcon();
        CountDrawable badge;
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    private void displayProfile() {
        String userName = SharedPreferenceUtils.getInstance(context).getString(USER_NAME);
        String userImage = SharedPreferenceUtils.getInstance(context).getString(USER_PROFILE_IMAGE);
        String mobileNumber = SharedPreferenceUtils.getInstance(context).getString(USER_MOBILE_NO);
        if (!TextUtils.isEmpty(userName)) {
            //textUserName.setText(userName);
        }
        if (!TextUtils.isEmpty(mobileNumber)) {
            textMobileNumber.setText(mobileNumber);
        }

        if (!TextUtils.isEmpty(userImage)) {
            String imageUrl = BASE_URL_IMAGES + userImage;
            Picasso.with(currentActivity).load(imageUrl).placeholder(R.drawable.profile_icon).error(R.drawable.profile_icon).into(circleImageDrawerProfile);
        }
        FontUtils.changeFont(this, textUserName, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, textMobileNumber, FONT_OPEN_SANS_SEMIBOLD_TTF);
    }

    public void setSelection(int position) {
        fragmentType = position;
        drawerLayout.closeDrawer(Gravity.LEFT);
        frag = null;
        boolean isAddedBackStack = false;
        boolean isReplace = false;
        switch (position) {
            case MAINBOARD: {
                frag = new MainBoard();
                settingTitle("Your Address");
                removingHomeButton();
                isAddedBackStack = true;
                break;
            }
            case ORDER_HISTORY: {
                if (LoginUtils.isLogin(context))
                    startActivity(currentActivity, MyAppointmentsActivity.class, bundle, false, REQUEST_TAG_EDIT_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_LEFT);
                else {
                    startActivity(currentActivity, LoginActivity.class, bundle, true, REQUEST_TAG_EDIT_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_LEFT);
                }
                break;
            }
            case PROFILE: {
                if (LoginUtils.isLogin(context))
                    startActivity(currentActivity, ProfileActivity.class, bundle, true, REQUEST_TAG_EDIT_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_LEFT);
                else {
                    startActivity(currentActivity, LoginActivity.class, bundle, true, REQUEST_TAG_EDIT_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_LEFT);
                }
                break;
            }
            case CONTACT_US: {
                startActivity(currentActivity, ContactUsActivity.class, bundle, false, REQUEST_TAG_EDIT_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_LEFT);
                break;
            }
            case ABOUT_US: {
                String title = getString(R.string.title_about_us);
                toOpenWebViewActivity(title, ABOUTUS_URL);
                break;
            }
            case PRIVACY_POLICY: {
                String title = getString(R.string.title_privacy);
                toOpenWebViewActivity(title, PRIVACY_URL);
                break;
            }
            case TERMS_AND_SERVICES: {
                String title = getString(R.string.title_terms);
                toOpenWebViewActivity(title, TERMSCONDITIONS_URL);
                break;
            }
            case FAQ: {
                String title = getString(R.string.title_faqs);
                toOpenWebViewActivity(title, FAQS_URL);
                break;
            }
            case LOGOUT: {
                if (isUserLogin) {
                    alert(currentActivity, getResources().getString(R.string.alert_title_logout), getResources().getString(R.string.alert_message_logout), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_LOGOUT);
                } else {
                    logout();
                }
                break;
            }


        }

        if (frag != null) {
            switchContent(frag, false, isReplace, frag.getClass().getName());
        }
    }

    private void toOpenWebViewActivity(String title, String url) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("WebUrl", url);
        bundle.putString("title", title);
        startActivity(currentActivity, WebViewActivity.class, bundle, true, REQUEST_TAG_EDIT_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_LEFT);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        actionBarDrawerToggle.syncState();
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public void onBackPressed() {
        if (isExitable) {
            super.onBackPressed();
        } else {
            toast(getResources().getString(R.string.message_app_exit), true);
            isExitable = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isExitable = false;
                }
            }, APP_EXIT_TIME);
        }
    }

    //On click event for rate this app button
    public void btnRateAppOnClick() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //Try Google play
        intent.setData(Uri.parse("market://details?id=com.neon.vyan"));
        if (!MyStartActivity(intent)) {
            //Market (Google play) app seems not installed, let's try to open a webbrowser
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.neon.vyan"));
            if (!MyStartActivity(intent)) {
                //Well if this also fails, we have run out of options, inform the user.
                Toast.makeText(this, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isUserLogin() {
        isUserLogin = false;
        String mobileNumber = SharedPreferenceUtils.getInstance(currentActivity).getString(USER_MOBILE_NO);
        if (!mobileNumber.isEmpty()) {
            isUserLogin = true;
            return isUserLogin;
        }
        return isUserLogin;
    }

    @Override
    public void productAddedToCart(ProductModel productModel) {
        if (!isProductAlreadyList(productModel)) {
            productModel.setProductQty(1);
            productModel.setServiceQty(1);
            productModelArrayList.add(0, productModel);
        }
        setCount(Dashboard.this, String.valueOf(productModelArrayList.size()));
    }

    private boolean isProductAlreadyList(ProductModel productModel) {
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        int count = 0;
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                productModelArrayList.remove(count);
                return true;
            }
            count++;
        }
        return false;
    }

    public boolean isProductExist(ProductModel productModel) {
        if (productModelArrayList == null) return false;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                return true;
            }

        }
        return false;
    }
}

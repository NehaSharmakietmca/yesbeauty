package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.model.ProfileDetailsModel;
import com.app.yesbeautyapp.network.BaseVolleyRequest;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.Helper;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;
import com.app.yesbeautyapp.widgets.CircularImageView;

import static com.app.yesbeautyapp.constants.AppConstants.USER_EMAIL_ID;

public class UpdateProfileActivity extends BaseActivity {

    private TextView labelNameTV;
    private TextView labelMobileNoTV;
    private TextView labelemailTV;
    private TextView labelAddressTV;
    private TextView labelGenderTV;
    private TextView labelDobTV;
    private EditText editName;
    private EditText editMobileNo;
    private EditText editEmail;
    private EditText editAddress;
    private TextView dobTV;
    private Button btnEditProfile;
    private Button btnMale;
    private Button btnFemale;
    private Button btnSignOut;
    private ImageView dobIV;
    private CircularImageView circularProfileImage;

    private int bytesAvailable;
    private int bytesRead;
    private String mimeType;
    private ProfileDetailsModel profileDetailsModel;
    private Calendar mCalendar;
    private String gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_update_profile));
        settingHomeButton();
        labelNameTV = (TextView) findViewById(R.id.labelNameTV);
        labelMobileNoTV = (TextView) findViewById(R.id.labelMobileNoTV);
        labelemailTV = (TextView) findViewById(R.id.labelemailTV);
        labelAddressTV = (TextView) findViewById(R.id.labelAddressTV);
        labelGenderTV = (TextView) findViewById(R.id.labelGenderTV);
        labelDobTV = (TextView) findViewById(R.id.labelDobTV);
        editName = (EditText) findViewById(R.id.editName);
        editMobileNo = (EditText) findViewById(R.id.editMobileNo);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editAddress = (EditText) findViewById(R.id.editAddress);
        dobTV = (TextView) findViewById(R.id.dobTV);
        btnEditProfile = (Button) findViewById(R.id.btnEditProfile);
        btnMale = (Button) findViewById(R.id.btnMale);
        btnFemale = (Button) findViewById(R.id.btnFemale);
        btnSignOut = (Button) findViewById(R.id.btnSignOut);
        circularProfileImage = (CircularImageView) findViewById(R.id.circularProfileImage);
        dobIV = (ImageView) findViewById(R.id.dobIV);

        FontUtils.changeFont(context, labelNameTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, labelMobileNoTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, labelemailTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, labelAddressTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, labelGenderTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, labelDobTV, FONT_OPEN_SANS_REGULAR_TTF);

        FontUtils.changeFont(context, editName, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, editMobileNo, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, editEmail, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, editAddress, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, dobTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnEditProfile, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnMale, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnFemale, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnSignOut, FONT_OPEN_SANS_REGULAR_TTF);

        mCalendar = Calendar.getInstance();

        editName.setText(SharedPreferenceUtils.getInstance(context).getString(USER_NAME));
        editEmail.setText(SharedPreferenceUtils.getInstance(context).getString(USER_EMAIL_ID));

        getProfile();
    }

    @Override
    protected void initContext() {
        context = UpdateProfileActivity.this;
        currentActivity = UpdateProfileActivity.this;
    }

    @Override
    protected void initListners() {
        btnEditProfile.setOnClickListener(this);
        circularProfileImage.setOnClickListener(this);
        dobIV.setOnClickListener(this);
        btnMale.setOnClickListener(this);
        btnFemale.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnEditProfile: {
                toHideKeyboard();
                if (Validator.isNetworkAvailable(currentActivity)) {
                    if (isMandatoryFields()) {
                        updateProfile();
                    }
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                }
                break;
            }
            case R.id.circularProfileImage: {
                toHideKeyboard();
                String loginWith = SharedPreferenceUtils.getInstance(currentActivity).getString(LOGIN_WITH);
                if (TextUtils.isEmpty(loginWith)) {
                    if (Validator.isNetworkAvailable(currentActivity)) {
                        selectImage();
                    } else {
                        alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_cancel_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                    }
                }
                break;
            }
            case R.id.dobIV: {
                toHideKeyboard();
                toOpenDatePicker();
                break;
            }
            case R.id.btnMale: {
                toHideKeyboard();
                gender = btnMale.getText().toString();
                btnFemale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                btnMale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                break;
            }
            case R.id.btnFemale: {
                toHideKeyboard();
                gender = btnFemale.getText().toString();
                btnFemale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                btnMale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;
            }
            case R.id.btnSignOut: {
                alert(currentActivity, getResources().getString(R.string.alert_title_logout), getResources().getString(R.string.alert_message_logout), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), true, true, ALERT_TYPE_LOGOUT);
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {
        if (alertType == ALERT_TYPE_LOGOUT) {
            logout();
        }
    }

    public void logout() {
        SharedPreferenceUtils.getInstance(context).clearALl();
        setResult(RESULT_OK);
        finish();
    }

    private void toOpenDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        String selectedDate = dobTV.getText().toString();
        if (!TextUtils.isEmpty(selectedDate)) {
            /*String yearString = selectedDate.substring(6, 10);
            String monthString = selectedDate.substring(3, 5);
            String dayString = selectedDate.substring(0, 2);
            year = Integer.parseInt(yearString);
            month = Integer.parseInt(monthString);
            day = Integer.parseInt(dayString);
            month--;*/
        }
        DatePickerDialog dpDialog = new DatePickerDialog(currentActivity, dobListner, year, month, day);
        //dpDialog.updateDate(year, month, day);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpDialog.show();
    }

    DatePickerDialog.OnDateSetListener dobListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.YEAR, year);
            mCalendar.set(Calendar.MONTH, monthOfYear);
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd MMM yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            dobTV.setText(sdf.format(mCalendar.getTime()));
        }
    };

    private void getProfile() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonGetUser = null;
        int userId = SharedPreferenceUtils.getInstance(context).getInteger(USER_ID);
        try {
            jsonGetUser = new JSONObject();
            jsonGetUser.put("user_id", userId);
            Log.e("jsonGetUser", jsonGetUser.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_GET_USER = GET_USER_DETAILS_URL;
        JsonObjectRequest profileUpdateRequest = new JsonObjectRequest(Request.Method.POST, URL_GET_USER, jsonGetUser, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                cancelProgressDialog();
                try {
                    String message = response.getString(RESPONCE_MESSAGE);
                    if (!response.getBoolean(RESPONCE_ERROR)) {
                        if (message == null) return;
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        ProfileDetailsModel profileModel = gson.fromJson(message.toString(), ProfileDetailsModel.class);
                        if (profileModel == null) return;
                        profileDetailsModel = profileModel;
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_PHONE, profileDetailsModel.getPhoneNumber());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, profileDetailsModel.getName());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_EMAIL_ID, profileDetailsModel.getEmailId());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_ADDRESS, profileDetailsModel.getAddress());
                        toSetProfileDetails();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(profileUpdateRequest);
    }

    private void toSetProfileDetails() {
        if (profileDetailsModel == null) return;
        editName.setText(profileDetailsModel.getName() == null ? "" : profileDetailsModel.getName());
        editEmail.setText(SharedPreferenceUtils.getInstance(context).getString(USER_EMAIL_ID));
        editMobileNo.setText(profileDetailsModel.getPhoneNumber() == null ? "" : profileDetailsModel.getPhoneNumber());
        editAddress.setText(profileDetailsModel.getAddress() == null ? "" : profileDetailsModel.getAddress());
        dobTV.setText(profileDetailsModel.getDob() == null ? "" : profileDetailsModel.getDob());
        gender = profileDetailsModel.getGender();
        if (!TextUtils.isEmpty(gender)) {
            if (gender.equalsIgnoreCase(btnMale.getText().toString())) {
                btnFemale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                btnMale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
            } else if (gender.equalsIgnoreCase(btnFemale.getText().toString())) {
                btnFemale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                btnMale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }
        } else {
            btnFemale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            btnMale.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }

        toSetProfileImage();
    }

    private void toSetProfileImage() {
        String imageUrl = "";
        String loginWith = SharedPreferenceUtils.getInstance(currentActivity).getString(LOGIN_WITH);
        if (TextUtils.isEmpty(loginWith)) {

            String userImage = profileDetailsModel.getImage();
            imageUrl = BASE_URL_IMAGES + userImage;

        }

        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(currentActivity).load(imageUrl).error(R.drawable.profile_icon).into(circularProfileImage);
        }
    }


    private boolean isMandatoryFields() {

        editName.setError(null);
        editMobileNo.setError(null);
        editEmail.setError(null);
        editAddress.setError(null);

        String numberError = Validator.getInstance().validateNumber(context, editMobileNo.getText().toString());

        if (editName.getText().toString().isEmpty()) {
            editName.setError(getResources().getString(R.string.error_editText_name));
            editName.requestFocus();
            return false;
        } else if (!TextUtils.isEmpty(numberError)) {
            editMobileNo.setError(numberError);
            editMobileNo.requestFocus();
            return false;
        } else if (!Validator.getInstance().isValidEmail(context, editEmail.getText().toString()).equals("")) {
            String emailError = Validator.getInstance().isValidEmail(context, editEmail.getText().toString());
            editEmail.setError(emailError);
            editEmail.requestFocus();
            return false;
        } else if (editAddress.getText().toString().isEmpty()) {
            editAddress.setError(getResources().getString(R.string.error_editText_address));
            editAddress.requestFocus();
            return false;
        } else if (dobTV.getText().toString().isEmpty()) {
            toast(getResources().getString(R.string.error_dob), true);
            return false;
        } else if (TextUtils.isEmpty(gender)) {
            toast(getResources().getString(R.string.error_gender), true);
            return false;
        }
        initProfileDetailsModel();
        return true;
    }

    private void initProfileDetailsModel() {
        if (profileDetailsModel == null) {
            profileDetailsModel = ProfileDetailsModel.getInstance();
        }
        int userId = SharedPreferenceUtils.getInstance(context).getInteger(USER_ID);
        profileDetailsModel.setId(userId);
        profileDetailsModel.setName(editName.getText().toString());
        profileDetailsModel.setEmailId(editEmail.getText().toString());
        profileDetailsModel.setPhoneNumber(editMobileNo.getText().toString());
        profileDetailsModel.setAddress(editAddress.getText().toString());
        profileDetailsModel.setDob(dobTV.getText().toString());
        profileDetailsModel.setGender(gender == null ? "" : gender);
        profileDetailsModel.setImage(profileDetailsModel.getImage() == null ? "" : profileDetailsModel.getImage());
    }

    private void updateProfile() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonUpdateUser = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            jsonUpdateUser = new JSONObject(gson.toJson(profileDetailsModel));
            Log.e("jsonUpdateUser", jsonUpdateUser.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_UPDATE_USER = UPDATE_USER_DETAILS_URL;
        JsonObjectRequest profileUpdateRequest = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_USER, jsonUpdateUser, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                cancelProgressDialog();
                try {
                    logTesting(getResources().getString(R.string.nwk_response_edit_profile), response.toString(), Log.ERROR);
                    String message = response.getString(RESPONCE_MESSAGE);
                    if (response.getBoolean(RESPONCE_ERROR)) {
                        alert(currentActivity, getString(R.string.message_profile_update_fail), getString(R.string.message_profile_update_fail), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                    } else {
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, editName.getText().toString());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_PHONE, editMobileNo.getText().toString());
                        toast(getResources().getString(R.string.message_profile_updated), true);
                        setResult(RESULT_OK);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getResources().getString(R.string.nwk_error_edit_profile), true);
                logTesting(getResources().getString(R.string.nwk_error_edit_profile), error.toString(), Log.ERROR);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(profileUpdateRequest);
    }

    private void selectImage() {
        try {
            if (checkPermission()) {
                openChooseImageDialog();
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(currentActivity, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openChooseImageDialog();
                }
                break;
            }
        }
    }


    public void openChooseImageDialog() {
        final CharSequence[] options = {getString(R.string.option_take_photo), getString(R.string.option_gallery), getString(R.string.option_cancel)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(currentActivity);
        builder.setTitle(getString(R.string.select_option));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getString(R.string.option_take_photo))) {
                    dialog.dismiss();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);
                } else if (options[item].equals(getString(R.string.option_gallery))) {
                    dialog.dismiss();
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                } else if (options[item].equals(getString(R.string.option_cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (resultCode == RESULT_OK) {
                    Bitmap imageBitmap = (Bitmap) imageReturnedIntent.getExtras().get(KEY_DATA);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                    circularProfileImage.setImageBitmap(imageBitmap);
                    uploadImageByVolley();
                }
                break;
            }
            case PICK_IMAGE_GALLERY: {
                if (resultCode == RESULT_OK) {
                    Uri filePath = imageReturnedIntent.getData();
                    try {
                        //Getting the Bitmap from Gallery
                        Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                        circularProfileImage.setImageBitmap(imageBitmap);
                        uploadImageByVolley();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                break;
            }

        }
    }

    public void uploadImageByVolley() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);


        final String lineEnd = "\r\n";
        final String twoHyphens = "--";
        final String boundary = "*****";

        final int maxBufferSize = 1 * 1024 * 1024;
        final byte[] imageBytesArray = Helper.getImageBytes(((BitmapDrawable) circularProfileImage.getDrawable()).getBitmap());
        Long timeMillis = System.currentTimeMillis();
        final String fileName = "Image_" + timeMillis.toString() + ".jpg";


        String imageUploadUrl = UPLOAD_USER_IMAGE_URL;

        BaseVolleyRequest uploadProfileImageRequest = new BaseVolleyRequest(1, imageUploadUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                try {
                    //cancelProgressDialog();
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    Log.e("image response  is", response.toString() + jsonString);
                    JSONObject jsonObject = new JSONObject(jsonString);
                    if (jsonObject.getBoolean(RESPONCE_ERROR)) {
                        alert(currentActivity, getString(R.string.message_image_upload_fail), getString(R.string.message_image_upload_fail), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                    } else {
                        SharedPreferenceUtils.getInstance(context).putString(USER_PROFILE_IMAGE, fileName);
                        profileDetailsModel.setId(SharedPreferenceUtils.getInstance(context).getInteger(USER_ID));
                        profileDetailsModel.setImage(fileName);
                        updateProfileImage();
                    }
                } catch (UnsupportedEncodingException e) {
                    cancelProgressDialog();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                    cancelProgressDialog();
                }
                Log.e("image response  is", response.toString());
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getString(R.string.error_uploading_image), true);
                Log.e(getString(R.string.error_uploading_image), error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                mimeType = "multipart/form-data;boundary=" + boundary;
                return mimeType;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);


                try {
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);

                    ByteArrayInputStream fileInputStream = new ByteArrayInputStream(imageBytesArray);
                    bytesAvailable = fileInputStream.available();

                    int bufferSize = Math.min(imageBytesArray.length, maxBufferSize);
                    byte[] buffer = new byte[bufferSize];

                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }


                    //    dos.write(imageBytesArray, 0, bufferSize);
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    return bos.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return imageBytesArray;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Long timeMillis = System.currentTimeMillis();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Connection", "Keep-Alive");
                params.put("ENCTYPE", "multipart/form-data");
                params.put("accept", "application/json");
                params.put("uploaded_file", fileName);
                params.put("Content-Type", "multipart/form-data;boundary=" + boundary);
                return params;

            }

        };

        AplusoneApplication.getInstance().addToRequestQueue(uploadProfileImageRequest);
    }

    private void updateProfileImage() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonUpdateUser = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            jsonUpdateUser = new JSONObject(gson.toJson(profileDetailsModel));
            Log.e("jsonUpdateUser", jsonUpdateUser.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_UPDATE_USER = UPDATE_USER_IMAGE_URL;
        JsonObjectRequest profileUpdateRequest = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_USER, jsonUpdateUser, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                cancelProgressDialog();
                try {
                    logTesting(getResources().getString(R.string.nwk_response_edit_profile), response.toString(), Log.ERROR);
                    String message = response.getString(RESPONCE_MESSAGE);
                    if (response.getBoolean(RESPONCE_ERROR)) {
                        alert(currentActivity, getString(R.string.message_profile_update_fail), getString(R.string.message_profile_update_fail), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                    } else {
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_PROFILE_IMAGE, editName.getText().toString());
                        toast(getString(R.string.message_image_upload_suc), true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(profileUpdateRequest);
    }

}

package com.app.yesbeautyapp.activity;

import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.AppointmentProductListAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.fragment.BaseFragment;
import com.app.yesbeautyapp.model.OrderModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.DateTimeUtils;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AppointmentHistoryDetailActivity extends BaseActivity {
    private TextView dateValueTV;
    private TextView orderIdTV;
    private TextView orderIdValueTV;
    private TextView nameTV;
    private TextView nameValueTV;
    private TextView addressTV;
    private TextView addressValueTV;
    private TextView serviceTypeTV;
    private TextView serviceTypeValueTV;
    private TextView categoryTV;
    private TextView categoryValueTV;
    private TextView serviceMinutesTV;
    private TextView serviceMinutesValueTV;
    private TextView bookingDateTV;
    private TextView bookingDateValueTV;
    private TextView bookingTimeTV;
    private TextView bookingTimeValueTV;
    private TextView stylistLabelTV;
    private TextView stylistValueTV;
    private TextView paymentLabelTV;
    private TextView paymentValueTV;
    private TextView totalServiceChargeLabelTV;
    private TextView totalServiceChargeValueTV;
    private TextView toatalProductCostLabelTV;
    private TextView toatalProductCostValueTV;
    private TextView tarsportChargeLabelTV;
    private TextView transportChargeValueTV;
    private TextView walletRedeemLabelTV;
    private TextView walletRedeemValueTV;
    private TextView couponDiscountLabelTV;
    private TextView couponDiscountValueTV;
    private TextView disposableLabelTV;
    private TextView disposableValueTV;
    private TextView totalAmountLabelTV;
    private TextView totalAmountValueTV;
    private TextView serviceDetailsTV;
    private RecyclerView productListRecyclerView;
    private AppCompatButton cancelBTN;
    private AppCompatButton rescheduleBTN;
    private LinearLayout linBottom;
    private OrderModel orderModel;
    private AppointmentProductListAdapter productListAdapter;
    private List<ProductModel> productModelArrayList = new ArrayList<>();
    private RadioButton reason1;
    private RadioButton reason2;
    private RadioButton reason3;
    private RadioButton reason4;
    private RadioGroup radioGroup;
    private ImageView cancelImageView;
    private Button submitButton;
    private BottomSheetDialog reasonEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_history_detail);
    }

    @Override
    protected void initViews() {
        if (getIntent().getExtras() != null) {
            orderModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
        }
        settingTitle("Appointment Details");
        dateValueTV = findViewById(R.id.text_date_value);
        orderIdTV = findViewById(R.id.text_order_id_label);
        orderIdValueTV = findViewById(R.id.text_order_id_value);
        nameTV = findViewById(R.id.text_appointment_history_name);
        nameValueTV = findViewById(R.id.text_appointment_history_name_value);
        addressTV = findViewById(R.id.text_appointment_history_address);
        addressValueTV = findViewById(R.id.text_appointment_history_address_value);
        serviceTypeTV = findViewById(R.id.text_appointment_history_service_type_label);
        serviceTypeValueTV = findViewById(R.id.text_appointment_history_service_type_value);
        categoryTV = findViewById(R.id.text_appointment_history_category_label);
        categoryValueTV = findViewById(R.id.text_appointment_history_category_value);
        serviceMinutesTV = findViewById(R.id.text_appointment_history_service_minutes_label);
        serviceMinutesValueTV = findViewById(R.id.text_appointment_history_service_minutes_value);
        bookingDateTV = findViewById(R.id.text_appointment_history_booking_date_label);
        bookingDateValueTV = findViewById(R.id.text_appointment_history_booking_date_value);
        bookingTimeTV = findViewById(R.id.text_appointment_history_booking_time_label);
        bookingTimeValueTV = findViewById(R.id.text_appointment_history_booking_time_value);
        stylistLabelTV = findViewById(R.id.text_appointment_history_booking_stylist_label);
        stylistValueTV = findViewById(R.id.text_appointment_history_booking_stylist_value);
        paymentLabelTV = findViewById(R.id.text_appointment_history_booking_payment_medium_label);
        paymentValueTV = findViewById(R.id.text_appointment_history_booking_payment_medium_value);
        totalServiceChargeLabelTV = findViewById(R.id.text_appointment_history_booking_total_service_charge_label);
        totalServiceChargeValueTV = findViewById(R.id.text_appointment_history_booking_total_service_charge_value);
        toatalProductCostLabelTV = findViewById(R.id.text_appointment_history_booking_total_product_cost_label);
        toatalProductCostValueTV = findViewById(R.id.text_appointment_history_booking_total_product_cost_value);
        tarsportChargeLabelTV = findViewById(R.id.text_appointment_history_booking_transport_charge_label);
        transportChargeValueTV = findViewById(R.id.text_appointment_history_booking_transport_charge_value);
        walletRedeemLabelTV = findViewById(R.id.text_appointment_history_booking_wallet_redeem_amount_label);
        walletRedeemValueTV = findViewById(R.id.text_appointment_history_booking_wallet_redeem_amount_value);
        couponDiscountLabelTV = findViewById(R.id.text_appointment_history_coupon_discount_label);
        couponDiscountValueTV = findViewById(R.id.text_appointment_history_coupon_discount_value);
        disposableLabelTV = findViewById(R.id.text_appointment_history_booking_disposable_charge_label);
        disposableValueTV = findViewById(R.id.text_appointment_history_booking_disposable_charge_value);
        totalAmountLabelTV = findViewById(R.id.text_appointment_history_booking_total_amount_label);
        totalAmountValueTV = findViewById(R.id.text_appointment_history_booking_total_amount_value);
        serviceDetailsTV = findViewById(R.id.text_appointment_history_service_details_label);
        productListRecyclerView = findViewById(R.id.recycler_view_appointment_product_list);
        cancelBTN = findViewById(R.id.btn_appointment_history_details_cancel);
        rescheduleBTN = findViewById(R.id.btn_appointment_history_details_reschedule);
        linBottom = findViewById(R.id.lin_bottom);
        setFont();
        // createProductList();
        setProductRecyclerView();
        setAppointmentHistoryDetails();
        getAppointmentDetails();


    }

    @Override
    protected void initContext() {
        currentActivity = this;
        context = this;
    }

    @Override
    protected void initListners() {
        rescheduleBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_appointment_history_details_cancel:
                showCancelReasonDialog();
                break;
            case R.id.btn_appointment_history_details_reschedule:
                if (bundle == null) {
                    bundle = new Bundle();
                }
                bundle.putParcelable(AppConstants.MODEL_OBJ, orderModel);
                bundle.putBoolean(AppConstants.KEY_BOOLEAN,true);
                ((BaseActivity) currentActivity).startActivity(currentActivity, TimeScheduleActivity.class, bundle, true, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_LEFT);
                finish();
        }

    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    private void setFont() {
        FontUtils.changeFont(context, dateValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, orderIdTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, orderIdValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, nameTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, nameValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, addressTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, addressValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, categoryTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, categoryValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, serviceMinutesTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, serviceMinutesValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, serviceTypeTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, serviceTypeValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, bookingDateTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingDateValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, bookingTimeTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingTimeValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, stylistLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, stylistValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, paymentLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, paymentValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, totalServiceChargeLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, totalServiceChargeValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, toatalProductCostLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, toatalProductCostValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, tarsportChargeLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, transportChargeValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, walletRedeemLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, walletRedeemValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, couponDiscountLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, couponDiscountValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, disposableLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, disposableValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, totalAmountLabelTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, totalAmountValueTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, serviceDetailsTV, FONT_OPEN_SANS_SEMIBOLD_TTF);

    }

    private void getAppointmentDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject orderDetails = new JSONObject();
        try {
            orderDetails.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            orderDetails.put(AppConstants.ORDER_ID, orderModel.getOrderId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_ORDER_DETAILS, orderDetails, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                productModelArrayList.clear();
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                cancelProgressDialog();
                try {
                    Gson gson = new Gson();
                    List<ProductModel> productModels = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), ProductModel[].class));
                    productModelArrayList.addAll(productModels);
                    productListAdapter.notifyDataSetChanged();
                    toatalProductCostValueTV.setText(context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", totalProductAmount()) + "/-");
                    totalServiceChargeValueTV.setText(context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", getServiceAmt()) + "/-");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    private void setProductRecyclerView() {
        productListAdapter = new AppointmentProductListAdapter(currentActivity, productModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        productListRecyclerView.setLayoutManager(mLayoutManager);
        productListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        productListRecyclerView.setAdapter(productListAdapter);
    }

    private void setAppointmentHistoryDetails() {
        if (orderModel == null) return;
        String orderId = orderModel.getOrderId();
        if (orderId.length() == 1) {
            orderId = "000" + orderId;
        } else if (orderId.length() == 2) {
            orderId = "00" + orderId;
        } else if (orderId.length() == 3) {
            orderId = "0" + orderId;
        }
        if(orderModel.getOrderStatus().equalsIgnoreCase("3")){
            linBottom.setVisibility(View.GONE);
        }else{
            linBottom.setVisibility(View.VISIBLE);
        }
        orderIdValueTV.setText(orderId);
        dateValueTV.setText(DateTimeUtils.getDateInFormat(orderModel.getOrderDate()));
        nameValueTV.setText(orderModel.getName());
        addressValueTV.setText(orderModel.getAddress());
        serviceTypeValueTV.setText(orderModel.getServiceType());
        bookingDateValueTV.setText(orderModel.getBookingDate());
        bookingTimeValueTV.setText(orderModel.getBookingTime());
        paymentValueTV.setText(orderModel.getPaymentMode());
        serviceTypeValueTV.setText(orderModel.getServiceType());
        String totalAmt = context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", Double.parseDouble(orderModel.getOrderAmount())) + "/-";
        totalAmountValueTV.setText(totalAmt);
        String discAmt = context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", Double.parseDouble(orderModel.getDiscount())) + "/-";
        couponDiscountValueTV.setText(discAmt);
        String dispbleAmt = context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", Double.parseDouble("0")) + "/-";
        disposableValueTV.setText(dispbleAmt);
        String walletAmt = context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", Double.parseDouble("0")) + "/-";
        walletRedeemValueTV.setText(walletAmt);
    }

    private double totalProductAmount() {
        double totalAmount = 0.0;
        if (productModelArrayList == null) return totalAmount;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null) {
                double totalAmt = 0;
                String productCost = productModel.getProductCost();
                if (TextUtils.isEmpty(productCost)) productCost = "0";
                totalAmt = (productModel.getProductQty() * Double.parseDouble(productCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    private double getServiceAmt() {
        double totalAmount = 0.0;
        if (productModelArrayList == null) return totalAmount;

        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null) {
                double totalAmt = 0;
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = (productModel.getServiceQty() * Double.parseDouble(serviceCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    private void createProductList() {
        ProductModel productModel;
        productModel = new ProductModel();
        productModel.setProductName("O3 Skin Purifying Treatment");
        productModel.setDescription("Gadget used- \n Ultrasonic Machine & Dermabrasion Machine \n for Acne prone skin");
        productModel.setDuration("50 Minutes");
        productModel.setServiceCost("Rs. 750");
        productModel.setProductCost("Rs. 400");
        productModel.setCat("Facial Treatment");
        productModel.setScat("Luxury");
        productModel.setShortDesc("Clean Up");
        productModel.setAmount("Rs. 1150");
        productModelArrayList.add(productModel);
        productModel = new ProductModel();
        productModel.setProductName("Pearl Skin Purifying Treatment");
        productModel.setDescription("Gadget used- \n Ultrasonic Machine & Dermabrasion Machine \n for Acne prone skin");
        productModel.setDuration("50 Minutes");
        productModel.setServiceCost("Rs. 750");
        productModel.setProductCost("Rs. 400");
        productModel.setCat("Facial Treatment");
        productModel.setScat("Luxury");
        productModel.setShortDesc("Clean Up");
        productModel.setAmount("Rs. 1150");
        productModelArrayList.add(productModel);
    }

    public void showCancelReasonDialog() {
        if (reasonEditDialog != null) {
            reasonEditDialog.setCancelable(false);
            reasonEditDialog.setCanceledOnTouchOutside(false);

        }
        View view = getLayoutInflater().inflate(R.layout.fragment_booking_caccel_reason, null);
        reasonEditDialog = new BottomSheetDialog(this);
        reason1 = view.findViewById(R.id.reason_1);
        reason2 = view.findViewById(R.id.reason_2);
        reason3 = view.findViewById(R.id.reason_3);
        reason4 = view.findViewById(R.id.reason_4);
        radioGroup = view.findViewById(R.id.reason_radio_group);
        submitButton = view.findViewById(R.id.reason_submit_button);
        cancelImageView = view.findViewById(R.id.reason_cancel);
        if (radioGroup != null) {
            final int radioGroupButtonId = radioGroup.getCheckedRadioButtonId();
            setRadioButtonSelection(radioGroupButtonId);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    setRadioButtonSelection(i);

                }
            });
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reasonEditDialog != null) {
                    reasonEditDialog.dismiss();
                }
                cancelBooking(orderModel);
            }
        });
        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reasonEditDialog != null) {
                    reasonEditDialog.dismiss();
                }
            }
        });

        reasonEditDialog.setContentView(view);

        reasonEditDialog.show();
    }

    private void setRadioButtonSelection(int i) {
        switch (i) {
            case -1:

                submitButton.setEnabled(false);

                break;
            case R.id.reason_1:
                if (submitButton != null) {
                    submitButton.setEnabled(true);
                }
                if (reason1 != null) {
                    orderModel.setCancelReason(reason1.getText().toString());
                }
                break;
            case R.id.reason_2:

                if (submitButton != null) {
                    submitButton.setEnabled(true);
                }
                if (reason1 != null) {
                    orderModel.setCancelReason(reason2.getText().toString());
                }

                break;
            case R.id.reason_3:
                if (submitButton != null) {
                    submitButton.setEnabled(true);
                }
                if (reason1 != null) {
                    orderModel.setCancelReason(reason3.getText().toString());
                }

                break;
            case R.id.reason_4:
                if (reasonEditDialog != null) {
                    reasonEditDialog.dismiss();
                }
                showCancelReasonEditDialog();
                break;


        }
    }

    public void showCancelReasonEditDialog() {
        View view = getLayoutInflater().inflate(R.layout.fragment_booking_reason_other_issue, null);
        ImageView backButton = view.findViewById(R.id.btn_other_issue_back);
        final Button otherReasonButton = view.findViewById(R.id.other_issue_reason_button);
        final EditText otherResonEdit = view.findViewById(R.id.other_issue_edit);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                showCancelReasonDialog();
            }
        });
        otherReasonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otherResonEdit == null || otherResonEdit.getText() == null || otherReasonButton.getText().toString().isEmpty()) {
                    Toast.makeText(AppointmentHistoryDetailActivity.this, "Please give us suggestion", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    dialog.dismiss();
                    orderModel.setCancelReason(otherResonEdit.getText().toString());
                    cancelBooking(orderModel);
                }


            }
        });

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void cancelBooking(OrderModel orderModel) {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        ;
        try {
            jsons = new JSONObject();
            jsons.put("authid", SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            jsons.put("ordid", orderModel.getOrderId());
            jsons.put("cancelreason", orderModel.getCancelReason());
            Log.e("cancelBooking", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_CANCEL_BOOKING, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                finish();
                            }
                        }
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                //  failurePromocodeTextView.setVisibility(View.VISIBLE);
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }
}

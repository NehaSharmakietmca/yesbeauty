package com.app.yesbeautyapp.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.SearchServiceAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SearchServiceActivity extends BaseActivity {
        private RecyclerView recyclerView;
        private EditText editSearch;
        private TextView copyRightsTV;
        private TextView noDataTV;
        private List<ProductModel> searchModelArrayList;
        private SearchServiceAdapter searchListAdapter;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_search_service);
        }


        @Override
        protected void initViews() {
            settingTitle(getString(R.string.what_are_you_looking_for));
            searchModelArrayList = new ArrayList<>();
            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            editSearch = (EditText) findViewById(R.id.editSearch);
            noDataTV = (TextView) findViewById(R.id.noDataTV);
            searchListAdapter = new SearchServiceAdapter(currentActivity, searchModelArrayList);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(searchListAdapter);

            FontUtils.changeFont(context, editSearch, FONT_OPEN_SANS_REGULAR_TTF);

            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }

        @Override
        protected void initContext() {
            context = SearchServiceActivity.this;
            currentActivity = SearchServiceActivity.this;
        }

        @Override
        protected void initListners() {

            editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        toHideKeyboard();
                        if (TextUtils.isEmpty(editSearch.getText().toString())) {
                            searchModelArrayList.clear();
                            noDataTV.setVisibility(View.VISIBLE);
                            searchListAdapter.notifyDataSetChanged();
                        }
                        getSearchedList(editSearch.getText().toString());
                        return true;
                    }
                    return false;
                }
            });
        }

        @Override
        protected boolean isActionBar() {
            return true;
        }

        @Override
        protected boolean isHomeButton() {
            return true;
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void onAlertClicked(int alertType) {

        }

        private void getSearchedList(String searchedString) {
            progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
            String URL_GET_SEARCHED = SEARCHSERVICE_URL;
            Log.e("URL_GET_SEARCHED", URL_GET_SEARCHED);
            JSONObject jsons = null;

            try {
                jsons = new JSONObject();
                jsons.put("keyword", searchedString);
                Log.e("jsonGetAllService", jsons.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_SEARCHED,jsons, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                    cancelProgressDialog();
                    try {
                        Gson gson = new Gson();
                        List<ProductModel> serviceModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), ProductModel[].class));
                        searchModelArrayList.clear();
                        searchModelArrayList.addAll(serviceModelTempList);
                        if (searchModelArrayList != null && !searchModelArrayList.isEmpty()) {
                            noDataTV.setVisibility(View.GONE);
                        } else {
                            noDataTV.setVisibility(View.VISIBLE);
                        }
                        searchListAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                         e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    cancelProgressDialog();
                    toast(getString(R.string.msg_not_connect), false);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return super.getHeaders();
                }
            };

            AplusoneApplication.getInstance().addToRequestQueue(request);
        }
}

package com.app.yesbeautyapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.PromocodeAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.PromocodeModel;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;

public class PromoCodeActivity extends BaseActivity implements RecyclerClickListner {
    private EditText promocodeEditText;
    private RecyclerView promocodeRecycleViewList;
    private TextView applyTextView;
    private TextView headingTextView;
    private TextView failurePromocodeTextView;
    private PromocodeAdapter promocodeAdapter;
    private List<PromocodeModel> promocodeModelList = new ArrayList<PromocodeModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_code);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_promocode));
        promocodeEditText = findViewById(R.id.promocodeTextView);
        promocodeRecycleViewList = findViewById(R.id.recycler_promocode_list);
        applyTextView = findViewById(R.id.promocodeApplyTextView);
        headingTextView = findViewById(R.id.promocodeHeading);
        failurePromocodeTextView = findViewById(R.id.promocodeFailureTextView);
        promocodeRecycleViewList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        promocodeRecycleViewList.setLayoutManager(mLayoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(promocodeRecycleViewList.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.custom_divider));
        promocodeRecycleViewList.addItemDecoration(divider);
        promocodeRecycleViewList.setItemAnimator(new DefaultItemAnimator());
        promocodeAdapter = new PromocodeAdapter(currentActivity, promocodeModelList);
        promocodeRecycleViewList.setAdapter(promocodeAdapter);
        getPromocodeList();
        promocodeAdapter.setOnItemClickListner(this);
        setTextChangeListener();

    }

    @Override
    protected void initContext() {
        context = PromoCodeActivity.this;
        currentActivity = PromoCodeActivity.this;
    }

    @Override
    protected void initListners() {
        applyTextView.setOnClickListener(this);

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.promocodeApplyTextView:
                if (isMandatoryField()) {
                    PromocodeModel promocodeModel = new PromocodeModel();
                    promocodeModel.setPromocode(promocodeEditText.getText().toString());
                    applyPromocode(promocodeModel);
                }
                break;
        }


    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    @Override
    public void onItemClick(int position, View v) {
        PromocodeModel promocodeModel = promocodeModelList.get(position);
        promocodeEditText.setText(promocodeModel.getPromocode());
        applyPromocode(promocodeModel);


    }

    @Override
    public void onItemLongClick(int position, View v) {

    }

    private void setTextChangeListener() {
        promocodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (promocodeEditText.getText() != null && !promocodeEditText.getText().toString().isEmpty()) {
                    applyTextView.setVisibility(View.VISIBLE);
                } else {
                    applyTextView.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getPromocodeList() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put("user_id", SharedPreferenceUtils.getInstance(PromoCodeActivity.this).getString(AppConstants.USER_ID));
            Log.e("jsonPromocodeList", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_ALL_PROMOCODE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successfull fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        Gson gson = new Gson();
                        List<PromocodeModel> promocodeTempList = Arrays.asList(gson.fromJson(response.getJSONArray(MESSAGE).toString(), PromocodeModel[].class));
                        promocodeModelList.clear();
                        promocodeModelList.addAll(promocodeTempList);
                        promocodeAdapter.updateAdapter(promocodeModelList);

                    } else {
                        cancelProgressDialog();
                        ((BaseActivity) currentActivity).logTesting("fetch Service error", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    public void applyPromocode(PromocodeModel promocodeModel) {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        ;
        try {
            jsons = new JSONObject();
            jsons.put("promocode", promocodeModel.getPromocode());
            Log.e("applyPromocode", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, APPLY_PROMOCODE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successfull fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        Gson gson = new Gson();
                        PromocodeModel promocodeModel = gson.fromJson(response.getJSONObject(MESSAGE).toString(), PromocodeModel.class);
                        Intent intent = new Intent();
                        intent.putExtra(AppConstants.MODEL_OBJ, promocodeModel);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        cancelProgressDialog();
                        Toast.makeText(context, "Promocode is not valid", Toast.LENGTH_LONG).show();
                        promocodeEditText.setText("");
                        ((BaseActivity) currentActivity).logTesting("Promocode is not valid", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                failurePromocodeTextView.setVisibility(View.VISIBLE);
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    private boolean isMandatoryField() {
        if (promocodeEditText.getText() == null || promocodeEditText.getText().toString().isEmpty()) {
            return false;
        }
        return true;

    }
}

package com.app.yesbeautyapp.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.PlaceOrderModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class PaymentMethodActivity extends BaseActivity {

    private ArrayList<ProductModel> productModelArrayList;
    private TextView paymentModeTV;
    private TextView paymentSummaryTV;
    private TextView lblServiceCostTV;
    private TextView serviceCostTV;
    private TextView lblProductCostTV;
    private TextView productCostTV;
    private TextView lblDispCostTV;
    private TextView dispCostTV;
    private TextView lblDiscountTV;
    private TextView discountTV;
    private TextView lblTotalAmtTV;
    private TextView totalAmtTV;
    private Button btnProceed;
    private TextView labelCouponCodeTV;
    private EditText editCouponCode;
    private Button btnApplyCoupon;
    private TextView labelReferralCodeTV;
    private EditText editReferralCode;
    private Button btnApplyReferral;
    private RadioGroup radioGroup;
    private String paymentMethod;
    private double discountAmount;
    private String couponCode;
    private String scheduleDate;
    private String scheduleTime;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
    }

    @Override
    protected void initViews() {
        settingTitle("Payment");
        productModelArrayList = CartProductModel.getInstance().getProductModelArrayList();
        if (getIntent() != null) {
            scheduleDate = getIntent().getStringExtra("scheduleDate");
            scheduleTime = getIntent().getStringExtra("scheduleTime");
        }
        paymentSummaryTV = findViewById(R.id.paymentSummaryTV);
        lblServiceCostTV = findViewById(R.id.lblServiceCostTV);
        serviceCostTV = findViewById(R.id.serviceCostTV);
        lblProductCostTV = findViewById(R.id.lblProductCostTV);
        productCostTV = findViewById(R.id.productCostTV);
        lblDispCostTV = findViewById(R.id.lblDispCostTV);
        dispCostTV = findViewById(R.id.dispCostTV);
        lblDiscountTV = findViewById(R.id.lblDiscountTV);
        discountTV = findViewById(R.id.discountTV);
        lblTotalAmtTV = findViewById(R.id.lblTotalAmtTV);
        totalAmtTV = findViewById(R.id.totalAmtTV);
        paymentModeTV = findViewById(R.id.paymentModeTV);
        btnProceed = findViewById(R.id.btnProceed);
        radioGroup = findViewById(R.id.radioGroup);
        labelCouponCodeTV = findViewById(R.id.labelCouponCodeTV);
        editCouponCode = findViewById(R.id.editCouponCode);
        btnApplyCoupon = findViewById(R.id.btnApplyCoupon);
        labelReferralCodeTV = findViewById(R.id.labelReferralCodeTV);
        editReferralCode = findViewById(R.id.editReferralCode);
        btnApplyReferral = findViewById(R.id.btnApplyReferral);

        FontUtils.changeFont(context, paymentSummaryTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, lblServiceCostTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, serviceCostTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblProductCostTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, productCostTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblDispCostTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, dispCostTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblDiscountTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, discountTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblTotalAmtTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, paymentModeTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, paymentModeTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, btnProceed, FONT_OPEN_SANS_SEMIBOLD_TTF);

        FontUtils.changeFont(context, labelCouponCodeTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, editCouponCode, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnApplyCoupon, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, labelReferralCodeTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, editReferralCode, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnApplyReferral, FONT_OPEN_SANS_SEMIBOLD_TTF);

        if (productModelArrayList != null) {
            serviceCostTV.setText("" + (int) getServiceAmt());
            productCostTV.setText("" + (int) getProductAmt());
            totalAmtTV.setText("Rs." + (int) totalProductAmount() + "/-");
        }

        paymentMethod = "Cash After Service";

    }

    @Override
    protected void initContext() {
        context = PaymentMethodActivity.this;
        currentActivity = PaymentMethodActivity.this;
    }

    @Override
    protected void initListners() {
        btnProceed.setOnClickListener(this);
        btnApplyCoupon.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    paymentMethod = rb.getText().toString();
                    //Toast.makeText(context, paymentMethod, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnProceed: {
                toHideKeyboard();
                if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                    initPlaceOrderModel();
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                }
                break;
            }
            case R.id.btnApplyCoupon: {
                toHideKeyboard();
                if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                    if (isMandatoryFields()) {
                        couponCode = editCouponCode.getText().toString();
                        applyCoupon();
                    }
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                }
                break;
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    private void initPlaceOrderModel() {
        double totalAmount = totalProductAmount();
        PlaceOrderModel placeOrderModel = new PlaceOrderModel();
        placeOrderModel.setUserId(SharedPreferenceUtils.getInstance(context).getString(USER_ID));
        placeOrderModel.setProductModelArrayList(CartProductModel.getInstance().getProductModelArrayList());
        placeOrderModel.setTotalAmount(((int) (totalAmount - discountAmount)) + "");
        placeOrderModel.setDiscount(discountAmount + "");
        placeOrderModel.setDiscode(couponCode);
        placeOrderModel.setPaymentMode(paymentMethod);
        placeOrderModel.setPaymentStatus("sucess");
        placeOrderModel.setTnxId(System.currentTimeMillis() + "");
        placeOrderModel.setScheduleDate(scheduleDate);
        placeOrderModel.setScheduleTime(scheduleTime);
        proceedBooking(placeOrderModel);
    }


    private void proceedBooking(PlaceOrderModel placeOrderModel) {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonProceedBooking = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        try {
            jsonProceedBooking = new JSONObject(gson.toJson(placeOrderModel));
            Log.e("jsonProceedBooking", jsonProceedBooking.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_PROCEED_BOOKING = PROCEED_BOOKING_URL;
        JsonObjectRequest proceedBookingRequest = new JsonObjectRequest(Request.Method.POST, URL_PROCEED_BOOKING, jsonProceedBooking, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                cancelProgressDialog();
                try {
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                String message = messageObj.getString("message");
                                if (message != null && message.equalsIgnoreCase("success")) {
                                    toOpenBookingSuccessActivity(messageObj.getString("orderId"));
                                } else {
                                    couponCode = "";
                                    toast("Failed to place your order.", true);
                                }
                            } else {
                                couponCode = "";
                                toast("Failed to place your order.", true);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                //toOpenBookingSuccessActivity();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(proceedBookingRequest);
    }

    private boolean isMandatoryFields() {

        editCouponCode.setError(null);
        if (editCouponCode.getText().toString().isEmpty()) {
            editCouponCode.setError(getResources().getString(R.string.error_editText_coupon));
            editCouponCode.requestFocus();
            return false;
        }
        return true;
    }

    private void applyCoupon() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonApplyCoupon = null;
        try {
            jsonApplyCoupon = new JSONObject();
            jsonApplyCoupon.put("code", couponCode);
            jsonApplyCoupon.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            Log.e("jsonApplyCoupon", jsonApplyCoupon.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_APPLY_COUPON = URL_APPLYCOUPON;

        JsonObjectRequest profileUpdateRequest = new JsonObjectRequest(Request.Method.POST, URL_APPLY_COUPON, jsonApplyCoupon, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                String discountPer = messageObj.getString("discount_per");
                                if (!TextUtils.isEmpty(discountPer)) {
                                    double discount = Double.parseDouble(discountPer);
                                    double totalAmount = totalProductAmount();
                                    discountAmount = ((totalAmount * discount) / 100);
                                    totalAmtTV.setText("Rs." + (int) (totalAmount - discountAmount) + "/-");
                                    discountTV.setText("" + (int) discountAmount);
                                    editCouponCode.setText("");
                                    toast("Coupon Code Applied Successfully.", true);
                                }
                            } else {
                                couponCode = "";
                                toast("Invalid Coupon Code", true);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                couponCode = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(profileUpdateRequest);
    }


    private double totalProductAmount() {
        double totalAmount = 0.0;
        if (productModelArrayList == null) return totalAmount;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String productCost = productModel.getProductCost();
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(productCost)) productCost = "0";
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = (productModel.getProductQty() * Double.parseDouble(productCost))
                        + (productModel.getServiceQty() * Double.parseDouble(serviceCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }


    private double getServiceAmt() {
        double totalAmount = 0.0;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = (productModel.getServiceQty() * Double.parseDouble(serviceCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    private double getProductAmt() {
        double totalAmount = 0.0;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String productCost = productModel.getProductCost();
                if (TextUtils.isEmpty(productCost)) productCost = "0";
                totalAmt = (productModel.getProductQty() * Double.parseDouble(productCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    private void toOpenBookingSuccessActivity(String orderId) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("bookingId", orderId);
        startActivity(currentActivity, BookingSuccessActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
        finish();
    }
}

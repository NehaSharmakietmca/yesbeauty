package com.app.yesbeautyapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.ReviewOrderDetailAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.dao.CartDataSource;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.model.CartModel;
import com.app.yesbeautyapp.model.PromocodeModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.LoginUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;

public class ReviewOrderDetailsActivity extends BaseActivity {

    private RecyclerView recyclerReviewOrder;
    private ArrayList<CartModel> cartModelArrayList;
    private ReviewOrderDetailAdapter orderDetailAdapter;
    private EditText editPromoCode;
    private TextView amountLbl;
    private TextView totalAmtTV;
    private Button btnCheckOut;
    private TextView totalamountTextView;
    private TextView inclusiveTextView;
    private TextView removePromocodeTextView;
    private LinearLayout amountLinearLayout;
    private LinearLayout discountLinearLayout;
    private TextView discountTextView;
    private TextView discountValueTextview;
    private TextView amountTextView;
    private TextView amountValueTextView;
    private TextView appliedPromocodeTextView;
    private double discount =0;

    private double totalAmt = 0;
    private boolean isBookNow;
    private TextView payAfterServiceTV;
    private CartDataSource cartDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order_details);
        cartDataSource = new CartDataSource(this);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_review));
        settingHomeButton();
        if (getIntent().getExtras() != null) {
            cartModelArrayList = getIntent().getExtras().getParcelableArrayList(CART_MODEL_OBJ_ARRAY);
            isBookNow = getIntent().getExtras().getBoolean("isBookNow");
        }
        if (cartModelArrayList == null) {
            cartModelArrayList = new ArrayList<>();
        }
        btnCheckOut = findViewById(R.id.btnCheckout);
        editPromoCode = findViewById(R.id.promocodeTextView);
        amountLbl = findViewById(R.id.totalamountTextView);
        totalAmtTV = findViewById(R.id.text_amount);
        inclusiveTextView = findViewById(R.id.textInclusiveTax);
        payAfterServiceTV = findViewById(R.id.payAfterServiceTextView);
        removePromocodeTextView = findViewById(R.id.removePromocodeTextView);
        amountTextView = findViewById(R.id.amountTextView);
        amountValueTextView = findViewById(R.id.amountValueTextView);
        discountTextView = findViewById(R.id.discountTextView);
        discountValueTextview = findViewById(R.id.discountValueValueTextView);
        appliedPromocodeTextView = findViewById(R.id.applyPromocodeTextView);
        discountLinearLayout = findViewById(R.id.lin_discount);
        amountLinearLayout = findViewById(R.id.lin_amount);



        recyclerReviewOrder = findViewById(R.id.recyclerReview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(currentActivity);
        recyclerReviewOrder.setLayoutManager(layoutManager);
        orderDetailAdapter = new ReviewOrderDetailAdapter(currentActivity, cartModelArrayList);
        recyclerReviewOrder.setAdapter(orderDetailAdapter);

        FontUtils.changeFont(context, totalAmtTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnCheckOut, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, amountLbl, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, totalAmtTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, removePromocodeTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, amountTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, amountValueTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, discountTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, discountValueTextview, FONT_OPEN_SANS_REGULAR_TTF);

        for (int i = 0; i < cartModelArrayList.size(); i++) {
            CartModel cartModel = cartModelArrayList.get(i);
            if (cartModel == null) continue;
            String totalAmtStr = cartModel.getTotalAmount();
            if (TextUtils.isEmpty(totalAmtStr)) continue;
            totalAmt += Double.parseDouble(totalAmtStr);
        }
        String.format("%.2f",totalAmt);
        String amountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmt + "/-";
        totalAmtTV.setText(amountValue);
        totalAmtTV.setText(String.valueOf(amountValue));
        amountValueTextView.setText(amountValue);

        String discountValue = getResources().getString(R.string.rupees_symbol)+" "+0.0 + "/-";
        discountValueTextview.setText(discountValue);

        if(amountTextView!= null){
            if(cartModelArrayList!= null && cartModelArrayList.size()>1)
                amountTextView.setText("Total Amount");
            else{
                amountTextView.setText("Total Amount ");
            }
        }
    }

    @Override
    protected void initContext() {
        context = ReviewOrderDetailsActivity.this;
        currentActivity = ReviewOrderDetailsActivity.this;
    }

    @Override
    protected void initListners() {
        btnCheckOut.setOnClickListener(this);
        editPromoCode.setOnClickListener(this);
        removePromocodeTextView.setOnClickListener(this);
       // btnApplyCoupon.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.btnApplyCoupon: {
                toHideKeyboard();
                if (!LoginUtils.isLogin(context)) {
                    toOpenLoginActivity();
                    return;
                }
                if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                    if (isMandatoryFields()) {
                        applyCoupon(editCouponCode.getText().toString());
                    }
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                }
                break;
            }*/
            case R.id.btnCheckout: {
                if (!LoginUtils.isLogin(context)) {
                    toOpenLoginActivity();
                    return;
                }else{
                    alert(currentActivity, "Alert", getResources().getString(R.string.alert_message_place_order), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), true, true, ALERT_TYPE_CONFIRM_ORDER);
                }
                break;
            }
            case R.id.promocodeTextView: {
                if (!LoginUtils.isLogin(context)) {
                    toOpenLoginActivity();
                    return;
                }else{
                    startActivity(currentActivity, PromoCodeActivity.class, null, true, REQUEST_TAG_PROMOCODE, true, ANIMATION_SLIDE_UP);

                }
                break;
            }
            case R.id.removePromocodeTextView: {
                removePromocodeTextView.setVisibility(View.GONE);
                appliedPromocodeTextView.setVisibility(View.GONE);
               // discountLinearLayout.setVisibility(View.GONE);
               // amountLinearLayout.setVisibility(View.GONE);

                discount = 0;
                String.format("%.2f",discount);
                String discountValue = getResources().getString(R.string.rupees_symbol)+" "+discount + "/-";
                discountValueTextview.setText(String.valueOf(discountValue));
                String.format("%.2f",totalAmt);
                String amountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmt + "/-";
                amountValueTextView.setText(String.valueOf(amountValue));
                editPromoCode.setText("");
                String.format("%.2f",totalAmt);
                String totalAmountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmt + "/-";
                totalAmtTV.setText(totalAmountValue);
                break;
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {
        if(alertType == ALERT_TYPE_CONFIRM_ORDER){
            BookingModel bookingModel = new BookingModel();
            bookingModel.setDiscountAmount(String.valueOf(discount));
            bookingModel.setTotalAmount(String.valueOf(totalAmt));
            bookingModel.setPaymentType("COD");
            bookingModel.setUserId(Integer.parseInt(SharedPreferenceUtils.getInstance(this).getString(AppConstants.USER_ID)));
            bookingModel.setServiceList(cartModelArrayList);
            bookService(bookingModel);
        }

    }

    private void toCheckLoginAndNavigate() {

    }

    private void toOpenOtpActivity() {
        if (bundle == null) {
            bundle = new Bundle();
        }
        String userPhone = "";
        if (cartModelArrayList != null && !cartModelArrayList.isEmpty()) {
            CartModel cartModel = cartModelArrayList.get(0);
            if (cartModel != null) {
                userPhone = cartModel.getPhoneNumber();
            }
        }
        if (TextUtils.isEmpty(userPhone)) {
            toast(getString(R.string.error_editText_phone), true);
            return;
        }
        bundle.putString(USER_PHONE, userPhone);
        startActivity(currentActivity, OtpValidation.class, bundle, true, REQUEST_TAG_OTP_VALIDATION, true, ANIMATION_SLIDE_UP);
    }

    private void toOpenLoginActivity() {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean("isCheckout", true);
        startActivity(currentActivity, LoginActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
    }

    private void toOpenPaymentActivity() {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putBoolean("isBookNow", false);
        bundle.putDouble("totalAmt", totalAmt);
        bundle.putParcelableArrayList(CART_MODEL_OBJ_ARRAY, cartModelArrayList);
      //  startActivity(currentActivity, PaymentMethodActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
    }

    private boolean isMandatoryFields() {

        editPromoCode.setError(null);
        if (editPromoCode.getText().toString().isEmpty()) {
            editPromoCode.setError(getResources().getString(R.string.error_editText_coupon));
            editPromoCode.requestFocus();
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAG_PROMOCODE && resultCode == RESULT_OK) {
          PromocodeModel promocodeModel =  data.getParcelableExtra(MODEL_OBJ);
          if(promocodeModel != null){
              editPromoCode.setText(promocodeModel.getPromocode());
              removePromocodeTextView.setVisibility(View.VISIBLE);
              amountLinearLayout.setVisibility(View.VISIBLE);
              discountLinearLayout.setVisibility(View.VISIBLE);

              discount = Double.parseDouble(promocodeModel.getAmount());
              String.format("%.2f",discount);
              String discountValue = getResources().getString(R.string.rupees_symbol)+" "+discount + "/-";
              discountValueTextview.setText(String.valueOf(discountValue));
              String.format("%.2f",totalAmt);
              String amountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmt + "/-";
              amountValueTextView.setText(String.valueOf(amountValue));
             // discountValueTextview.setText(promocodeModel.getDiscount());
              appliedPromocodeTextView.setVisibility(View.VISIBLE);
              double totalAmount = (totalAmt- Double.parseDouble(promocodeModel.getAmount()));

              String.format("%.2f",totalAmount);
              String totalAmountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmount + "/-";
              totalAmtTV.setText(totalAmountValue);
              totalAmt =totalAmount;

          }
        }
    }

    private void bookService(BookingModel bookingModel) {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject bookingServiceRequest = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            bookingServiceRequest = new JSONObject(gson.toJson(bookingModel));
            Log.e("bookingModel", bookingServiceRequest.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest bookingRequest = new JsonObjectRequest(Request.Method.POST, URL_PLACE_ORDER, bookingServiceRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    cancelProgressDialog();
                    String message = response.getString(RESPONCE_MESSAGE);
                    if (response.getBoolean(RESPONCE_ERROR) || message.isEmpty()) {
                       // alert(currentActivity, getString(R.string.msg_reg_failed), getString(R.string.msg_reg_failed), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_NO_NETWORK);
                    } else {
                        if(!isBookNow){
                            if(cartDataSource != null){
                                cartDataSource.deleteCart();
                            }
                        }
                        Gson gson = new Gson();
                        BookingModel bookingModel1 = gson.fromJson(response.getJSONObject(MESSAGE).toString(), BookingModel.class);
                        if (bundle == null) {
                            bundle = new Bundle();
                        }
                        bundle.putParcelable(MODEL_OBJ, bookingModel1);
                        startActivity(currentActivity, ConfirmationActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getResources().getString(R.string.nwk_error_sign_up), true);
                logTesting(getResources().getString(R.string.nwk_error_sign_up), error.toString(), Log.ERROR);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };
        AplusoneApplication.getInstance().addToRequestQueue(bookingRequest);
    }
}

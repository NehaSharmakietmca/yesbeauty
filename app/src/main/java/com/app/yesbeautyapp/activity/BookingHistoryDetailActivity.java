package com.app.yesbeautyapp.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.BookingHistoryDetailsAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.BookingHistoryListener;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.model.CartModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;

public class BookingHistoryDetailActivity extends BaseActivity implements RecyclerClickListner ,BookingHistoryListener {
    private RecyclerView bookingHistoryRecyclerView;
    private BookingHistoryDetailsAdapter bookingHistoryDetailsAdapter;
    private TextView bookingIdTextView;
    private TextView bookingIdLabelTextView;
    private TextView bookingDateLabelTextView;
    private TextView bookingDateValueTextView;
    private TextView bookingTotalAmountLabelTextView;
    private TextView bookingTotalAmountValueTextView;
    private TextView bookingdiscountLabelTextView;
    private TextView bookingdiscountValueTextView;
    private TextView bookingPayableAmountLabelTextView;
    private TextView bookingPayableAmountvalueTextView;
    private TextView bookingPaymentTypeTextView;
    private TextView bookingPaymentTypeValueTextView;
    private BookingModel bookingModel;
    private List<CartModel> cartModelList = new ArrayList<CartModel>();
    private int reschedulePosition;
    private int cancelPosition;
    private RadioButton reason1;
    private RadioButton reason2;
    private RadioButton reason3;
    private RadioButton reason4;
    private RadioGroup radioGroup;
    private ImageView cancelImageView;
    private CartModel cancelCartModel;
    private Button submitButton;
    private BottomSheetDialog reasonEditDialog;


    @Override
    protected void initViews() {
        settingTitle(getString(R.string.booking_history));
        bookingHistoryRecyclerView = findViewById(R.id.recyler_booking_history_details);
        bookingIdTextView = findViewById(R.id.booking_history_details_booking_id);
        bookingIdLabelTextView = findViewById(R.id.text_booking_history_id_label);
        bookingDateLabelTextView = findViewById(R.id.text_booking_history_details_booking_date_label);
        bookingDateValueTextView = findViewById(R.id.booking_history_details_booking_date_value);
        bookingTotalAmountLabelTextView = findViewById(R.id.text_booking_history_total_amount_label);
        bookingTotalAmountValueTextView = findViewById(R.id.booking_history_details_total_amount_value);
        bookingdiscountLabelTextView = findViewById(R.id.text_booking_history_discount_label);
        bookingdiscountValueTextView = findViewById(R.id.booking_history_details_discount_value);
        bookingPayableAmountLabelTextView = findViewById(R.id.text_booking_history_details_payable_amount_label);
        bookingPayableAmountvalueTextView = findViewById(R.id.text_booking_history_details_payable_amount_value);
        bookingPaymentTypeTextView = findViewById(R.id.text_booking_history_details_payment_type_label);
        bookingPaymentTypeValueTextView = findViewById(R.id.text_booking_history_details_payment_type_value);


        if (getIntent().getExtras() != null) {
            bookingModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
            if (bookingModel != null) {
                bookingIdTextView.setText(bookingModel.getBookingId());
              double amount =  Double.parseDouble(bookingModel.getTotalAmount())- Double.parseDouble(bookingModel.getDiscountAmount());
                String.format("%.2f",amount);
                String amountValue = getResources().getString(R.string.rupees_symbol)+" "+amount + "/-";
                bookingPayableAmountvalueTextView.setText(String.valueOf(amountValue));
                bookingDateValueTextView.setText(bookingModel.getTimestamp());
                bookingdiscountValueTextView.setText(bookingModel.getDiscountAmount());
                bookingTotalAmountValueTextView.setText(bookingModel.getTotalAmount());
                bookingPaymentTypeValueTextView.setText(bookingModel.getPaymentType());
                cartModelList = bookingModel.getServiceList();
            }
        }
        bookingHistoryRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        bookingHistoryRecyclerView.setLayoutManager(mLayoutManager);
        bookingHistoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        bookingHistoryDetailsAdapter = new BookingHistoryDetailsAdapter(currentActivity, cartModelList,this);
        bookingHistoryRecyclerView.setAdapter(bookingHistoryDetailsAdapter);

        FontUtils.changeFont(context, bookingIdTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingIdLabelTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingDateLabelTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingDateValueTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingTotalAmountLabelTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingTotalAmountValueTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingdiscountLabelTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingdiscountValueTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingPayableAmountLabelTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingPayableAmountvalueTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingPaymentTypeTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingPaymentTypeValueTextView, FONT_OPEN_SANS_REGULAR_TTF);
//        int radioGroupButtonId = radioGroup.getCheckedRadioButtonId();

    }

    @Override
    protected void initContext() {
        context = BookingHistoryDetailActivity.this;
        currentActivity = BookingHistoryDetailActivity.this;

    }

    @Override
    protected void initListners() {

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onAlertClicked(int alertType) {
        if (alertType == ALERT_TYPE_CANCEL_BOOKING) {

        }

    }


    @Override
    public void onItemClick(int position, View v) {

    }

    @Override
    public void onItemLongClick(int position, View v) {

    }



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void rescheduleClickListener(CartModel cartModel,int pos) {
        reschedulePosition =pos;
        if(bundle == null){
            bundle = new Bundle();
        }
        bundle.putParcelable(MODEL_OBJ,cartModel);
        startActivity(BookingHistoryDetailActivity.this,RescheduleBookingActivity.class,bundle,true,REQUEST_TAG_RESCHEDULE, true,ANIMATION_SLIDE_UP);


    }

    @Override
    public void cancelClickListener(CartModel cartModel ,int pos) {
        cancelPosition = pos;
        if(bundle == null){
            bundle = new Bundle();
        }
        cancelCartModel = cartModelList.get(cancelPosition);
       // cancelBooking(cartModel);
        showCancelReasonDialog();
       // alert(currentActivity, getResources().getString(R.string.alert_title_cancel), getResources().getString(R.string.alert_title_cancel_booking_message), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_CANCEL_BOOKING);
        //bundle.putParcelable(MODEL_OBJ,cartModel);
       // startActivity(BookingHistoryDetailActivity.this,RescheduleBookingActivity.class,bundle,true,REQUEST_TAG_CANCEL, true,ANIMATION_SLIDE_UP);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_TAG_RESCHEDULE){
            if(resultCode == RESULT_OK){
                getBookingHistoryList();
            }
        }

    }

    public void cancelBooking(CartModel cartModel) {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        ;
        try {
            jsons = new JSONObject();
            jsons.put("id", cartModel.getId());
            jsons.put("actiontext",cartModel.getActionNext());
            Log.e("cancelBooking", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_CANCEL_BOOKING, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    //cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successful fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                      //  Gson gson = new Gson();
                        getBookingHistoryList();
                       // PromocodeModel promocodeModel = gson.fromJson(response.getJSONObject(MESSAGE).toString(), PromocodeModel.class);
                       // Intent intent = new Intent();
                       // intent.putExtra(AppConstants.MODEL_OBJ, promocodeModel);
                        //setResult(RESULT_OK, intent);
                       // finish();
                    } else {
                        cancelProgressDialog();
                        Toast.makeText(context, "Promocode is not valid", Toast.LENGTH_LONG).show();
                        ((BaseActivity) currentActivity).logTesting("CancelBooking", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
              //  failurePromocodeTextView.setVisibility(View.VISIBLE);
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }
    private void getBookingHistoryList()  {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject bookingHistory = new JSONObject();
        try {
            bookingHistory.put("user_id", SharedPreferenceUtils.getInstance(BookingHistoryDetailActivity.this).getString(AppConstants.USER_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_BOOKING_HISTORY, bookingHistory, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successfull fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        Gson gson = new Gson();
                        List<BookingModel> bookingModelTempList = Arrays.asList(gson.fromJson(response.getJSONArray(MESSAGE).toString(), BookingModel[].class));
                        getSelectedBookingModel(bookingModelTempList);

                    } else {
                        cancelProgressDialog();
                        ((BaseActivity) currentActivity).logTesting("fetch Service error", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }
    private void getSelectedBookingModel(List<BookingModel> bookingModelList){
        for(BookingModel listBookingModel : bookingModelList){
            if(listBookingModel.getBookingId().equals(bookingModel.getBookingId())){
                if(listBookingModel != null) {
                    cartModelList.clear();
                    bookingHistoryDetailsAdapter.notifyDataSetChanged();
                    cartModelList.addAll(listBookingModel.getServiceList());
                    bookingHistoryDetailsAdapter.notifyDataSetChanged();

                }
            }
        }
    }
    public void showCancelReasonDialog() {
        if(reasonEditDialog != null){
            reasonEditDialog.setCancelable(false);
            reasonEditDialog.setCanceledOnTouchOutside(false);

        }
        View view = getLayoutInflater().inflate(R.layout.fragment_booking_caccel_reason, null);
        reasonEditDialog = new BottomSheetDialog(this);
        reason1 = view.findViewById(R.id.reason_1);
        reason2 = view.findViewById(R.id.reason_2);
        reason3 = view.findViewById(R.id.reason_3);
        reason4 = view.findViewById(R.id.reason_4);
        radioGroup = view.findViewById(R.id.reason_radio_group);
        submitButton = view.findViewById(R.id.reason_submit_button);
        cancelImageView =  view.findViewById(R.id.reason_cancel);
        if(radioGroup != null) {
            final int radioGroupButtonId = radioGroup.getCheckedRadioButtonId();
            setRadioButtonSelection(radioGroupButtonId);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    setRadioButtonSelection(i);

                }
            });
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   if(reasonEditDialog != null){
                       reasonEditDialog.dismiss();
                   }
                   cancelBooking(cancelCartModel);
            }
        });
        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(reasonEditDialog != null){
                    reasonEditDialog.dismiss();
                }
            }
        });

        reasonEditDialog.setContentView(view);

        reasonEditDialog.show();
    }

    private void setRadioButtonSelection(int i) {
        switch (i) {
            case -1:

               submitButton.setEnabled(false);

                break;
            case R.id.reason_1:
                if(submitButton != null){
                    submitButton.setEnabled(true);
                }
                if(reason1 != null) {
                    cancelCartModel.setActionNext(reason1.getText().toString());
                }
                break;
            case R.id.reason_2:

                if(submitButton != null){
                    submitButton.setEnabled(true);
                }
                if(reason1 != null) {
                    cancelCartModel.setActionNext(reason2.getText().toString());
                }

                break;
            case R.id.reason_3:
                if(submitButton != null){
                    submitButton.setEnabled(true);
                }
                if(reason1 != null) {
                    cancelCartModel.setActionNext(reason3.getText().toString());
                }

                break;
            case R.id.reason_4:
                if(reasonEditDialog != null){
                    reasonEditDialog.dismiss();
                }
                showCancelReasonEditDialog();
            break;



        }
    }
    public void showCancelReasonEditDialog() {
        View view = getLayoutInflater().inflate(R.layout.fragment_booking_reason_other_issue, null);
        ImageView backButton = view.findViewById(R.id.btn_other_issue_back);
        final Button otherReasonButton = view.findViewById(R.id.other_issue_reason_button);
        final EditText otherResonEdit = view.findViewById(R.id.other_issue_edit);
        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog != null){
                    dialog.dismiss();
                }
                showCancelReasonDialog();
            }
        });
        otherReasonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(otherResonEdit == null || otherResonEdit.getText()== null || otherReasonButton.getText().toString().isEmpty()){
                    Toast.makeText(BookingHistoryDetailActivity.this,"Please give us suggestion",Toast.LENGTH_LONG).show();
                    return;
                }else{
                    dialog.dismiss();
                    cancelCartModel.setActionNext(otherResonEdit.getText().toString());
                    cancelBooking(cancelCartModel);
                }


            }
        });

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


}

package com.app.yesbeautyapp.activity;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.goodiebag.pinview.Pinview;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.CancellingResendOtpThread;
import com.app.yesbeautyapp.model.LoginOrSignUpModel;
import com.app.yesbeautyapp.receiver.IncomingSms;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.Helper;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;


public class OtpValidation extends BaseActivity implements View.OnClickListener, CancellingResendOtpThread {
    private Button btnSubmit;
    private TextView btnResendOtp;
    private TextView sentMobileNumberVerificationCode;
    private Pinview pinview;
    private LoginOrSignUpModel loginOrSignUpModel;
    public static Runnable r;
    public static Handler handler;
    public String textMobileNo;
    private int otp;
    private String suppliededOtp;

    private BroadcastReceiver smsReciever;
    private Boolean recieverRegistered = false;

    private int oldOtp;

    static Handler handlerButtonVisibility;
    public static Runnable rButtonVisibility;
    private CountDownTimer countDownTimer;

    private TextView textViewTimer;
    private CountDownTimer timerForUnregisteringBroadcastReciever;
    private boolean istimerForUnregisteringBroadcastRecieverrunning = false;
    private int intNumberOfTimesResendPress = 0;
    private TextView appNameTextView;

    private boolean isCheckout = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_validate);
        settingTitle("Verify OTP");
    }

    public void onCreateWork() {
        Bundle data = getIntent().getExtras();
        if (data == null) {
            return;
        }
        loginOrSignUpModel = data.getParcelable("loginModel");
        textMobileNo = data.getString(USER_MOBILE_NO);
        sentMobileNumberVerificationCode.setText("Please type verification code sent to " + " +91" + textMobileNo);
        handler = new Handler();

        SharedPreferenceUtils.getInstance(context).putString(USER_MOBILE_NO, textMobileNo.toString().trim());
        otp = Helper.randomNumberCreation(100000, 999999);
        SharedPreferenceUtils.getInstance(context).putString(AppConstants.KEY_OTP, String.valueOf(otp));
        smsReciever = new IncomingSms(OtpValidation.this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(EVENT_SMS_RECEIVED);
        intentFilter.setPriority(PRIORITY_HIGH);
        registerReceiver(smsReciever, intentFilter);

        if (Validator.isNetworkAvailable(context)) {
            btnResendOtp.setClickable(false);
            btnResendOtp.setAlpha(0.5f);
            functionEnablingButtons();
            functionInitializingCountDownTimer();
            sendMessageRequest();
            recieverRegistered = true;
            functionForUnregisteringBroadcastReceiever();
        } else {
            alert(currentActivity, getString(R.string.alert_message_no_network), getString(R.string.alert_message_no_network), getString(R.string.alert_ok_button_text_no_network), getString(R.string.alert_cancel_button_text_no_network), false, false, ALERT_TYPE_NO_NETWORK);
        }
    }

    @Override
    protected void initViews() {
        bundle = new Bundle();

        btnSubmit = (Button) findViewById(R.id.submitButton);
        pinview = findViewById(R.id.pinview);
        btnResendOtp = (TextView) findViewById(R.id.resendOtpButton);
        appNameTextView = (TextView) findViewById(R.id.text_app_name);
        sentMobileNumberVerificationCode = (TextView) findViewById(R.id.text_verification_code_sent_number);

        textViewTimer = (TextView) findViewById(R.id.textViewTimer);
        if (getIntent() != null && getIntent().getExtras() != null) {
            isCheckout = getIntent().getExtras().getBoolean("isCheckout", false);
        }
        FontUtils.changeFont(this, textViewTimer, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, btnResendOtp, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, sentMobileNumberVerificationCode, FONT_OPEN_SANS_SEMIBOLD_TTF);

        FontUtils.changeFont(this, btnSubmit, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, appNameTextView, FONT_OPEN_SANS_SEMIBOLD_TTF);

        onCreateWork();
    }

    @Override
    protected void initContext() {
        currentActivity = OtpValidation.this;
        context = OtpValidation.this;
    }

    @Override
    protected void initListners() {
        btnSubmit.setOnClickListener(this);
        btnResendOtp.setOnClickListener(this);

    }

    @Override
    public boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitButton: {
                toHideKeyboard();
                if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                    suppliededOtp = pinview.getValue();
                    if (suppliededOtp.equals("")) {
                        toast(getResources().getString(R.string.emptyOtp), true);
                    } else {
                        int otp = Integer.parseInt(SharedPreferenceUtils.getInstance(context).getString(KEY_OTP));
                        if (Integer.parseInt(suppliededOtp) == otp || Integer.parseInt(suppliededOtp) == 123456) {
                            if (recieverRegistered == true) {
                                unregisterReceiver(smsReciever);
                                recieverRegistered = false;
                            }
                            if (istimerForUnregisteringBroadcastRecieverrunning) {
                                timerForUnregisteringBroadcastReciever.cancel();
                                istimerForUnregisteringBroadcastRecieverrunning = false;
                            }
                            numberVerified();
                        } else {
                            toast(getResources().getString(R.string.messageOtpInvalid), true);
                        }
                    }
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, false, ALERT_TYPE_NO_NETWORK);
                }
                break;
            }
            case R.id.resendOtpButton: {
                toHideKeyboard();
                functionInitializingCountDownTimer();
                intNumberOfTimesResendPress = intNumberOfTimesResendPress + 1;

                if (intNumberOfTimesResendPress <= RESET_LIMIT) {
                    if (istimerForUnregisteringBroadcastRecieverrunning) {
                        timerForUnregisteringBroadcastReciever.cancel();
                        istimerForUnregisteringBroadcastRecieverrunning = false;
                    }
                    functionForUnregisteringBroadcastReceiever();

                    if (Validator.isNetworkAvailable(context)) {
                        sendMessageRequest();
                        btnResendOtp.setClickable(false);
                        btnResendOtp.setAlpha(0.5f);
                        functionEnablingButtons();
                    } else {
                        alert(currentActivity, getString(R.string.alert_message_no_network), getString(R.string.alert_message_no_network), getString(R.string.alert_ok_button_text_no_network), getString(R.string.alert_cancel_button_text_no_network), false, false, ALERT_TYPE_NO_NETWORK);
                    }
                }
                break;
            }
        }
    }

    @Override
    public void cancelHandler(boolean isCancel) {
        handlerButtonVisibility.removeCallbacks(rButtonVisibility);
        countDownTimer.cancel();
        timerForUnregisteringBroadcastReciever.cancel();
    }

    public void functionEnablingButtons() {
        rButtonVisibility = new Runnable() {
            @Override
            public void run() {
                btnResendOtp.setClickable(true);
                btnResendOtp.setAlpha(1);

            }
        };
        handlerButtonVisibility = new Handler();
        handlerButtonVisibility.postDelayed(rButtonVisibility, longTimeAfterWhichButtonEnable);
    }


    @Override
    public void onAlertClicked(int alertType) {

    }


    public void functionInitializingCountDownTimer() {
        textViewTimer.setVisibility(View.VISIBLE);
        countDownTimer = new CountDownTimer(longTotalVerificationTime, longOneSecond) {
            @Override
            public void onTick(long millisUntilFinished) {
                textViewTimer.setVisibility(View.VISIBLE);
                int remainingIntTime = (int) millisUntilFinished / 1000;
                String remainingTime = "";
                if (remainingIntTime < 10) {
                    remainingTime = "0" + remainingIntTime;
                } else {
                    remainingTime = "" + remainingIntTime;
                }
                textViewTimer.setText("00:" + remainingTime + " ");
            }

            @Override
            public void onFinish() {
                textViewTimer.setVisibility(View.INVISIBLE);
                pinview.setFocusable(true);

                pinview.requestFocus();
                pinview.setFocusableInTouchMode(true);
                pinview.invalidate();
            }
        }.start();
    }

    public void functionForUnregisteringBroadcastReceiever() {
        istimerForUnregisteringBroadcastRecieverrunning = true;
        timerForUnregisteringBroadcastReciever = new CountDownTimer(timeafterwhichRecieverUnregister, 120000) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                if (recieverRegistered == true) {
                    unregisterReceiver(smsReciever);
                    recieverRegistered = false;
                }
            }
        };
    }

    private void sendMessageRequest() {

        oldOtp = Integer.parseInt(SharedPreferenceUtils.getInstance(context).getString(KEY_OTP));
        String message = "Dear%20Customer,%20your%20verification%20OTP%20is%20" + oldOtp + "%20,%20Thanks%20for%20subscribing%20with%20BEAUTYNWISH";
        String url = "http://anysms.in/api.php?username=Beautynwish&password=869831&sender=BNWISH&sendto=" + textMobileNo + "&message=" + message;
        Log.e("sendMessageRequestUrl", url);

        new RequestTask().execute(url);
    }

    public void autoFillOtp(int otp) {

        final String stringOtp = String.valueOf(otp);
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                pinview.setValue(stringOtp);
                textViewTimer.setText("00:00" + " ");
                btnResendOtp.setClickable(true);
                btnResendOtp.setAlpha(1);
                cancelHandler(true);
                autoSubmitOtp();
            }
        });
    }

    public void autoSubmitOtp() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast(getResources().getString(R.string.messageNoValidated), true);
                numberVerified();
            }
        }, 1000);
    }

    // to do the operation
    public void numberVerified() {
        toast(getResources().getString(R.string.messageNoValidated), true);

        if (loginOrSignUpModel == null) {
            initSignUpModel();
            loginUser();
        } else {
            registerUser();
        }
    }

    private void initSignUpModel() {
        LoginOrSignUpModel.getInstance().setPhone(textMobileNo);
    }

    private void registerUser() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonSignUpRequest = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        try {
            jsonSignUpRequest = new JSONObject(gson.toJson(loginOrSignUpModel));
            Log.e("jsonSignUpRequest", jsonSignUpRequest.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_SIGN_UP = URL_REGISTER;
        JsonObjectRequest signUpRequest = new JsonObjectRequest(Request.Method.POST, URL_SIGN_UP, jsonSignUpRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_ID, messageObj.getString(USER_ID));
                                SharedPreferenceUtils.getInstance(currentActivity).putInteger(USERTYPE, messageObj.getInt(USERTYPE));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_MOBILE_NO, LoginOrSignUpModel.getInstance().getPhone());
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_EMAIL, LoginOrSignUpModel.getInstance().getEmail());
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, LoginOrSignUpModel.getInstance().getFname());
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_LAST_NAME, LoginOrSignUpModel.getInstance().getLname());
                                SharedPreferenceUtils.getInstance(context).putBoolean(AppConstants.IS_USER_LOGIN, true);

                                if (isCheckout) {
                                    finish();
                                } else {
                                    if (messageObj.getInt(USERTYPE) == 1) {
                                        startActivity(currentActivity, Dashboard.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                                    }
                                    finish();
                                }
                            } else {
                                alert(currentActivity, getString(R.string.msg_reg_failed), getString(R.string.msg_reg_failed), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_NO_NETWORK);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getResources().getString(R.string.nwk_error_sign_up), true);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };
        AplusoneApplication.getInstance().addToRequestQueue(signUpRequest);
    }


    private void loginUser() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonSignUpRequest = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            jsonSignUpRequest = new JSONObject(gson.toJson(LoginOrSignUpModel.getInstance()));
            Log.e("jsonSignUpRequest", jsonSignUpRequest.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_SIGN_UP = URL_LOGIN;
        JsonObjectRequest signUpRequest = new JsonObjectRequest(Request.Method.POST, URL_SIGN_UP, jsonSignUpRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_ID, messageObj.getString(USER_ID));
                                SharedPreferenceUtils.getInstance(currentActivity).putInteger(USERTYPE, messageObj.getInt(USERTYPE));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_MOBILE_NO, LoginOrSignUpModel.getInstance().getPhone());
                                //SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_EMAIL, messageObj.getString(USER_EMAIL));
                                //SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, messageObj.getString(USER_NAME));
                                //SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.ADDRESS, messageObj.getString(ADDRESS));
                                //SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_PROFILE_IMAGE, messageObj.getString(USER_PROFILE_IMAGE));
                                SharedPreferenceUtils.getInstance(context).putBoolean(AppConstants.IS_USER_LOGIN, true);

                                if (isCheckout) {
                                    finish();
                                } else {
                                    if (messageObj.getInt(USERTYPE) == 1) {
                                        startActivity(currentActivity, Dashboard.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                                    } else {
                                        startActivity(currentActivity, VendorDashBoardActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                                    }
                                    finish();
                                }
                            } else {
                                alert(currentActivity, getString(R.string.msg_reg_failed), getString(R.string.msg_reg_failed), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_NO_NETWORK);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getResources().getString(R.string.nwk_error_sign_up), true);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };
        AplusoneApplication.getInstance().addToRequestQueue(signUpRequest);
    }


    @Override
    protected void onDestroy() {
        if (smsReciever != null) {
            if (recieverRegistered == true) {
                unregisterReceiver(smsReciever);
                recieverRegistered = false;
            }
        }
        super.onDestroy();
    }

    /*class to Request the Gupshup service to send otp Start*/
    class RequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            URL url = null;
            try {
                url = new URL(uri[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod("GET");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.connect();
                BufferedReader rd = new BufferedReader(new
                        InputStreamReader(conn.getInputStream()));
                String line;
                StringBuffer buffer = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    buffer.append(line).append("\n");
                }
                System.out.println(buffer.toString());
                rd.close();
                conn.disconnect();
                return buffer.toString();

            } catch (IOException e) {
                e.printStackTrace();
            }

            String responseString = "";
            return responseString;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!TextUtils.isEmpty(result)) {

            }
        }

    }


}
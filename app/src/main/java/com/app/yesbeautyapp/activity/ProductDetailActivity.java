package com.app.yesbeautyapp.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.ProductAddedToCartListener;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;

public class ProductDetailActivity extends BaseActivity implements ProductAddedToCartListener {
    private ProductModel productModel;
    private TextView textProductName;
    private TextView textProductDescription;
    private TextView textServiceTiming;
    private TextView textProductRate;
    private TextView textServiceRate;
    private TextView textProductAmount;
    private ImageView productImage;
    private LinearLayout addToCartLL;
    private TextView addToCartTV;
    private ArrayList<ProductModel> productModelArrayList;

    @Override
    protected void initViews() {
        productModelArrayList = CartProductModel.getInstance().getProductModelArrayList();
        if (productModelArrayList == null) {
            productModelArrayList = new ArrayList<>();
        }
        if (getIntent().getExtras() != null) {
            productModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
            if (productModel != null) {
                if (productModel.getProductName() != null)
                    settingTitle(productModel.getProductName().trim());
            }
        }
        findViewById();
        setProductDetailBinding();

        if (isProductExist(productModel)) {
            addToCartTV.setText(AppConstants.ADDED);
        } else {
            addToCartTV.setText(AppConstants.ADD_TO_CART_BUTTON);
        }
    }

    @Override
    protected void initContext() {
        context = this;
        currentActivity = this;

    }

    @Override
    protected void initListners() {
        addToCartLL.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
    }

    private void setProductDetailBinding() {
        if (productModel == null) return;
        textProductName.setText(productModel.getProductName().trim());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textProductDescription.setText(Html.fromHtml(productModel.getDescription().trim(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            textProductDescription.setText(Html.fromHtml(productModel.getDescription().trim()));
        }
        float totalAmt = 0;
        String productCost = productModel.getProductCost();
        String serviceCost = productModel.getServiceCost();
        if (TextUtils.isEmpty(productCost)) productCost = "0";
        if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
        textServiceTiming.setText(productModel.getDuration() + " " + "Minutes");
        textProductRate.setText("Product Cost:" + " " + "Rs." + productCost);
        textServiceRate.setText("Service Cost: " + " " + "Rs." + serviceCost);
        totalAmt = Float.parseFloat(productCost) + Float.parseFloat(serviceCost);
        textProductAmount.setText("Total Amount:" + " " + "Rs. " + (int) totalAmt);
        String imageUrl = AppConstants.BASE_URL_IMAGES + productModel.getProductImage();
        Picasso.with(currentActivity).load(imageUrl).error(R.drawable.imghome5).placeholder(R.drawable.imghome5).into(productImage);

        FontUtils.changeFont(currentActivity, textProductName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(currentActivity, textProductRate, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(currentActivity, textServiceRate, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(currentActivity, textProductAmount, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(currentActivity, textProductDescription, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(currentActivity, textServiceTiming, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(currentActivity, addToCartTV, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_add_to_cart: {
                if (!isProductAlreadyList(productModel)) {
                    productModel.setProductQty(1);
                    productModel.setServiceQty(1);
                    productModelArrayList.add(0, productModel);
                    addToCartTV.setText(AppConstants.ADDED);

                } else {
                    addToCartTV.setText(AppConstants.ADD_TO_CART_BUTTON);
                }
                CartProductModel.getInstance().setProductModelArrayList(productModelArrayList);
                break;
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    private void findViewById() {
        productImage = findViewById(R.id.product_image);
        textProductName = findViewById(R.id.text_product_name);
        textProductRate = findViewById(R.id.text_product_rate);
        textServiceRate = findViewById(R.id.text_service_rate);
        textProductAmount = findViewById(R.id.text_product_amount);
        textServiceTiming = findViewById(R.id.text_service_timing);
        textProductDescription = findViewById(R.id.text_product_description);
        addToCartLL = findViewById(R.id.lin_add_to_cart);
        addToCartTV = findViewById(R.id.text_add_to_cart);
    }

    @Override
    public void productAddedToCart(ProductModel productModel) {

    }

    private boolean isProductAlreadyList(ProductModel productModel) {
        if (productModelArrayList == null) return false;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        int count = 0;
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                productModelArrayList.remove(count);
                return true;
            }
            count++;
        }
        return false;
    }

    public boolean isProductExist(ProductModel productModel) {
        if (productModelArrayList == null) return false;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                return true;
            }

        }
        return false;
    }

}

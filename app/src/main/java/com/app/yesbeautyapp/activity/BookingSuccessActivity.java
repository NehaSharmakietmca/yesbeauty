package com.app.yesbeautyapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.utils.FontUtils;


public class BookingSuccessActivity extends BaseActivity {

    private TextView lblSuccessTV;
    private TextView textBooking;
    private Button btnBookOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_success);
    }

    @Override
    protected void initViews() {
        lblSuccessTV = findViewById(R.id.lblSuccessTV);
        textBooking = findViewById(R.id.textBooking);
        btnBookOther = findViewById(R.id.btnBookOther);

        FontUtils.changeFont(context, lblSuccessTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, textBooking, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnBookOther, FONT_OPEN_SANS_SEMIBOLD_TTF);

        if (getIntent().getExtras() != null) {
            String bookingId = getIntent().getExtras().getString("bookingId");
            String msg = "Your Order Id #" + bookingId + " has been placed and is being processed";
            textBooking.setText(msg);
        }
    }

    @Override
    protected void initContext() {
        context = BookingSuccessActivity.this;
        currentActivity = BookingSuccessActivity.this;
    }

    @Override
    protected void initListners() {
        btnBookOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToHome();
            }
        });
    }

    @Override
    protected boolean isActionBar() {
        return false;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToHome();
    }

    private void goToHome() {
        Intent intent = new Intent(currentActivity, Dashboard.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_slide_up, R.anim.anim_stopping_exiting_activity);
        finish();
    }
}

package com.app.yesbeautyapp.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.AppointmentViewPagerAdapter;
import com.app.yesbeautyapp.adapter.ViewPagerAdapter;
import com.app.yesbeautyapp.fragment.AppointmentHistoryFragment;
import com.app.yesbeautyapp.fragment.AppointmentOngoingFragment;

public class MyAppointmentsActivity extends BaseActivity {
    private TabLayout appointmentTabLayout;
    private ViewPager appointmentViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_appointments);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_mybookings));
        appointmentTabLayout = findViewById(R.id.appointments_tabs);
        appointmentViewPager = findViewById(R.id.appointmentViewPager);
        setupViewPager(appointmentViewPager);
        appointmentTabLayout.setVisibility(View.VISIBLE);
        appointmentTabLayout.setupWithViewPager(appointmentViewPager);
    }

    @Override
    protected void initContext() {
        currentActivity = this;
        context = this;

    }

    @Override
    protected void initListners() {

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    private void setupViewPager(ViewPager viewPager) {
        AppointmentViewPagerAdapter adapter = new AppointmentViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AppointmentOngoingFragment(), "ONGOING");
        adapter.addFrag(new AppointmentHistoryFragment(), "HISTORY");
        viewPager.setAdapter(adapter);
    }
}

package com.app.yesbeautyapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.utils.FontUtils;

public class ContactUsActivity extends BaseActivity {

    private TextView lblMailTV;
    private TextView textMailTV;
    private TextView lblCallTV;
    private TextView textCallTV;

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_contact_us));
        lblMailTV = findViewById(R.id.lblMailTV);
        textMailTV = findViewById(R.id.textMailTV);
        lblCallTV = findViewById(R.id.lblCallTV);
        textCallTV = findViewById(R.id.textCallTV);

        FontUtils.changeFont(context, lblMailTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, lblCallTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, textMailTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, textCallTV, FONT_OPEN_SANS_REGULAR_TTF);
    }

    @Override
    protected void initContext() {
        context = ContactUsActivity.this;
        currentActivity = ContactUsActivity.this;
    }

    @Override
    protected void initListners() {

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onAlertClicked(int alertType) {

    }
}

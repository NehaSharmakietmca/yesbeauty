package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.DateAdapter;
import com.app.yesbeautyapp.adapter.ServiceAdapter;
import com.app.yesbeautyapp.adapter.TimeAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.DateModel;
import com.app.yesbeautyapp.model.OrderModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.model.ScheduleDateTimeModel;
import com.app.yesbeautyapp.model.ScheduleTimeModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TimeScheduleActivity extends BaseActivity {

    private ArrayList<ProductModel> productModelArrayList;
    private ArrayList<ScheduleDateTimeModel> scheduleDateTimeModels;
    private ArrayList<ScheduleTimeModel> scheduleTimeModelArrayList = new ArrayList<ScheduleTimeModel>();
    private ArrayList<DateModel> dayList;
    private DateAdapter dateAdapter;
    private RecyclerView recyclerView;
    private TextView addressTV;
    private TextView btnChangeAddress;
    private Button btnProceed;
    private FusedLocationProviderClient mFusedLocationClient;
    protected static long MIN_UPDATE_INTERVAL = 30 * 1000;
    LocationRequest locationRequest;
    Location currentLocation = null;
    private String currentAddress;
    private TimeAdapter timeAdapter;
    private RecyclerView timeRecyclerView;
    private String selectedTimeSlot;
    private TextView noSlotFoundTextView;
    private TextView normalSlotTv;
    private LinearLayout noTimeSlotLinearLayout;
    private LinearLayout callUSll;
    private DateModel selectedDateModel;
    private OrderModel orderModel;
    private boolean isFromHistory = false;
    private String dateStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_schedule);
    }

    @Override
    protected void initViews() {
        settingTitle(getIntent().getStringExtra("title"));
        if (getIntent().getExtras() != null) {
            orderModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
            isFromHistory = getIntent().getExtras().getBoolean(KEY_BOOLEAN);
        }
        scheduleDateTimeModels = new ArrayList<>();
        dayList = new ArrayList<>();
        productModelArrayList = CartProductModel.getInstance().getProductModelArrayList();
        if (productModelArrayList == null) {
            productModelArrayList = new ArrayList<>();
        }
        recyclerView = findViewById(R.id.recycler_date);
        addressTV = findViewById(R.id.addressTV);
        btnChangeAddress = findViewById(R.id.btnChangeAddress);
        btnProceed = findViewById(R.id.btnProceed);
        timeRecyclerView = findViewById(R.id.recycler_time_slot);
        noSlotFoundTextView = findViewById(R.id.text_no_slots_found);
        normalSlotTv = findViewById(R.id.tv_normal_slot);
        callUSll = findViewById(R.id.lin_call_us);
        noTimeSlotLinearLayout = findViewById(R.id.lin_no_time_slot);

        TextView schDT = findViewById(R.id.tv_schedule_date_and_time);
        TextView lcnTV = findViewById(R.id.tv_location);
        TextView tTV = findViewById(R.id.tv_time_arrival);
        FontUtils.changeFont(context, schDT, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, lcnTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, tTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, addressTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, btnProceed, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, normalSlotTv, FONT_OPEN_SANS_REGULAR_TTF);

        getWeekDays();
        dateAdapter = new DateAdapter(currentActivity, dayList);
        recyclerView.setAdapter(dateAdapter);

        toSetAddress();
        getUserDetails();
        getScheduleList();
        checkForLocationRequest();
        checkForLocationSettings();

        setTimeScheduleRecyclerAdapter();

        setDateClickListener();
        setTimeClickListener();
        if (isFromHistory) {
            btnProceed.setText("Reschedule");
        } else {
            btnProceed.setText("Submit");
        }
    }

    @Override
    protected void initContext() {
        context = TimeScheduleActivity.this;
        currentActivity = TimeScheduleActivity.this;
    }

    @Override
    protected void initListners() {
        btnChangeAddress.setOnClickListener(this);
        btnProceed.setOnClickListener(this);
        callUSll.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnChangeAddress: {
                startActivity(currentActivity, AddressActivity.class, bundle, true, REQUEST_UPDATEADDRESS, true, ANIMATION_SLIDE_UP);
                break;
            }
            case R.id.btnProceed: {
                if (isFromHistory) {
                    if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                        if(TextUtils.isEmpty(dateStr)|| TextUtils.isEmpty(selectedTimeSlot)){
                            Toast.makeText(TimeScheduleActivity.this,"Please select Date And Time",Toast.LENGTH_LONG).show();
                            return;
                        }
                        rescheduleOrder();
                    } else {
                        alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                    }
                } else {
                    if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                        if(TextUtils.isEmpty(dateStr)|| TextUtils.isEmpty(selectedTimeSlot)){
                            Toast.makeText(TimeScheduleActivity.this,"Please select Date And Time",Toast.LENGTH_LONG).show();
                            return;
                        }
                        saveUserDetails();
                    } else {
                        alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, false, ALERT_TYPE_NO_NETWORK);
                    }
                }
                break;
            }
            case R.id.lin_call_us: {
                makeCall();
                break;
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    private void toOpenPaymentActivity() {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (selectedDateModel != null) {
             dateStr = selectedDateModel.getYear() + "-" + selectedDateModel.getIntMonth() + "-" + selectedDateModel.getDate();
            bundle.putString("scheduleDate", dateStr);
            bundle.putString("scheduleTime", selectedTimeSlot);
        }
        startActivity(currentActivity, PaymentMethodActivity.class, bundle, true, REQUEST_UPDATEADDRESS, true, ANIMATION_SLIDE_UP);
    }

    private void getWeekDays() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE-d-MMMM-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        String[] days = new String[7];
        days[0] = sdf.format(date);
        String dateArr[] = days[0].split("-");
        DateModel dateModel = new DateModel();
        dateModel.setDay(dateArr[0]);
        dateModel.setDate(dateArr[1]);
        dateModel.setMonth(dateArr[2]);
        dateModel.setIntMonth(dateArr[3]);
        dateModel.setYear(dateArr[4]);
        dayList.add(dateModel);
        for (int i = 1; i < 7; i++) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            date = cal.getTime();
            days[i] = sdf.format(date);
            if (TextUtils.isEmpty(days[i])) continue;
            dateArr = days[i].split("-");
            dateModel = new DateModel();
            dateModel.setDay(dateArr[0]);
            dateModel.setDate(dateArr[1]);
            dateModel.setMonth(dateArr[2]);
            dateModel.setIntMonth(dateArr[3]);
            dateModel.setYear(dateArr[4]);
            dayList.add(dateModel);
        }
    }

    public void checkForLocationRequest() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(currentActivity);
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    //Check for location settings.
    public void checkForLocationSettings() {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(currentActivity);

            settingsClient.checkLocationSettings(builder.build())
                    .addOnSuccessListener(currentActivity, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            callCurrentLocation();
                        }
                    })
                    .addOnFailureListener(currentActivity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(currentActivity, 99);
                                    } catch (IntentSender.SendIntentException sie) {
                                        sie.printStackTrace();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    Toast.makeText(currentActivity, "Setting change is not available.Try in another device.", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callCurrentLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(RECEIVE_LOCATION_PERMISSION_REQUEST);
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    currentLocation = locationResult.getLastLocation();
                    //toast(context, "Your Location : " + getCompleteAddressString());
                    currentAddress = getCompleteAddressString();
                    if (currentAddress == null) return;
                    if (TextUtils.isEmpty(addressTV.getText().toString())) {
                        addressTV.setText(currentAddress.trim());
                    }
                }
            }, Looper.myLooper());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getCompleteAddressString() {
        String strAdd = "";
        if (currentLocation == null) return strAdd;
        Geocoder geocoder = new Geocoder(currentActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress == null) return strAdd;
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    if (!TextUtils.isEmpty(returnedAddress.getAddressLine(i)))
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void startLocationPermissionRequest(int requestCode) {
        ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    private void requestPermissions(final int requestCode) {
        startLocationPermissionRequest(requestCode);
    }

    private void startPhoneCallPermissionRequest(int requestCode) {
        ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.CALL_PHONE}, requestCode);
    }

    private void requestPhoneCallPermissions(final int requestCode) {
        startPhoneCallPermissionRequest(requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == currentActivity.RESULT_OK) {
            callCurrentLocation();
        } else if (requestCode == REQUEST_UPDATEADDRESS && resultCode == currentActivity.RESULT_OK) {
            toSetAddress();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RECEIVE_LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callCurrentLocation();
                }
                break;
            case RECEIVE_PHONE_CALL_PERMISSION:
                makeCall();
                break;
        }

    }

    public void getScheduleList() {
        JSONObject jsons = null;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL_GET_SCHEDULE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    Gson gson = new Gson();
                    List<ScheduleDateTimeModel> scheduleModels = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), ScheduleDateTimeModel[].class));
                    scheduleDateTimeModels.clear();
                    scheduleDateTimeModels.addAll(scheduleModels);
                    if (dayList != null) {
                        selectedDateModel = dayList.get(0);
                        updateTimeSlot();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    private void getUserDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            Log.e("GetUserDetails", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_PROFILE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_EMAIL, messageObj.getString(USER_EMAIL));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, messageObj.getString(USER_NAME));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_ADDRESS, messageObj.getString(USER_ADDRESS));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_LAST_NAME, messageObj.getString(USER_LAST_NAME));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_BUILDING, messageObj.getString(USER_BUILDING));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_STREET, messageObj.getString(USER_STREET));
                                toSetAddress();
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    private void toSetAddress() {
        String building = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_BUILDING);
        String street = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_STREET);
        String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
        StringBuilder completeAddress = new StringBuilder();
        if (!TextUtils.isEmpty(building)) {
            completeAddress.append(building);
            completeAddress.append(", ");
        }
        if (!TextUtils.isEmpty(street)) {
            completeAddress.append(street);
            completeAddress.append(", ");
        }
        if (!TextUtils.isEmpty(address)) {
            completeAddress.append(address);
        }
        if (TextUtils.isEmpty(completeAddress.toString()) && currentAddress != null) {
            completeAddress.append(currentAddress);
        }
        addressTV.setText(completeAddress.toString().trim());
    }

    private void setDateClickListener() {

        dateAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                dateAdapter.selectedItem();
                selectedDateModel = dayList.get(position);
                scheduleTimeModelArrayList.clear();
                updateTimeSlot();
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

    private void setTimeScheduleRecyclerAdapter() {
        timeAdapter = new TimeAdapter(currentActivity, scheduleTimeModelArrayList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(currentActivity, 2);
        timeRecyclerView.setLayoutManager(gridLayoutManager);
        timeRecyclerView.setAdapter(timeAdapter);
    }

    private void setTimeClickListener() {
        timeAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                timeAdapter.selectedItem();
                ScheduleTimeModel scheduleTimeModel = scheduleTimeModelArrayList.get(position);
                selectedTimeSlot = scheduleTimeModel.getTimeSlot();
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

    private void updateTimeSlot() {
        if (scheduleDateTimeModels == null || selectedDateModel == null) return;
        for (int i = 0; i < scheduleDateTimeModels.size(); i++) {
            ScheduleDateTimeModel scheduleDateTimeModel = scheduleDateTimeModels.get(i);
            if (scheduleDateTimeModel == null) return;
            String scheduleDate = scheduleDateTimeModel.getScheduleDate();
            if (!TextUtils.isEmpty(scheduleDate)) {
                if (selectedDateModel == null) return;
                 dateStr = selectedDateModel.getYear() + "-" + selectedDateModel.getIntMonth() + "-" + selectedDateModel.getDate();
                if (dateStr.equalsIgnoreCase(scheduleDate)) {
                    scheduleTimeModelArrayList.addAll(scheduleDateTimeModel.getScheduleTimeModelList());
                    break;
                } else {
                    scheduleTimeModelArrayList.clear();
                }
            }
        }
        if (!scheduleTimeModelArrayList.isEmpty()) {
            timeRecyclerView.setVisibility(View.VISIBLE);
            noSlotFoundTextView.setVisibility(View.GONE);
            noTimeSlotLinearLayout.setVisibility(View.GONE);
            callUSll.setVisibility(View.GONE);
            normalSlotTv.setVisibility(View.VISIBLE);
            ScheduleTimeModel scheduleTimeModel = scheduleTimeModelArrayList.get(0);
            selectedTimeSlot = scheduleTimeModel.getTimeSlot();
        } else {
            timeRecyclerView.setVisibility(View.GONE);
            noSlotFoundTextView.setVisibility(View.VISIBLE);
            noTimeSlotLinearLayout.setVisibility(View.VISIBLE);
            callUSll.setVisibility(View.VISIBLE);
            normalSlotTv.setVisibility(View.GONE);
        }
        timeAdapter.updateAdapter(scheduleTimeModelArrayList);
    }

    private void makeCall() {
        String number = "8888888888";
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        if (Build.VERSION.SDK_INT >= 23) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPhoneCallPermissions(AppConstants.RECEIVE_PHONE_CALL_PERMISSION);
            } else {
                startActivity(callIntent);
            }
        } else {
            startActivity(callIntent);

        }
    }


    public void saveUserDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        final String building = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_BUILDING);
        final String street = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_STREET);
        String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
        final String addressStr;
        if (TextUtils.isEmpty(address)) {
            address = currentAddress;
        }
        addressStr = address;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            jsons.put(AppConstants.USER_BUILDING, building);
            jsons.put(AppConstants.USER_STREET, street);
            jsons.put(AppConstants.USER_ADDRESS, addressStr);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_ADDRESS, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_ADDRESS, addressStr);
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_BUILDING, building);
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_STREET, street);
                                toOpenPaymentActivity();
                            } else {
                                toast("Failed to change update info.", false);
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    public void rescheduleOrder() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;

        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            jsons.put(AppConstants.ORDER_ID, orderModel.getOrderId());
            jsons.put(AppConstants.SCHEDULE_DATE, dateStr);
            jsons.put(AppConstants.SCHEDULE_TIME, selectedTimeSlot);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_RESCHEDULE_BOOKING, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                              finish();
                            } else {
                                toast("Failed to change update info.", false);
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

}

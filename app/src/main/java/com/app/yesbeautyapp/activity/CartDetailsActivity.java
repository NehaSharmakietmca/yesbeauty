package com.app.yesbeautyapp.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.CartAdapter;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.dao.CartDataSource;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.CartModel;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.LoginUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;

public class CartDetailsActivity extends BaseActivity {


    private LinearLayout infoLL;
    private RecyclerView recyclerCart;
    private CartAdapter cartAdapter;
    private ArrayList<ProductModel> productModelArrayList;
    private TextView totalAmtTV;
    private TextView checkoutTV;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_details);
    }

    @Override
    protected void initViews() {
        title = getIntent().getStringExtra("title");
        settingTitle(title);
        settingHomeButton();
        productModelArrayList = CartProductModel.getInstance().getProductModelArrayList();
        if (productModelArrayList == null) {
            productModelArrayList = new ArrayList<>();
        }
        recyclerCart = findViewById(R.id.recyclerCart);
        totalAmtTV = findViewById(R.id.totalAmtTV);
        checkoutTV = findViewById(R.id.checkoutTV);
        recyclerCart = findViewById(R.id.recyclerCart);
        infoLL = findViewById(R.id.infoLL);
        FontUtils.changeFont(context, totalAmtTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, checkoutTV, FONT_OPEN_SANS_SEMIBOLD_TTF);

        LinearLayoutManager layoutManager = new LinearLayoutManager(currentActivity);
        cartAdapter = new CartAdapter(currentActivity, productModelArrayList);
        recyclerCart.setLayoutManager(layoutManager);
        recyclerCart.setAdapter(cartAdapter);

        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null) {
                productModel.setProductQty(1);
                productModel.setServiceQty(1);
            }
        }
        setTotalAmt();
    }

    @Override
    protected void initContext() {
        context = CartDetailsActivity.this;
        currentActivity = CartDetailsActivity.this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void initListners() {
        infoLL.setOnClickListener(this);
        checkoutTV.setOnClickListener(this);
        cartAdapter.setOnItemClickListner(new RecyclerClickListner() {
            @Override
            public void onItemClick(int position, View v) {
                if (productModelArrayList.size() < position) return;
                ProductModel productModel = productModelArrayList.get(position);
                if (productModel == null) return;
                switch (v.getId()) {
                    case R.id.addServiceIV: {
                        if (productModel.getServiceQty() < 3) {
                            productModel.setServiceQty(productModel.getServiceQty() + 1);
                        } else {
                            toast("Maximum limit reached for this service.", false);
                        }
                        break;
                    }
                    case R.id.minusServiceIV: {
                        if (productModel.getServiceQty() > 1) {
                            productModel.setServiceQty(productModel.getServiceQty() - 1);
                            if (productModel.getProductQty() > productModel.getServiceQty()) {
                                productModel.setProductQty(productModel.getProductQty() - 1);
                            }
                        }
                        break;
                    }
                    case R.id.addProductIV: {
                        if (productModel.getProductQty() < productModel.getServiceQty()) {
                            productModel.setProductQty(productModel.getProductQty() + 1);
                        } else {
                            toast("Limit can't be greater than service limit.", false);
                        }
                        break;
                    }
                    case R.id.minusProductIV: {
                        if (productModel.getProductQty() > 0) {
                            productModel.setProductQty(productModel.getProductQty() - 1);
                            if (productModel.getProductQty() == 0) {
                                toast("Now you can use your own product for this service.", true);
                            }
                        }
                        break;
                    }
                }
                setTotalAmt();
                cartAdapter.notifyDataSetChanged();
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.checkoutTV: {
                if (productModelArrayList.size() == 0) {
                    return;
                }
                if (bundle == null) {
                    bundle = new Bundle();
                }
                if (LoginUtils.isLogin(context)) {
                    bundle.putString("title", title);
                    CartProductModel.getInstance().setProductModelArrayList(productModelArrayList);
                    startActivity(currentActivity, TimeScheduleActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                } else {
                    bundle.putBoolean("isCheckout", true);
                    startActivity(currentActivity, LoginActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                }
                break;
            }
            case R.id.infoLL:{
                showOfferAndDiscountDialog();
                break;
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    private void setTotalAmt() {

        totalAmtTV.setText("Total: Rs. " + ((int) totalProductAmount()) + "/-");
    }

    private double totalProductAmount() {
        double totalAmount = 0.0;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String productCost = productModel.getProductCost();
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(productCost)) productCost = "0";
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = (productModel.getProductQty() * Double.parseDouble(productCost))
                        + (productModel.getServiceQty() * Double.parseDouble(serviceCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    public boolean removeItem(ProductModel productModel) {
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        int count = 0;
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                productModelArrayList.remove(count);
                return true;
            }
            count++;
        }
        return false;
    }

    private void showOfferAndDiscountDialog() {
        final Dialog offersAndDiscountDialog = new Dialog(context);
        offersAndDiscountDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater li = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.dialog_ratecard, null);
        Button submitButton = v.findViewById(R.id.btn_submit);

        FontUtils.changeFont(context, submitButton, FONT_OPEN_SANS_SEMIBOLD_TTF);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offersAndDiscountDialog.dismiss();
            }
        });
        offersAndDiscountDialog.setCancelable(false);
        offersAndDiscountDialog.setCanceledOnTouchOutside(false);
        offersAndDiscountDialog.setContentView(v);
        WindowManager m = currentActivity.getWindowManager();
        Display d = m.getDefaultDisplay();
        Window dialogWindow = offersAndDiscountDialog.getWindow();
        WindowManager.LayoutParams p = dialogWindow.getAttributes();
        //p.height = (int) (d.getHeight() * 0.75);
        p.width = (int) (d.getWidth() * 0.95);
        dialogWindow.setAttributes(p);
        offersAndDiscountDialog.show();
    }

}

package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.network.BaseVolleyRequest;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.Helper;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;
import com.app.yesbeautyapp.widgets.CircularImageView;

public class ProfileActivity extends BaseActivity {

    private View view;
    private EditText nameEditText;
    private EditText surnameEditText;
    private EditText emailEditText;
    private EditText mobileNumberEditText;
    private EditText addressEditText;
    private Button saveButton;
    private ImageView gpsImageView;
    private int bytesAvailable;
    private int bytesRead;
    private String mimeType;
    private boolean isCurrentLocation = false;

    private FusedLocationProviderClient mFusedLocationClient;
    protected static long MIN_UPDATE_INTERVAL = 30 * 1000;
    LocationRequest locationRequest;
    private CircularImageView profileImageView;
    Location currentLocation = null;
    String userProfileUrl;
    Bitmap bitmapProfileImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_profile));
        //settingHomeButton();
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        surnameEditText = (EditText) findViewById(R.id.surnameEditText);
        gpsImageView = (ImageView) findViewById(R.id.image_gps);
        emailEditText = (EditText) findViewById(R.id.emailIdEditText);
        mobileNumberEditText = (EditText) findViewById(R.id.edtMobileNumber);
        addressEditText = (EditText) findViewById(R.id.editTextAddress);
        profileImageView = findViewById(R.id.profileImageview);
        saveButton = (Button) findViewById(R.id.saveButton);
        FontUtils.changeFont(context, nameEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, surnameEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, emailEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, mobileNumberEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, addressEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, saveButton, FONT_OPEN_SANS_SEMIBOLD_TTF);


        mobileNumberEditText.setText(SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_MOBILE_NO));
        nameEditText.setText(SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_NAME));
        emailEditText.setText(SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_EMAIL));
        surnameEditText.setText(SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_LAST_NAME));
        String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
        if (!TextUtils.isEmpty(address)) {
            addressEditText.setText(address.trim());
        }
        checkForLocationRequest();
        checkForLocationSettings();
        userProfileUrl = SharedPreferenceUtils.getInstance(currentActivity).getString(AppConstants.USER_PROFILE_IMAGE);

        if (userProfileUrl != null && !userProfileUrl.equals("")) {
            //Picasso.with(currentActivity).load(BASE_URL_IMAGES + "/" + userProfileUrl).placeholder(R.drawable.profile_icon).error(R.drawable.profile_icon).into(profileImageView);
        }
        getUserDetails();
        setTermAndConditionsCheckBox();
    }

    @Override
    protected void initContext() {
        context = ProfileActivity.this;
        currentActivity = ProfileActivity.this;
    }

    @Override
    protected void initListners() {
        gpsImageView.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        profileImageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_gps:
                alert(currentActivity, getResources().getString(R.string.alert_title_location), getResources().getString(R.string.alert_message_location), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_LOCATION_UPDATED);

                break;
            case R.id.saveButton:
                toHideKeyboard();
                if (isMandatoryFields()) {
                    saveUserDetails();
                }
                break;
            case R.id.profileImageview:
                toHideKeyboard();
                selectImage();
                break;
        }

    }

    private void selectImage() {
        try {
            if (checkPermission()) {
                openChooseImageDialog();
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openChooseImageDialog() {
        final CharSequence[] options = {getString(R.string.option_take_photo), getString(R.string.option_gallery), getString(R.string.option_cancel)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(currentActivity);
        builder.setTitle(getString(R.string.select_option));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getString(R.string.option_take_photo))) {
                    dialog.dismiss();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);
                } else if (options[item].equals(getString(R.string.option_gallery))) {
                    dialog.dismiss();
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                } else if (options[item].equals(getString(R.string.option_cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onAlertClicked(int alertType) {
        switch (alertType) {
            case ALERT_TYPE_IMAGE_UPLOAD: {
                uploadImageByVolley();
                break;
            }
            case ALERT_TYPE_LOCATION_UPDATED: {
                isCurrentLocation = true;
                addressEditText.setText("");
                checkForLocationSettings();
                checkForLocationRequest();
                break;

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (resultCode == currentActivity.RESULT_OK) {
                callCurrentLocation();
            }
        }
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (resultCode == RESULT_OK) {
                    Bitmap imageBitmap = (Bitmap) data.getExtras().get(KEY_DATA);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                    profileImageView.setImageBitmap(imageBitmap);
                    alert(currentActivity, getResources().getString(R.string.alert_message_upload_image), getResources().getString(R.string.alert_message_upload_image), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, false, ALERT_TYPE_IMAGE_UPLOAD);
                    // uploadImageByVolley();
                }
                break;
            }
            case PICK_IMAGE_GALLERY: {
                if (resultCode == RESULT_OK) {
                    Uri filePath = data.getData();
                    try {
                        //Getting the Bitmap from Gallery
                        Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                        profileImageView.setImageBitmap(imageBitmap);
                        alert(currentActivity, getResources().getString(R.string.alert_message_upload_image), getResources().getString(R.string.alert_message_upload_image), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, false, ALERT_TYPE_IMAGE_UPLOAD);
                        // uploadImageByVolley();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }

        }


    }

    public void saveUserDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(ProfileActivity.this).getString(USER_ID));
            jsons.put(AppConstants.USER_EMAIL, emailEditText.getText().toString().trim());
            jsons.put(AppConstants.USER_NAME, nameEditText.getText().toString().trim());
            jsons.put(AppConstants.USER_LAST_NAME, surnameEditText.getText().toString().trim());
            jsons.put(AppConstants.USER_ADDRESS, addressEditText.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_PROFILE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                toast("Profile Updated Sucessfully.", false);
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }


    @Override
    protected boolean isHomeButton() {
        return true;
    }

    private void getUserDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(ProfileActivity.this).getString(USER_ID));
            Log.e("GetUserDetails", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_GET_PROFILE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_EMAIL, messageObj.getString(USER_EMAIL));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, messageObj.getString(USER_NAME));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_ADDRESS, messageObj.getString(USER_ADDRESS));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_LAST_NAME, messageObj.getString(USER_LAST_NAME));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_BUILDING, messageObj.getString(USER_BUILDING));
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_STREET, messageObj.getString(USER_STREET));
                                nameEditText.setText(messageObj.getString(USER_NAME));
                                emailEditText.setText(messageObj.getString(USER_EMAIL));
                                surnameEditText.setText(messageObj.getString(USER_LAST_NAME));

                                String address = messageObj.getString(USER_ADDRESS);
                                if (!TextUtils.isEmpty(address)) {
                                    addressEditText.setText(address.trim());
                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");

                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }


    private boolean isMandatoryFields() {
        nameEditText.setError(null);
        emailEditText.setError(null);
        mobileNumberEditText.setError(null);
        addressEditText.setError(null);
        if (nameEditText.getText().toString().isEmpty()) {
            nameEditText.setError(getResources().getString(R.string.error_name_empty));
            nameEditText.requestFocus();
            return false;
        } else if (!Validator.getInstance().validatePhoneNumber(context, mobileNumberEditText.getText().toString()).equals("")) {
            String numberError = Validator.getInstance().validatePhoneNumber(context, mobileNumberEditText.getText().toString());
            mobileNumberEditText.setError(numberError);
            mobileNumberEditText.requestFocus();
            return false;

        } else if (!Validator.getInstance().isValidEmail(context, emailEditText.getText().toString()).equals("")) {
            String emailError = Validator.getInstance().isValidEmail(context, emailEditText.getText().toString());
            emailEditText.setError(emailError);
            emailEditText.requestFocus();
            return false;
        } else if (addressEditText.getText().toString().isEmpty()) {
            addressEditText.setError(getResources().getString(R.string.edit_text_address));
            addressEditText.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(currentActivity, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openChooseImageDialog();
                }
            }
            break;
            case RECEIVE_LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callCurrentLocation();
                }

        }

    }


    private void startLocationPermissionRequest(int requestCode) {
        ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    private void requestPermissions(final int requestCode) {
        startLocationPermissionRequest(requestCode);
    }

    public void checkForLocationRequest() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(currentActivity);
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    //Check for location settings.
    public void checkForLocationSettings() {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(currentActivity);

            settingsClient.checkLocationSettings(builder.build())
                    .addOnSuccessListener(currentActivity, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            callCurrentLocation();
                        }
                    })
                    .addOnFailureListener(currentActivity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {


                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(currentActivity, 99);
                                    } catch (IntentSender.SendIntentException sie) {
                                        sie.printStackTrace();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    Toast.makeText(currentActivity, "Setting change is not available.Try in another device.", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Callback received when a permissions request has been completed.
     */


    public void callCurrentLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(RECEIVE_LOCATION_PERMISSION_REQUEST);
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    currentLocation = locationResult.getLastLocation();
                    //toast(context, "Your Location : " + getCompleteAddressString());
                    String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
                    if (!isCurrentLocation) {
                        if (!TextUtils.isEmpty(address)) {
                            addressEditText.setText(address.trim());
                        } else {
                            addressEditText.setText(getCompleteAddressString().trim());
                        }
                    } else {
                        addressEditText.setText(getCompleteAddressString().trim());
                    }
                }
            }, Looper.myLooper());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getCompleteAddressString() {
        String strAdd = "";
        if (currentLocation == null) return strAdd;
        Geocoder geocoder = new Geocoder(currentActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress == null) return strAdd;
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    if (!TextUtils.isEmpty(returnedAddress.getAddressLine(i)))
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    public void uploadImageByVolley() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);


        final String lineEnd = "\r\n";
        final String twoHyphens = "--";
        final String boundary = "*****";


        final int maxBufferSize = 1 * 1024 * 1024;
        final byte[] meetupImageBytesArray = Helper.getImageBytes(((BitmapDrawable) profileImageView.getDrawable()).getBitmap());
        Long timeMillis = System.currentTimeMillis();
        final String fileName = "profileImage_" + timeMillis.toString() + ".jpg";


        String imageUploadUrl = UPLOAD_PROFILE_IMAGE;
        BaseVolleyRequest uploadProfileImageRequest = new BaseVolleyRequest(1, imageUploadUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    Log.e("image response  is", response.toString() + jsonString);
                    if (bundle == null) {
                        bundle = new Bundle();

                    }
//                    cancelProgressDialog();
                    //updateProfile(fileName);
                    //addNews();

                } catch (UnsupportedEncodingException e) {
                    cancelProgressDialog();
                    e.printStackTrace();
                }
                Log.e("image response  is", response.toString());
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast("error uploading image", true);
                Log.e("image upload error is", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                mimeType = "multipart/form-data;boundary=" + boundary;
                return mimeType;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);


                try {
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);

                    ByteArrayInputStream fileInputStream = new ByteArrayInputStream(meetupImageBytesArray);
                    bytesAvailable = fileInputStream.available();

                    int bufferSize = Math.min(meetupImageBytesArray.length, maxBufferSize);
                    byte[] buffer = new byte[bufferSize];

                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }


                    //    dos.write(meetupImageBytesArray, 0, bufferSize);
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    return bos.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return meetupImageBytesArray;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Long timeMillis = System.currentTimeMillis();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Connection", "Keep-Alive");
                params.put("ENCTYPE", "multipart/form-data");
                params.put("accept", "application/json");
                params.put("uploaded_file", fileName);
                params.put("Content-Type", "multipart/form-data;boundary=" + boundary);
                return params;

            }

        };

        AplusoneApplication.getInstance().addToRequestQueue(uploadProfileImageRequest);


    }

    public void updateProfile(final String profileName) {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.KEY_ID, Integer.parseInt(SharedPreferenceUtils.getInstance(ProfileActivity.this).getString(USER_ID)));

            jsons.put(AppConstants.KEY_EMAIL, emailEditText.getText().toString().trim());
            jsons.put(AppConstants.keyName, nameEditText.getText().toString().trim());
            jsons.put(AppConstants.keyUserNumber, mobileNumberEditText.getText().toString().trim());
            jsons.put(AppConstants.USER_PROFILE_IMAGE, profileName);
            jsons.put(AppConstants.USER_ADDRESS, addressEditText.getText().toString().trim());

            Log.e("jsonGetAllService", jsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_PROFILE, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    ((BaseActivity) currentActivity).logTesting("is successful fetch Service", "hi" + response.getBoolean(AppConstants.KEY_ERROR), Log.ERROR);
                    if (!response.getBoolean(AppConstants.KEY_ERROR)) {
                        Gson gson = new Gson();
                        SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_EMAIL, emailEditText.getText().toString().trim());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(USER_NAME, nameEditText.getText().toString());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_ADDRESS, addressEditText.getText().toString());
                        SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_PROFILE_IMAGE, profileName);
                        // finish();
                        //SharedPreferenceUtils.getInstance(ProfileActivity.this).putString(AppConstants.USER_PROFILE_IMAGE,profileName);


                        //  setSliderImageAdapter();

                    } else {
                        cancelProgressDialog();
                        ((BaseActivity) currentActivity).logTesting("fetch Service error", "true", Log.ERROR);
                    }


                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch products json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }
    private void setTermAndConditionsCheckBox() {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I have read and agree to all ");
        spanTxt.append("Terms and Conditions");
        final ForegroundColorSpan fcs = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent));
        spanTxt.setSpan(fcs, spanTxt.length() - "Terms and Conditions".length(), spanTxt.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
              /*  Toast.makeText(getApplicationContext(), "Terms of services Clicked",
                        Toast.LENGTH_SHORT).show();
                Uri uri = Uri.parse("https://s3.amazonaws.com/konnect-toc/G4S_Konnect_TOC.html"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/
            }
        }, spanTxt.length() - "Terms and Conditions".length(), spanTxt.length(), 0);

    }
}

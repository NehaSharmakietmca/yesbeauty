package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AddressActivity extends BaseActivity {

    private EditText buildingEditText;
    private EditText streetEditText;
    private EditText addressEditText;
    private Button saveButton;
    private ImageView gpsImageView;

    private FusedLocationProviderClient mFusedLocationClient;
    protected static long MIN_UPDATE_INTERVAL = 30 * 1000;
    LocationRequest locationRequest;
    Location currentLocation = null;
    private String currentAddress;
    private boolean isCurrentLocation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
    }

    @Override
    protected void initViews() {
        settingTitle("Change Address");
        buildingEditText = findViewById(R.id.buildingEditText);
        streetEditText = findViewById(R.id.streetEditText);
        addressEditText = findViewById(R.id.editTextAddress);
        saveButton = findViewById(R.id.saveButton);
        gpsImageView = findViewById(R.id.image_gps);

        FontUtils.changeFont(context, buildingEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, streetEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, addressEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, saveButton, FONT_OPEN_SANS_SEMIBOLD_TTF);
        toSetAddress();
        checkForLocationRequest();
        checkForLocationSettings();
    }

    @Override
    protected void initContext() {
        context = AddressActivity.this;
        currentActivity = AddressActivity.this;
    }

    @Override
    protected void initListners() {
        gpsImageView.setOnClickListener(this);
        saveButton.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_gps:
                alert(currentActivity, getResources().getString(R.string.alert_title_location), getResources().getString(R.string.alert_message_location), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_LOCATION_UPDATED);
                break;
            case R.id.saveButton:
                toHideKeyboard();
                if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                    if (isMandatoryFields()) {
                        saveUserDetails();
                    }
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, false, ALERT_TYPE_NO_NETWORK);
                }
                break;
        }
    }

    @Override
    public void onAlertClicked(int alertType) {
        switch (alertType) {
            case ALERT_TYPE_LOCATION_UPDATED: {
                isCurrentLocation = true;
                checkForLocationSettings();
                checkForLocationRequest();
                break;

            }
        }
    }

    public void checkForLocationRequest() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(currentActivity);
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    //Check for location settings.
    public void checkForLocationSettings() {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(currentActivity);

            settingsClient.checkLocationSettings(builder.build())
                    .addOnSuccessListener(currentActivity, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            callCurrentLocation();
                        }
                    })
                    .addOnFailureListener(currentActivity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(currentActivity, 99);
                                    } catch (IntentSender.SendIntentException sie) {
                                        sie.printStackTrace();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    Toast.makeText(currentActivity, "Setting change is not available.Try in another device.", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void callCurrentLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(RECEIVE_LOCATION_PERMISSION_REQUEST);
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    currentLocation = locationResult.getLastLocation();
                    //toast(context, "Your Location : " + getCompleteAddressString());
                    currentAddress = getCompleteAddressString();
                    if (currentAddress == null) return;
                    if (isCurrentLocation) {
                        addressEditText.setText(currentAddress.trim());
                    } else {
                        if (TextUtils.isEmpty(addressEditText.getText().toString())) {
                            addressEditText.setText(currentAddress.trim());
                        }
                    }
                }
            }, Looper.myLooper());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getCompleteAddressString() {
        String strAdd = "";
        if (currentLocation == null) return strAdd;
        Geocoder geocoder = new Geocoder(currentActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress == null) return strAdd;
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    if (!TextUtils.isEmpty(returnedAddress.getAddressLine(i)))
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void startLocationPermissionRequest(int requestCode) {
        ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    private void requestPermissions(final int requestCode) {
        startLocationPermissionRequest(requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (resultCode == currentActivity.RESULT_OK) {
                callCurrentLocation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RECEIVE_LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callCurrentLocation();
                }
        }

    }


    private void toSetAddress() {
        String building = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_BUILDING);
        String street = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_STREET);
        String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);

        if (building != null) {
            buildingEditText.setText(building.trim());
        }
        if (street != null) {
            streetEditText.setText(street.trim());
        }
        if (address != null) {
            addressEditText.setText(address.trim());
        }
    }

    private boolean isMandatoryFields() {
        buildingEditText.setError(null);
        streetEditText.setError(null);
        addressEditText.setError(null);
        if (buildingEditText.getText().toString().isEmpty()) {
            buildingEditText.setError("Please enter building name");
            buildingEditText.requestFocus();
            return false;
        } else if (streetEditText.getText().toString().isEmpty()) {
            streetEditText.setError("Please enter street and locality");
            streetEditText.requestFocus();
            return false;
        } else if (addressEditText.getText().toString().isEmpty()) {
            addressEditText.setError(getResources().getString(R.string.edit_text_address));
            addressEditText.requestFocus();
            return false;
        }
        return true;
    }

    public void saveUserDetails() {
        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);
        JSONObject jsons = null;
        try {
            jsons = new JSONObject();
            jsons.put(AppConstants.USER_ID, SharedPreferenceUtils.getInstance(context).getString(USER_ID));
            jsons.put(AppConstants.USER_BUILDING, buildingEditText.getText().toString().trim());
            jsons.put(AppConstants.USER_STREET, streetEditText.getText().toString().trim());
            jsons.put(AppConstants.USER_ADDRESS, addressEditText.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL_UPDATE_ADDRESS, jsons, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                toast("Address Changed Sucessfully.", false);
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_ADDRESS, addressEditText.getText().toString().trim());
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_BUILDING, buildingEditText.getText().toString().trim());
                                SharedPreferenceUtils.getInstance(currentActivity).putString(AppConstants.USER_STREET, streetEditText.getText().toString().trim());
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                toast("Failed to change your Address.", false);
                            }
                        }
                    }

                } catch (JSONException e) {
                    ((BaseActivity) currentActivity).logTesting("fetch User json exception is", e.toString(), Log.ERROR);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                ((BaseActivity) currentActivity).logTesting("error is", error.toString(), Log.ERROR);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");


                return params;
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(request);
    }

}

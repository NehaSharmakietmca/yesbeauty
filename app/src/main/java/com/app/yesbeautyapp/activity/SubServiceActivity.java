package com.app.yesbeautyapp.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.ViewPagerAdapter;
import com.app.yesbeautyapp.interfaces.ProductAddedToCartListener;
import com.app.yesbeautyapp.model.CartProductModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.model.ServiceModel;
import com.app.yesbeautyapp.model.SubServiceModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class SubServiceActivity extends BaseActivity implements ProductAddedToCartListener {

    private RelativeLayout bottomLayout;
    private ArrayList<SubServiceModel> serviceModelList;
    private ServiceModel serviceModel;
    private Menu menu;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ViewPagerAdapter viewPagerAdapter;
    private TextView minAmountTV;
    private TextView totalItemsTv;
    private TextView serviceChargeTV;
    public ArrayList<ProductModel> productModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_service);
    }

    @Override
    protected void initViews() {
        productModelArrayList = new ArrayList<>();
        bottomLayout = findViewById(R.id.bottomLayout);
        serviceChargeTV = (TextView) findViewById(R.id.tv_service_charge);
        minAmountTV = (TextView) findViewById(R.id.tv_min_charge_service);
        totalItemsTv = (TextView) findViewById(R.id.tv_total_items);
        FontUtils.changeFont(context, serviceChargeTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, minAmountTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(context, totalItemsTv, FONT_OPEN_SANS_SEMIBOLD_TTF);
        if (getIntent().getExtras() != null) {
            serviceModelList = getIntent().getExtras().getParcelableArrayList(MODEL_OBJ_ARRAY);
            serviceModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
            if (serviceModel != null) {
                settingTitle(serviceModel.getName());
            }
        }
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), serviceModelList);
        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout = findViewById(R.id.subservice_tabs);
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);

        settingHomeButton();
        //   setServiceRecyclerAdapter();
    }


    @Override
    protected void initContext() {
        context = SubServiceActivity.this;
        currentActivity = SubServiceActivity.this;
    }

    @Override
    protected void initListners() {
        //minAmountTV.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_min_charge_service: {
                toOpenCartDetailActivity();
                break;
            }
        }
    }

    @Override
    public void onAlertClicked(int alertType) {
        if (ALERT_TYPE_CLEAR_CART == alertType) {
            CartProductModel.getInstance().setProductModelArrayList(null);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (!productModelArrayList.isEmpty()) {
            alert(currentActivity, "Cart will be cleared", "Going back will clear your cart and you will have to start again.", getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_CLEAR_CART);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.cart_menu, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cart:
                //ActivityUtils.toOpenCartDetailActivity(currentActivity);
                break;
            case android.R.id.home: {
                if (!productModelArrayList.isEmpty()) {
                    alert(currentActivity, "Cart will be cleared", "Going back will clear your cart and you will have to start again.", getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_CLEAR_CART);
                    return false;
                }
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //setCount(SubServiceActivity.this, String.valueOf(cartDataSource.getCartDetailList().size()));
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void toOpenCartDetailActivity() {
        if (bundle == null) {
            bundle = new Bundle();
        }

        if (serviceModel != null) {
            bundle.putString("title", serviceModel.getName());
        }
        CartProductModel.getInstance().setProductModelArrayList(productModelArrayList);
        startActivity(currentActivity, CartDetailsActivity.class, bundle, true, REQUEST_TAG_PROFILE_ACTIVITY, true, ANIMATION_SLIDE_UP);
    }

    @Override
    public void productAddedToCart(ProductModel productModel) {
        if (!isProductAlreadyList(productModel)) {
            productModel.setProductQty(1);
            productModel.setServiceQty(1);
            productModelArrayList.add(0, productModel);
        }
        updateView();
    }

    private void updateView() {
        if (productModelArrayList == null) return;
        totalItemsTv.setText("Total- Rs:" + (int) totalProductAmount() + "/- " + productModelArrayList.size() + " item's");
        serviceChargeTV.setText("Service cost- Rs:" + (int) getServiceAmt() + "/-");

        if (getServiceAmt() >= 360) {
            bottomLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            minAmountTV.setText("Checkout >>");
            minAmountTV.setTextSize(20);
            minAmountTV.setOnClickListener(this);
        } else {
            bottomLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.grey));
            minAmountTV.setText(getString(R.string.serviceamttext));
            minAmountTV.setTextSize(16);
            minAmountTV.setOnClickListener(null);
        }
    }

    private boolean isProductAlreadyList(ProductModel productModel) {
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        int count = 0;
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                productModelArrayList.remove(count);
                return true;
            }
            count++;
        }
        return false;
    }

    public boolean isProductExist(ProductModel productModel) {
        if (productModelArrayList == null) return false;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel1 = productModelIterator.next();
            if (productModel1 == null) continue;
            if (productModel.getId().equalsIgnoreCase(productModel1.getId())) {
                return true;
            }

        }
        return false;
    }

    private double totalProductAmount() {

        double totalAmount = 0.0;
        if (productModelArrayList == null) return totalAmount;
        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String productCost = productModel.getProductCost();
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(productCost)) productCost = "0";
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = Double.parseDouble(productCost) + Double.parseDouble(serviceCost);
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    private double getServiceAmt() {
        double totalAmount = 0.0;
        if (productModelArrayList == null) return totalAmount;

        Iterator<ProductModel> productModelIterator = productModelArrayList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = Double.parseDouble(serviceCost);
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        productModelArrayList = CartProductModel.getInstance().getProductModelArrayList();
        if (productModelArrayList == null) productModelArrayList = new ArrayList<>();
        updateView();
        android.support.v4.app.Fragment fragment = viewPagerAdapter.getFragment(viewPager.getCurrentItem());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}

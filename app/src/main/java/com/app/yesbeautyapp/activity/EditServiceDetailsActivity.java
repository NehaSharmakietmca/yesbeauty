package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.dao.CartDataSource;
import com.app.yesbeautyapp.model.CartModel;
import com.app.yesbeautyapp.model.ServiceModel;
import com.app.yesbeautyapp.utils.ActivityUtils;
import com.app.yesbeautyapp.utils.CountDrawable;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;

public class EditServiceDetailsActivity extends BaseActivity {
    private TextView edtServiceType;
    private TextView edtQuantity;
    private TextView edtDateTextView;
    private TextView edtTimeTextView;
    private TextView edtMobileNumber;
    private TextView edtEmailId;
    private TextView edtAddress;
    private TextView amountTextView;
    private TextView fillDetailTextVIew;
    private TextView lblServiceTypeTV;
    private TextView lblQunatityTV;
    private TextView lblSelectDateTV;
    private EditText edtAdditionalComment;
    private TextView lbltimeTV;
    private Button addToCartButton;
    private LinearLayout linDate;
    private LinearLayout linTime;
    private ImageView gpsImageView;


    private Calendar mCalendar;
    private ServiceModel serviceModel;
    private double totalAmount;
    private double servicePrice;
    private CartModel cartModel;
    private Spinner timeSpinner;
    static final int TIME_DIALOG_ID = 1111;
    private int hour;
    private int minute;
    private Button bookNowButton;
    private CartDataSource cartDataSource;
    private Menu menu;

    private FusedLocationProviderClient mFusedLocationClient;
    protected static long MIN_UPDATE_INTERVAL = 30 * 1000;
    LocationRequest locationRequest;
    Location currentLocation = null;
    String myFormat = "dd MMM yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    String timeFormat = "hh:mm a";
    SimpleDateFormat stf = new SimpleDateFormat(timeFormat, Locale.US);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_service_details);
        mCalendar = Calendar.getInstance();
    }
    @Override
    protected void initViews() {
        settingTitle("Update Cart");
        settingHomeButton();
        cartDataSource = new CartDataSource(context);
        if (getIntent().getExtras() != null) {
            cartModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
            if (cartModel != null) {

                servicePrice = Double.parseDouble(cartModel.getTotalAmount());
                String.format("%.2f",servicePrice);
            }
        }
        gpsImageView = (ImageView) findViewById(R.id.img_gps);
        edtServiceType = (TextView) findViewById(R.id.edtServiceType);
        edtQuantity = (EditText) findViewById(R.id.edtQuantity);
        edtAdditionalComment = (EditText) findViewById(R.id.edtAdditionalComment);
        edtDateTextView = (TextView) findViewById(R.id.edtDateTextView);
        edtTimeTextView = (TextView) findViewById(R.id.edtTimeTextView);
        edtMobileNumber = (TextView) findViewById(R.id.edtMobileNumber);
        lblQunatityTV = (TextView) findViewById(R.id.lblQunatityTV);
        lblServiceTypeTV = (TextView) findViewById(R.id.lblServiceTypeTV);
        lblSelectDateTV = (TextView) findViewById(R.id.lblSelectDateTV);
        lbltimeTV = (TextView) findViewById(R.id.lbltimeTV);
        edtEmailId = (TextView) findViewById(R.id.edtEmailId);
        edtAddress = (TextView) findViewById(R.id.edtAddress);
        amountTextView = (TextView) findViewById(R.id.text_amount);
        bookNowButton = (Button) findViewById(R.id.bookNowButton);
        addToCartButton = (Button) findViewById(R.id.addToCartButton);
        timeSpinner = (Spinner) findViewById(R.id.timeSpinner);
        linDate = (LinearLayout) findViewById(R.id.lin_date);
        linTime = (LinearLayout) findViewById(R.id.lin_time);
        edtServiceType.setText(cartModel.getServiceName());
        FontUtils.changeFont(context, edtServiceType, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, edtQuantity, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, edtDateTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, edtTimeTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblQunatityTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblServiceTypeTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lblSelectDateTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, lbltimeTV, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, edtEmailId, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, edtAddress, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, amountTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookNowButton, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, addToCartButton, FONT_OPEN_SANS_REGULAR_TTF);
        edtAddress.setText(cartModel.getAddress());

        edtMobileNumber.setText(cartModel.getPhoneNumber());
        edtEmailId.setText(cartModel.getEmailId());
        edtQuantity.setText(cartModel.getQuantity());

        if (!TextUtils.isEmpty(edtQuantity.getText().toString())) {
            int numberOfService = Integer.parseInt(edtQuantity.getText().toString());
            totalAmount =  servicePrice * numberOfService;
            String.format("%.2f",(totalAmount));
            String amountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmount + "/-";

            amountTextView.setText(amountValue);
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(currentActivity);
        checkForLocationRequest();
        checkForLocationSettings();
        edtTimeTextView.setText(cartModel.getServiceTime());
        edtAdditionalComment.setText(cartModel.getComments());
        edtDateTextView.setText(cartModel.getServiceDate());
        edtTimeTextView.setText(cartModel.getServiceTime());
       // setDateTime();

    }

    @Override
    protected void initContext() {
        context = EditServiceDetailsActivity.this;
        currentActivity = EditServiceDetailsActivity.this;
    }

    @Override
    protected void initListners() {
        edtDateTextView.setOnClickListener(this);
        edtTimeTextView.setOnClickListener(this);
        addToCartButton.setOnClickListener(this);
        bookNowButton.setOnClickListener(this);
        linTime.setOnClickListener(this);
        linDate.setOnClickListener(this);
        gpsImageView.setOnClickListener(this);
        timeSpinner.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                TextView textView = (TextView) timeSpinner.getSelectedView();
                if (textView != null) {
                    textView.setTextColor(ContextCompat.getColor(EditServiceDetailsActivity.this, R.color.colorServiceText));
                    textView.setTextSize(15);
                    FontUtils.changeFont(EditServiceDetailsActivity.this, textView, FONT_OPEN_SANS_REGULAR_TTF);
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString())) {
                    totalAmount = 0;
                    String amountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmount + "/-";
                    amountTextView.setText(amountValue);
                    return;
                }
                int numberOfService = Integer.parseInt(s.toString());
                totalAmount = (int) (numberOfService * servicePrice);
                String amountValue = getResources().getString(R.string.rupees_symbol)+" "+totalAmount + "/-";
                amountTextView.setText(amountValue);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                Calendar calendar = Calendar.getInstance();
                Date date = new Date();
                try {
                    date = stf.parse(edtTimeTextView.getText().toString());
                    calendar.setTime(date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return new TimePickerDialog(this, timePickerListener, date.getHours(), date.getMinutes(),
                        false);

        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            updateTime(hour, minute);

        }

    };

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtDateTextView:
            case R.id.lin_date:
                toHideKeyboard();
                toOpenDatePicker();
                break;

            case R.id.edtTimeTextView:
            case R.id.lin_time:
                toHideKeyboard();
                showDialog(TIME_DIALOG_ID);
                break;

            case R.id.addToCartButton: {
                toHideKeyboard();
                if (isMandatoryFields()) {
                    initCartModel();
                    if (cartDataSource == null) {
                        return;
                    }
                    cartDataSource.updateCartDetails(cartModel);
                    toast(getString(R.string.msg_service_save), true);
                    setCount(EditServiceDetailsActivity.this, String.valueOf(cartDataSource.getCartDetailList().size()));
                    finish();
                    //
                }
                break;
            }
            case R.id.img_gps:
                alert(currentActivity, getResources().getString(R.string.alert_title_location), getResources().getString(R.string.alert_message_location), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_LOCATION_UPDATED);
                break;
            case R.id.bookNowButton: {
                toHideKeyboard();
                if (isMandatoryFields()) {
                    initCartModel();
                    ActivityUtils.toOpenBookNowActivity(currentActivity, cartModel);
                }
                break;
            }
        }
    }

    private void toOpenDatePicker() {
        try {
           Date date =  sdf.parse(edtDateTextView.getText().toString());
           Date currentDate = new  Date();
          currentDate = sdf.parse(sdf.format(currentDate));
          if(date.after(currentDate)) {
              mCalendar.setTime(date);
          }
        } catch (ParseException e) {
            int year = mCalendar.get(Calendar.YEAR);
            int month = mCalendar.get(Calendar.MONTH);
            int day = mCalendar.get(Calendar.DAY_OF_MONTH);
            e.printStackTrace();
        }
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpDialog = new DatePickerDialog(currentActivity, dobListner, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        mCalendar.add(Calendar.DATE, 30);
        long end = mCalendar.getTimeInMillis();
        dpDialog.getDatePicker().setMaxDate(end);
        dpDialog.show();
    }

    DatePickerDialog.OnDateSetListener dobListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.YEAR, year);
            mCalendar.set(Calendar.MONTH, monthOfYear);
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd MMM yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            edtDateTextView.setText(sdf.format(mCalendar.getTime()));
        }
    };

    @Override
    public void onAlertClicked(int alertType) {
        switch (alertType) {
            case ALERT_TYPE_LOCATION_UPDATED:
                edtAddress.setText("");
                checkForLocationRequest();
                checkForLocationSettings();
                break;
        }

    }

    private boolean isMandatoryFields() {
        edtQuantity.setError(null);
        edtDateTextView.setError(null);
        edtTimeTextView.setError(null);
        edtMobileNumber.setError(null);
        edtEmailId.setError(null);
        edtAddress.setError(null);
        if (edtQuantity.getText().toString().isEmpty()) {
            edtQuantity.setError(getResources().getString(R.string.error_quantity));
            edtQuantity.requestFocus();
            return false;
        } else if (Integer.parseInt(edtQuantity.getText().toString()) <= 0) {
            edtQuantity.setError(getResources().getString(R.string.errorMinimumQuantity));
            edtQuantity.requestFocus();
            return false;
        } else if (edtDateTextView.getText().toString().isEmpty()) {
            toast(getString(R.string.error_date), true);
            // edtDateTextView.setError(getResources().getString(R.string.error_empty_date));
            edtDateTextView.requestFocus();
            return false;
        }else if(!validateDate()){
            edtDateTextView.requestFocus();
            return false;
        }
        else if (edtTimeTextView.getText().toString().isEmpty()) {
            toast(getString(R.string.error_empty_time), true);
            // edtTimeTextView.setError(getResources().getString(R.string.error_empty_time));
            edtTimeTextView.requestFocus();
            return false;
        }else if(!validateTime()){
            edtTimeTextView.requestFocus();
            return false;
        }
        else if (!Validator.getInstance().validatePhoneNumber(EditServiceDetailsActivity.this, edtMobileNumber.getText().toString()).equals("")) {
            String numberError = Validator.getInstance().validatePhoneNumber(EditServiceDetailsActivity.this, edtMobileNumber.getText().toString());
            edtMobileNumber.setError(numberError);
            edtMobileNumber.requestFocus();
            return false;

        } else if (!Validator.getInstance().isValidEmail(EditServiceDetailsActivity.this, edtEmailId.getText().toString()).equals("")) {
            String emailError = Validator.getInstance().isValidEmail(EditServiceDetailsActivity.this, edtEmailId.getText().toString());
            edtEmailId.setError(emailError);
            edtEmailId.requestFocus();
            return false;
        } else if (edtAddress.getText().toString().isEmpty()) {
            edtAddress.setError(getResources().getString(R.string.edit_text_address));
            edtAddress.requestFocus();
            return false;
        }
        return true;
    }

/*    private void setDateTime() {
        Date d = new Date();
        String myFormat = "dd MMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        edtDateTextView.setText(sdf.format(d));
        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        //   updateTime(hour, minute);
    }*/

    private void updateTime(int hours, int mins) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";
        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        edtTimeTextView.setText(aTime);
    }

    private void initCartModel() {
        String serviceTypeLabel = "";

        cartModel.setServiceId(cartModel.getServiceId());
        cartModel.setServiceName(cartModel.getServiceName());
        //cartModel.setServiceImage(cartModel.get());
        cartModel.setQuantity(edtQuantity.getText().toString());
        cartModel.setServiceTypeLabel(serviceTypeLabel);
        cartModel.setServiceDate(edtDateTextView.getText().toString());
        cartModel.setServiceTime(edtTimeTextView.getText().toString());
        cartModel.setPhoneNumber(edtMobileNumber.getText().toString());
        cartModel.setEmailId(edtEmailId.getText().toString());
        cartModel.setAddress(edtAddress.getText().toString());
        cartModel.setComments(edtAdditionalComment.getText().toString());
        cartModel.setTotalAmount(totalAmount + "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cart:
                ActivityUtils.toOpenCartDetailActivity(currentActivity);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        setCount(EditServiceDetailsActivity.this, String.valueOf(cartDataSource.getCartDetailList().size()));
        return true;
    }

    public void setCount(Context context, String count) {
        MenuItem menuItem = menu.findItem(R.id.menu_cart);
        LayerDrawable icon = (LayerDrawable) menuItem.getIcon();
        CountDrawable badge;
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);
    }

    private void startLocationPermissionRequest(int requestCode) {
        ActivityCompat.requestPermissions(currentActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    private void requestPermissions(final int requestCode) {
        startLocationPermissionRequest(requestCode);
    }

    public void checkForLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    //Check for location settings.
    public void checkForLocationSettings() {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(currentActivity);

            settingsClient.checkLocationSettings(builder.build())
                    .addOnSuccessListener(currentActivity, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            callCurrentLocation();
                        }
                    })
                    .addOnFailureListener(currentActivity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {


                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(currentActivity, 99);
                                    } catch (IntentSender.SendIntentException sie) {
                                        sie.printStackTrace();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    Toast.makeText(currentActivity, "Setting change is not available.Try in another device.", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (resultCode == currentActivity.RESULT_OK) {
                callCurrentLocation();
            }
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == RECEIVE_LOCATION_PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callCurrentLocation();
            }
        }
    }

    public void callCurrentLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(currentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(RECEIVE_LOCATION_PERMISSION_REQUEST);
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    currentLocation = locationResult.getLastLocation();
                    //toast(context, "Your Location : " + getCompleteAddressString());
                    String address = SharedPreferenceUtils.getInstance(context).getString(AppConstants.USER_ADDRESS);
                    if (!TextUtils.isEmpty(address)) {
                        edtAddress.setText(address);
                    } else {
                        edtAddress.setText(getCompleteAddressString());
                    }
                }
            }, Looper.myLooper());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getCompleteAddressString() {
        String strAdd = "";
        if (currentLocation == null) return strAdd;
        Geocoder geocoder = new Geocoder(currentActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress == null) return strAdd;
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    if (!TextUtils.isEmpty(returnedAddress.getAddressLine(i)))
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private boolean validateDate() {
                try {
                    Date serviceDate = sdf.parse(edtDateTextView.getText().toString());
                    Date currentDate = new Date();
                    currentDate = sdf.parse(sdf.format(currentDate));
                    if (serviceDate.before(currentDate)) {
                        alert(currentActivity, "Alert", getResources().getString(R.string.alert_message_edit_cart_date), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, true, ALERT_TYPE_UPDATE_CART);
                        return false;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }


        return true;
    }
    private boolean validateTime() {
        try {
            mCalendar = Calendar.getInstance();
            Date date = sdf.parse(edtDateTextView.getText().toString());
            Date serviceTime = stf.parse(edtTimeTextView.getText().toString());
            Date currentTime = new Date();
            Date currentDate = sdf.parse(sdf.format(currentTime)) ;
            if(!date.after(currentDate)) {
                currentTime = stf.parse(stf.format(currentTime));
                if (serviceTime.before(currentTime)) {
                    alert(currentActivity, "Alert", getResources().getString(R.string.alert_message_edit_cart_time), getResources().getString(R.string.label_ok_button), getResources().getString(R.string.label_cancel_button), false, true, ALERT_TYPE_UPDATE_CART);
                    return false;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return true;

    }

}

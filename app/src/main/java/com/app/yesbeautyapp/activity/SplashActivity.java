package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;
import com.app.yesbeautyapp.utils.Validator;

public class SplashActivity extends BaseActivity {

    private TextView appNameTV;

    @Override
    protected void initViews() {
        appNameTV = findViewById(R.id.text_app_name);
        FontUtils.changeFont(context, appNameTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        if (!Validator.isNetworkAvailable(currentActivity)) {
            toast(getResources().getString(R.string.alert_message_no_network), true);
        }
    }

    @Override
    protected void initContext() {
        currentActivity = SplashActivity.this;
        context = SplashActivity.this;

    }

    @Override
    protected void initListners() {

    }

    @Override
    protected boolean isActionBar() {
        return false;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 23) {

                    if (!checkReceiveSmsPermission()) {
                        requestPermission();
                    } else {
                        moveToNextActivity();
                    }
                } else {
                    moveToNextActivity();
                }

            }
        }, SPLASH_TIME);

    }

    private boolean checkReceiveSmsPermission() {
        int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result3 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        //int result4 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED &&
                result3 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, RECEIVE_SMS_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RECEIVE_SMS_PERMISSION_REQUEST:
                moveToNextActivity();
                break;
        }
    }

    private void moveToNextActivity() {
        //startActivity(currentActivity, PaymentMethodActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
        boolean isUserLogin = SharedPreferenceUtils.getInstance(context).getBoolean(AppConstants.IS_USER_LOGIN);
        if (isUserLogin) {
            int userType = SharedPreferenceUtils.getInstance(currentActivity).getInteger(USERTYPE);
            if (userType == 1) {
                startActivity(currentActivity, Dashboard.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
            } else {
                startActivity(currentActivity, VendorDashBoardActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
            }
        } else {
            startActivity(currentActivity, LoginActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
        }
        finish();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onAlertClicked(int alertType) {

    }
}

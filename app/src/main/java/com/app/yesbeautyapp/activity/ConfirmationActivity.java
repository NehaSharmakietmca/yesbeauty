package com.app.yesbeautyapp.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class ConfirmationActivity extends BaseActivity {
    private TextView bookingHeadingTextView;
    private TextView bookingConfirmationTextView;
    private TextView bookingIdLabelTextView;
    private TextView bookingIdValueTextView;
    private BookingModel bookingModel;
    private Button backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.confirmation));
       // settingHomeButton();
        bookingConfirmationTextView = findViewById(R.id.booking_confirmation_msg);
        bookingIdValueTextView = findViewById(R.id.text_booking_id_value);
        bookingIdLabelTextView = findViewById(R.id.text_booking_id);
        backButton = findViewById(R.id.btn_back);
        bookingHeadingTextView = findViewById(R.id.text_booking_heading);
        FontUtils.changeFont(context, bookingConfirmationTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingIdValueTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingIdLabelTextView, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(context, bookingHeadingTextView, FONT_OPEN_SANS_REGULAR_TTF);

        if (getIntent().getExtras() != null) {
            bookingModel = getIntent().getExtras().getParcelable(MODEL_OBJ);
            assert bookingModel != null;
            bookingIdValueTextView.setText("#"+bookingModel.getBookingId());
        }

    }

    @Override
    protected void initContext() {
        context = this;
        currentActivity = ConfirmationActivity.this;

    }

    @Override
    protected void initListners() {
        backButton.setOnClickListener(this);

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                onBackPressed();
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        startActivity(currentActivity, Dashboard.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
        finish();
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

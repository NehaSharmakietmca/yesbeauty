package com.app.yesbeautyapp.activity;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.model.LoginOrSignUpModel;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;
import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.Validator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private EditText mobileNumberEditText;
    private Button sendButton;
    private GoogleApiClient apiClient;
    private final static int RESOLVE_HINT = 012;
    private ProgressDialog progressBar;
    private TextView appNameTextView;
    private TextView skipContinueTextView;
    private LinearLayout skipLL;
    private boolean isCheckout;
    private LinearLayout phoneDialogLinearLayout;
    private LinearLayout phonenumberoneLL;
    private LinearLayout phoneNumbertwoLL;
    private TextView phoneNumberOneTextView;
    private TextView phoneNumberTwoTextView;
    private TextView noneOftheAboveTextView;
    private CheckBox termAndConditionsCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        apiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.CREDENTIALS_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        apiClient.connect();
    }

    @Override
    protected void initViews() {
        skipLL = findViewById(R.id.skip_ll);
        mobileNumberEditText = (EditText) findViewById(R.id.mobileNumberEditText);
        sendButton = (Button) findViewById(R.id.sendButton);
        appNameTextView = (TextView) findViewById(R.id.text_app_name);
        phoneDialogLinearLayout = (LinearLayout) findViewById(R.id.lin_phone_number);
        phonenumberoneLL = (LinearLayout) findViewById(R.id.lin_phone_number1);
        phoneNumbertwoLL = (LinearLayout) findViewById(R.id.lin_phone_number2);
        phoneNumberOneTextView = (TextView) findViewById(R.id.text_mobile_number_one);
        phoneNumberTwoTextView = (TextView) findViewById(R.id.text_mobile_number_two);
        termAndConditionsCheckBox = (CheckBox) findViewById(R.id.check_term_conditions);
        noneOftheAboveTextView = (TextView) findViewById(R.id.text_none_of_the_above);
        if (getIntent() != null && getIntent().getExtras() != null) {
            isCheckout = getIntent().getExtras().getBoolean("isCheckout", false);
        }

        skipContinueTextView = (TextView) findViewById(R.id.text_skip_continue);
        FontUtils.changeFont(this, mobileNumberEditText, FONT_OPEN_SANS_REGULAR_TTF);
        FontUtils.changeFont(this, sendButton, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, appNameTextView, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, skipContinueTextView, FONT_OPEN_SANS_SEMIBOLD_TTF);
        FontUtils.changeFont(this, (TextView) findViewById(R.id.text_we_will), FONT_OPEN_SANS_REGULAR_TTF);

        phoneDialogLinearLayout.setVisibility(View.GONE);
        if (checkPermission()) {
            // detectPhoneNumber();
        } else {
            //  requestPermission();
        }
        setTermAndConditionsCheckBox();
    }

    @Override
    protected void initContext() {
        currentActivity = LoginActivity.this;
        context = LoginActivity.this;
    }

    @Override
    protected void initListners() {
        sendButton.setOnClickListener(this);
        skipLL.setOnClickListener(this);
        phonenumberoneLL.setOnClickListener(this);
        phoneNumbertwoLL.setOnClickListener(this);
        noneOftheAboveTextView.setOnClickListener(this);
    }

    @Override
    protected boolean isActionBar() {
        return false;
    }

    @Override
    protected boolean isHomeButton() {
        return false;
    }


    @Override
    public void onAlertClicked(int alertType) {
    }

    private boolean checkPermission() {
        int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result1 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, RECEIVE_SMS_PERMISSION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RECEIVE_SMS_PERMISSION_REQUEST:
                if (checkPermission()) {
                    detectPhoneNumber();
                }
                break;
        }
    }

    private void detectPhoneNumber() {

        String phoneNumbers[] = new String[2];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<SubscriptionInfo> subscription = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
            for (int i = 0; i < subscription.size(); i++) {
                SubscriptionInfo info = subscription.get(i);
                phoneNumbers[i] = info.getNumber();
                if (i == 1) {
                    break;
                }
            }
        } /*else {
         *//*  TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (checkPermission())
                phoneNumbers[0] = tMgr.getLine1Number();*//*
        }*/

        if (TextUtils.isEmpty(phoneNumbers[0]) && TextUtils.isEmpty(phoneNumbers[1])) {
            phoneDialogLinearLayout.setVisibility(View.GONE);
        } else {
            phoneDialogLinearLayout.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(phoneNumbers[0])) {
            phonenumberoneLL.setVisibility(View.VISIBLE);
            String phoneNumber = "";
            if (phoneNumbers[0].length() == 12) {
                phoneNumber = phoneNumbers[0].substring(2, phoneNumbers[0].length());
            } else {
                phoneNumber = phoneNumbers[0];
            }
            phoneNumberOneTextView.setText(phoneNumber);
        } else {
            phonenumberoneLL.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(phoneNumbers[1])) {
            phoneNumbertwoLL.setVisibility(View.VISIBLE);
            String phoneNumber = "";
            if (phoneNumbers[1].length() == 12) {
                phoneNumber = phoneNumbers[1].substring(2, phoneNumbers[1].length());
            } else {
                phoneNumber = phoneNumbers[1];
            }
            phoneNumberTwoTextView.setText(phoneNumber);
        } else {
            phoneNumbertwoLL.setVisibility(View.GONE);
        }
    }

    private boolean isMandatoryFields() {
        //mobileNumberEditText.setError(null);
        if (mobileNumberEditText.getText() == null || mobileNumberEditText.getText().toString().isEmpty()) {
            Toast.makeText(LoginActivity.this, "Please Enter Mobile Number", Toast.LENGTH_LONG).show();
            mobileNumberEditText.requestFocus();
            return false;
        } else if (!Validator.getInstance().validateNumber(context, mobileNumberEditText.getText().toString()).equals("")) {
            String numberError = Validator.getInstance().validateNumber(context, mobileNumberEditText.getText().toString());
            Toast.makeText(LoginActivity.this, numberError, Toast.LENGTH_LONG).show();
            mobileNumberEditText.requestFocus();
            return false;
        } else if (!termAndConditionsCheckBox.isChecked()) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.plese_check_terms_condition), Toast.LENGTH_LONG).show();
            mobileNumberEditText.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendButton: {
                toHideKeyboard();
                if (Validator.getInstance().isNetworkAvailable(currentActivity)) {
                    if (isMandatoryFields()) {
                        loginOrSignUp();
                    }
                } else {
                    alert(currentActivity, getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_message_no_network), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, false, ALERT_TYPE_NO_NETWORK);
                }
                break;

            }
            case R.id.skip_ll:
                if (isCheckout) {
                    finish();
                } else {
                    startActivity(currentActivity, Dashboard.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                    finish();
                }
                break;
            case R.id.lin_phone_number1:
                String phonenumberOne = phoneNumberOneTextView.getText().toString();
                mobileNumberEditText.setText(phonenumberOne);
                phoneDialogLinearLayout.setVisibility(View.GONE);
                sendButton.callOnClick();
                break;

            case R.id.lin_phone_number2:
                String phonenumberTwo = phoneNumberTwoTextView.getText().toString();
                mobileNumberEditText.setText(phonenumberTwo);
                phoneDialogLinearLayout.setVisibility(View.GONE);
                sendButton.callOnClick();
                break;
            case R.id.text_none_of_the_above:
                phoneDialogLinearLayout.setVisibility(View.GONE);
                break;
        }

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                apiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // requestHint();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                String phoneNumber = credential.getId();
                if (phoneNumber != null) {
                    phoneNumber = phoneNumber.substring(3, 13);
                    mobileNumberEditText.setText(phoneNumber);
                }
            }
        }
    }

    private void setTermAndConditionsCheckBox() {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I have read and agree to all ");
        spanTxt.append("Terms and Conditions");
        final ForegroundColorSpan fcs = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent));
        spanTxt.setSpan(fcs, spanTxt.length() - "Terms and Conditions".length(), spanTxt.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
              /*  Toast.makeText(getApplicationContext(), "Terms of services Clicked",
                        Toast.LENGTH_SHORT).show();
                Uri uri = Uri.parse("https://s3.amazonaws.com/konnect-toc/G4S_Konnect_TOC.html"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/
            }
        }, spanTxt.length() - "Terms and Conditions".length(), spanTxt.length(), 0);


        termAndConditionsCheckBox.setMovementMethod(LinkMovementMethod.getInstance());
        termAndConditionsCheckBox.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    private void loginOrSignUp() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JSONObject jsonSignUpRequest = null;
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            LoginOrSignUpModel.getInstance().setPhone(mobileNumberEditText.getText().toString());
            jsonSignUpRequest = new JSONObject(gson.toJson(LoginOrSignUpModel.getInstance()));
            Log.e("jsonSignUpRequest", jsonSignUpRequest.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String URL_SIGN_UP = URL_LOGIN;
        JsonObjectRequest signUpRequest = new JsonObjectRequest(Request.Method.POST, URL_SIGN_UP, jsonSignUpRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    cancelProgressDialog();
                    JSONArray jsonArray = response.getJSONArray(RESPONCE_MESSAGE);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject messageObj = jsonArray.getJSONObject(0);
                        if (messageObj != null) {
                            String error = messageObj.getString("error");
                            if (error != null && error.equalsIgnoreCase("false")) {
                                String type = messageObj.getString("type");
                                if (bundle == null) {
                                    bundle = new Bundle();
                                }
                                bundle.putBoolean("isCheckout", isCheckout);
                                bundle.putString(USER_MOBILE_NO, mobileNumberEditText.getText().toString());

                                if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("new")) {
                                    startActivity(currentActivity, RegisterActivity.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                                } else {
                                    startActivity(currentActivity, OtpValidation.class, bundle, false, REQUEST_TAG_NO_RESULT, true, ANIMATION_SLIDE_UP);
                                }
                                if (isCheckout) {
                                    finish();
                                }
                            } else {
                                alert(currentActivity, getString(R.string.msg_reg_failed), getString(R.string.msg_reg_failed), getResources().getString(R.string.alert_ok_button_text_no_network), getResources().getString(R.string.alert_cancel_button_text_no_network), true, true, ALERT_TYPE_NO_NETWORK);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getResources().getString(R.string.nwk_error_sign_up), true);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };
        AplusoneApplication.getInstance().addToRequestQueue(signUpRequest);
    }

}




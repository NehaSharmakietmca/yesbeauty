package com.app.yesbeautyapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.adapter.NotificationAdapter;
import com.app.yesbeautyapp.application.AplusoneApplication;
import com.app.yesbeautyapp.model.NotificationModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends BaseActivity {

    private RecyclerView recyclerNotification;
    private TextView noNotificationsTV;
    private ArrayList<NotificationModel> notificationList;
    private NotificationAdapter orderDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
    }

    @Override
    protected void initViews() {
        settingTitle(getString(R.string.title_notifications));
        settingHomeButton();
        if (notificationList == null) {
            notificationList = new ArrayList<>();
        }
        recyclerNotification = findViewById(R.id.recyclerNotification);
        noNotificationsTV = findViewById(R.id.noNotificationsTV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(currentActivity);
        recyclerNotification.setLayoutManager(layoutManager);
        orderDetailAdapter = new NotificationAdapter(currentActivity, notificationList);
        recyclerNotification.setAdapter(orderDetailAdapter);
        getNotificationList();
    }

    @Override
    protected void initContext() {
        context = NotificationActivity.this;
        currentActivity = NotificationActivity.this;
    }

    @Override
    protected void initListners() {

    }

    @Override
    protected boolean isActionBar() {
        return true;
    }

    @Override
    protected boolean isHomeButton() {
        return true;
    }

    @Override
    public void onAlertClicked(int alertType) {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getNotificationList() {

        progressDialog(context, context.getString(R.string.pdialog_message_loading), context.getString(R.string.pdialog_message_loading), false, false);

        JsonObjectRequest getNotificationRequest = new JsonObjectRequest(Request.Method.POST, GET_NOTIFICATION_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ((BaseActivity) currentActivity).logTesting("responce is", response.toString(), Log.ERROR);
                cancelProgressDialog();
                try {
                    Gson gson = new Gson();
                    List<NotificationModel> bookingModelList = Arrays.asList(gson.fromJson(response.getJSONArray(RESPONCE_MESSAGE).toString(), NotificationModel[].class));
                    notificationList.clear();
                    notificationList.addAll(bookingModelList);
                    orderDetailAdapter.notifyDataSetChanged();

                    if (notificationList.isEmpty()) {
                        noNotificationsTV.setVisibility(View.VISIBLE);
                        recyclerNotification.setVisibility(View.GONE);
                    } else {
                        noNotificationsTV.setVisibility(View.GONE);
                        recyclerNotification.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                cancelProgressDialog();
                toast(getString(R.string.alert_message_no_network), true);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        AplusoneApplication.getInstance().addToRequestQueue(getNotificationRequest);
    }

}

package com.app.yesbeautyapp.interfaces;

import com.app.yesbeautyapp.model.CartModel;

public interface BookingHistoryListener {
     void rescheduleClickListener(CartModel cartModel, int pos);
     void cancelClickListener(CartModel cartModel, int pos);

}

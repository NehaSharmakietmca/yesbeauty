package com.app.yesbeautyapp.interfaces;

import com.app.yesbeautyapp.model.ProductModel;

public interface ProductAddedToCartListener {
    void productAddedToCart(ProductModel productModel);
}

package com.app.yesbeautyapp.constants;

import com.app.yesbeautyapp.adapter.HomeProductListAdapter;

/**
 * Created by Mayank on 27/04/2016.
 */
public interface AppConstants {


    /*alert type*/
    public static final int ALERT_TYPE_NO_NETWORK = 0x01;
    public static final int ALERT_TYPE_LOGOUT = 0x02;


    public static final int ALERT_TYPE_CONFIRM_ORDER = 0x01;

    /* animation type*/
    public static final int ANIMATION_SLIDE_UP = 0x01;
    public static final int ANIMATION_SLIDE_LEFT = 0x02;


    /* splash screen*/
    public static final int SPLASH_TIME = 3000;


    /* App Tag*/
    public static final String APP_NAME = "Aplusone";
    public static final String DEVICE_TYPE = "Android";


    /* Request Tag*/
    public static final int REQUEST_TAG_NO_RESULT = 0x01;
    public static final int REQUEST_UPDATEADDRESS = 0x02;

    /* FORGOT_PASSWORD_REQUET_KEYS*/
    public static final String KEY_EMAIL = "email";
    public static final String KEY_BOOLEAN = "bookingHistory";

    public static final int RECEIVE_SMS_PERMISSION_REQUEST = 0x01;
    public static final int RECEIVE_LOCATION_PERMISSION_REQUEST = 0x03;
    public static final int RECEIVE_PHONE_CALL_PERMISSION = 0x04;


    public static final String KEY_ERROR = "error";

    public static final String KEY_ID = "id";

    /* otp  */
    public static final long timeafterwhichRecieverUnregister = 900000l;
    public static final long longTimeAfterWhichButtonEnable = 31000;
    public static final long longTotalVerificationTime = 31000;
    public static final long longOneSecond = 1000;
    public static final String KEY_OTP = "OtpIs";
    public static final String EVENT_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    public static final int PRIORITY_HIGH = 1000;
    public static final int RESET_LIMIT = 5;


    public static final int MAINBOARD = 0;
    public static final int ORDER_HISTORY = 1;
    public static final int PROFILE = 2;
    public static final int WALLET = 3;
    public static final int ABOUT_US = 4;
    public static final int CONTACT_US = 5;
    public static final int TERMS_AND_SERVICES = 6;
    public static final int PRIVACY_POLICY = 7;
    public static final int FAQ = 8;
    public static final int LOGOUT = 9;


    /* Dashboard*/
    public static final int APP_EXIT_TIME = 2000;
    public static final String MESSAGE = "message";

    public static final String FCM_TOKEN = "fcmToken";

    public static String keyName = "name";


    public static final String KEY_APPLICATION_NAME = "Aplusone";

    public static final String keyOtp = "OtpIs";
    public static final int highPriority = 2147483647;
    public static final int limitReset = 5;

    public static final String keyMobileNo = "mobileNumber";
    public static final String keyIsNumberVerified = "isNumberVerified";


    public static final String nameSharedPreference = "Aplusone";
    public static final String entityNotPresent = "notPresent";
    public static final String entityNotPresentInt = "0";
    public static final String keyUserNumber = "phone";

    public static final int REQUEST_TAG_EDIT_PROFILE_ACTIVITY = 0x93;
    public static final String eventSmsReceived = "android.provider.Telephony.SMS_RECEIVED";

    /*Font*/
    public static final String FONT_OPEN_SANS_REGULAR_TTF = "font/Roboto-Regular.ttf";
    public static final String FONT_OPEN_SANS_SEMIBOLD_TTF = "font/Roboto-Medium.ttf";


    /*Neha*/
    public static final String USER_PHONE = "mobile";
    public static final String USER_ID = "authid";
    public static final String ORDER_ID = "ordid";
    public static final String USER_NAME = "fname";
    public static final String USER_LAST_NAME = "lname";
    public static final String USER_EMAIL_ID = "email";
    public static final String USER_ADDRESS = "address";
    public static final String USER_STREET = "street";
    public static final String USER_BUILDING = "building";
    public static final String SCHEDULE_DATE = "schedule_date";
    public static final String SCHEDULE_TIME = "schedule_time";

    public static final String USER_PROFILE_IMAGE = "photo";
    public static final String USER_EMAIL = "email";
    public static final String LOGIN_WITH = "loginWith";
    public static final String USER_MOBILE_NO = "phone";
    public static final String USERTYPE = "user_type";
    public static final String IS_USER_LOGIN = "is_user_login";
    public static final String KEY_DATA = "data";

    public static final String USER_WALLET_BALANCE = "credits";


    public static final int PICK_IMAGE_CAMERA = 0x01;
    public static final int PICK_IMAGE_GALLERY = 0x02;
    public static int REQUEST_TAG_PICK_IMAGE = 0x03;
    public static int REQUEST_TAG_Image_Capture = 0x04;
    public static final int CAMERA_PERMISSION_REQUEST = 0x05;
    public static final int REQUEST_TAG_PROFILE_ACTIVITY = 0x06;
    public static final int REQUEST_TAG_UPDATE_PROFILE = 0x07;
    public static final int ALERT_TYPE_CLEAR_CART = 0x08;
    public static final int ALERT_TYPE_DELETE_CART_ITEM = 0x09;
    public static final int REQUEST_TAG_OTP_VALIDATION = 0x010;
    public static final int REQUEST_TAG_PAYMENT = 0x11;
    public static final int ALERT_TYPE_CANCEL_ORDER = 0x12;
    public static final int REQUEST_TAG_GET_PROMO_CODE = 0x13;
    public static final int REQUEST_TAG_PROMOCODE = 0x014;
    public static final int ALERT_TYPE_IMAGE_UPLOAD = 0x15;
    public static final int ALERT_TYPE_LOCATION_UPDATED = 0x16;
    public static final int ALERT_TYPE_UPDATE_CART = 0x17;
    public static final int REQUEST_TAG_RESCHEDULE = 0x18;
    public static final int REQUEST_TAG_CANCEL = 0x19;
    public static final int ALERT_TYPE_CANCEL_BOOKING = 0x20;
    public static final int ALERT_TYPE_RESCHEDULE_BOOKING = 0x21;


    /* Request Tag*/
    public static final String RESPONCE_MESSAGE = "response";
    public static final String RESPONCE_ERROR = "error";
    public static final String BASE_URL_IMAGES = "http://2kprojects.com/uploads/";
    public static final String BASE_URL_API = "http://2kprojects.com/app";
    public static final String URL_GET_ALL_SERVICE = BASE_URL_API + "/get_cat.php";
    public static final String URL_GET_TESTIMONIAL = BASE_URL_API + "/get_testimonials.php";
    public static final String URL_GET_BANNERIMG = BASE_URL_API + "/get_banner.php";
    //public static final String URL_GET_SUB_SERVICE = BASE_URL_API + "/get_subcat.php";
    public static final String URL_GET_SUB_SERVICE = BASE_URL_API + "/get_subcatn.php";
    public static final String URL_LOGIN = BASE_URL_API + "/check_login.php";
    public static final String URL_REGISTER = BASE_URL_API + "/user_register.php";
    public static final String URL_GET_SCHEDULE = BASE_URL_API + "/get_timeslot.php";
    public static final String URL_GET_PROFILE = BASE_URL_API + "/get_userinfo.php";
    public static final String URL_UPDATE_BIRTHANIV = BASE_URL_API + "/update_impdate.php";
    public static final String URL_UPDATE_PROFILE = BASE_URL_API + "/update_info.php";
    public static final String URL_UPDATE_ADDRESS = BASE_URL_API + "/update_fulladdress.php";
    public static final String URL_APPLYCOUPON = BASE_URL_API + "/check_coupon.php";
    public static final String PROCEED_BOOKING_URL = BASE_URL_API + "/place_order.php";
    public static final String URL_GET_BOOKING_HISTORY = BASE_URL_API + "/get_history.php";
    public static final String URL_GET_ONGOINGBOOKING_HISTORY = BASE_URL_API + "/get_upcoming.php";
    public static final String GET_NOTIFICATION_URL = BASE_URL_API + "/get_notification.php";
    public static final String ABOUTUS_URL = BASE_URL_API + "/aboutus.php";
    public static final String PRIVACY_URL = BASE_URL_API + "/privacy_policy.php";
    public static final String TERMSCONDITIONS_URL = BASE_URL_API + "/terms_conditions.php";
    public static final String FAQS_URL = BASE_URL_API + "/faqs.php";
    public static final String SEARCHSERVICE_URL = BASE_URL_API + "/get_search_services.php";
    public static final String URL_GET_ORDER_DETAILS = BASE_URL_API + "/get_order_details.php";
    public static final String URL_GET_HOMEPRODUCT = BASE_URL_API + "/get_home_product.php";
    public static final String URL_RESCHEDULE_BOOKING = BASE_URL_API + "/reschedule_order.php";


    public static final String PARENT_ID = "cid";
    public static final String MODEL_OBJ = "model";
    public static final String MODEL_OBJ_ARRAY = "modelArray";
    public static final String CART_MODEL_OBJ_ARRAY = "cartModelArray";
    public static final String URL_GET_ALL_PROMOCODE = BASE_URL_API + "/getPromocodeList";
    public static final String URL_PLACE_ORDER = BASE_URL_API + "/proceedOrder";
    public static final String APPLY_PROMOCODE = BASE_URL_API + "/applyPromocode";
    public static final String UPLOAD_PROFILE_PIC_NAME = BASE_URL_API + "/updateProfilePicUrl";
    public static final String UPLOAD_PROFILE_IMAGE = BASE_URL_API + "/uploadProfileImage";
    public static final String URL_CANCEL_BOOKING = BASE_URL_API + "/cancel_order.php";

    public static final String UPLOAD_USER_IMAGE_URL = BASE_URL_API + "uploadpprofileimage";
    public static final String UPDATE_USER_DETAILS_URL = BASE_URL_API + "updateUserProfile";

    public static final String GET_USER_DETAILS_URL = BASE_URL_API + "getUserProfile";
    public static final String UPDATE_USER_IMAGE_URL = BASE_URL_API + "updateUserImage";
    public static final String ADD_TO_CART_BUTTON =  "Add to Cart";
    public static final String ADDED =  "Remove";


}

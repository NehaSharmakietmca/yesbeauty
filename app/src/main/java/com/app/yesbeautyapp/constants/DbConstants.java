package com.app.yesbeautyapp.constants;


public interface DbConstants {

    /* Sqlite Db*/
    public static final String DATABASE_NAME = "aplusone.db";
    public static final int DATABASE_VERSION = 4;

    /* Tables*/
    public static final String TABLE_CART_DETAILS = "tbl_user_cart";

    /*Table User Columns */
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_SERVICE_ID = "serviceId";
    public static final String COLUMN_SERVICE_NAME = "serviceName";
    public static final String COLUMN_SERVICE_IMAGE = "serviceImage";
    public static final String COLUMN_SERVICE_TYPE_LABEL = "serviceTypeLabel";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_SERVICE_DATE = "serviceDate";
    public static final String COLUMN_SERVICE_TIME = "serviceTime";
    public static final String COLUMN_PHONE_NUMBER = "phoneNumber";
    public static final String COLUMN_EMAIL_ID = "emailId";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_COMMENTS = "comments";
    public static final String COLUMN_TOTAL_AMT = "totalAmount";

}


package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubSubServiceModel implements Parcelable {

    private String id;
    private String name;
    private String image;
    @SerializedName("product")
    private List<ProductModel> productModels;
    private boolean isSelected;


    protected SubSubServiceModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        image = in.readString();
        productModels = in.createTypedArrayList(ProductModel.CREATOR);
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeTypedList(productModels);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubSubServiceModel> CREATOR = new Creator<SubSubServiceModel>() {
        @Override
        public SubSubServiceModel createFromParcel(Parcel in) {
            return new SubSubServiceModel(in);
        }

        @Override
        public SubSubServiceModel[] newArray(int size) {
            return new SubSubServiceModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductModel> getProductModels() {
        return productModels;
    }

    public void setProductModels(List<ProductModel> productModels) {
        this.productModels = productModels;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

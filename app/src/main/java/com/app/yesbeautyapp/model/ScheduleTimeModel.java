package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ScheduleTimeModel implements Parcelable{

    private String id;
    @SerializedName("time_slot")
    private String timeSlot;

    protected ScheduleTimeModel(Parcel in) {
        id = in.readString();
        timeSlot = in.readString();
    }

    public static final Creator<ScheduleTimeModel> CREATOR = new Creator<ScheduleTimeModel>() {
        @Override
        public ScheduleTimeModel createFromParcel(Parcel in) {
            return new ScheduleTimeModel(in);
        }

        @Override
        public ScheduleTimeModel[] newArray(int size) {
            return new ScheduleTimeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(timeSlot);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }
}

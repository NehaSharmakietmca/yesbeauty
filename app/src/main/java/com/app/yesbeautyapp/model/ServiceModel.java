package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ServiceModel implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("parent_id")
    private String parentId;
    private String name;
    private String description;
    private String price;
    private String type;
    @SerializedName("creation_date")
    private String creationDate;
    private String image;

    public ServiceModel(Parcel in) {
        id = in.readString();
        parentId = in.readString();
        name = in.readString();
        description = in.readString();
        price = in.readString();
        type = in.readString();
        creationDate = in.readString();
        image = in.readString();
    }

    public ServiceModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public static Creator<ServiceModel> getCREATOR() {
        return CREATOR;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(parentId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(type);
        dest.writeString(creationDate);
        dest.writeString(image);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceModel> CREATOR = new Creator<ServiceModel>() {
        @Override
        public ServiceModel createFromParcel(Parcel in) {
            return new ServiceModel(in);
        }

        @Override
        public ServiceModel[] newArray(int size) {
            return new ServiceModel[size];
        }
    };


}

package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;


public class BannerImageModel implements Parcelable{
    private String image;

    protected BannerImageModel(Parcel in) {
        image = in.readString();
    }

    public static final Creator<BannerImageModel> CREATOR = new Creator<BannerImageModel>() {
        @Override
        public BannerImageModel createFromParcel(Parcel in) {
            return new BannerImageModel(in);
        }

        @Override
        public BannerImageModel[] newArray(int size) {
            return new BannerImageModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(image);
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

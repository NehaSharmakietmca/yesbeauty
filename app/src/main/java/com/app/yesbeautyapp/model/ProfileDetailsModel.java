package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ProfileDetailsModel implements Parcelable {

    private static ProfileDetailsModel profileDetailsModel;

    @SerializedName("user_id")
    private int id;
    private String name;
    @SerializedName("mobile")
    private String phoneNumber;
    @SerializedName("email")
    private String emailId;
    private String address;
    private String gender;
    private String dob;
    private String image;

    private ProfileDetailsModel() {

    }

    protected ProfileDetailsModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        phoneNumber = in.readString();
        emailId = in.readString();
        address = in.readString();
        gender = in.readString();
        dob = in.readString();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(phoneNumber);
        dest.writeString(emailId);
        dest.writeString(address);
        dest.writeString(gender);
        dest.writeString(dob);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileDetailsModel> CREATOR = new Creator<ProfileDetailsModel>() {
        @Override
        public ProfileDetailsModel createFromParcel(Parcel in) {
            return new ProfileDetailsModel(in);
        }

        @Override
        public ProfileDetailsModel[] newArray(int size) {
            return new ProfileDetailsModel[size];
        }
    };

    public static ProfileDetailsModel getInstance() {
        if (profileDetailsModel == null) {
            profileDetailsModel = new ProfileDetailsModel();
        }
        return profileDetailsModel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderModel implements Parcelable{

    @SerializedName("doe")
    private String orderDate;
    @SerializedName("id")
    private String orderId;
    private String productName;
    @SerializedName("schedule_date")
    private String bookingDate;
    @SerializedName("schedule_time")
    private String bookingTime;
    @SerializedName("payment_mode")
    private String paymentMode;
    @SerializedName("order_status")
    private String orderStatus;
    private String serviceType;
    @SerializedName("total_amount")
    private String orderAmount;
    private String cancelReason;
    private String name;
    private String address;
    private String discount;


    public OrderModel() {

    }

    protected OrderModel(Parcel in) {
        orderDate = in.readString();
        orderId = in.readString();
        productName = in.readString();
        bookingDate = in.readString();
        bookingTime = in.readString();
        paymentMode = in.readString();
        orderStatus = in.readString();
        serviceType = in.readString();
        orderAmount = in.readString();
        cancelReason = in.readString();
        name = in.readString();
        address = in.readString();
        discount = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderDate);
        dest.writeString(orderId);
        dest.writeString(productName);
        dest.writeString(bookingDate);
        dest.writeString(bookingTime);
        dest.writeString(paymentMode);
        dest.writeString(orderStatus);
        dest.writeString(serviceType);
        dest.writeString(orderAmount);
        dest.writeString(cancelReason);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(discount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel in) {
            return new OrderModel(in);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}

package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by GARIMA on 9/15/2017.
 */

public class CartProductModel implements Parcelable {

    private static CartProductModel cartProductModel;

    private CartProductModel() {

    }

    protected CartProductModel(Parcel in) {
        productModelArrayList = in.createTypedArrayList(ProductModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(productModelArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CartProductModel> CREATOR = new Creator<CartProductModel>() {
        @Override
        public CartProductModel createFromParcel(Parcel in) {
            return new CartProductModel(in);
        }

        @Override
        public CartProductModel[] newArray(int size) {
            return new CartProductModel[size];
        }
    };

    public static CartProductModel getInstance() {
        if (cartProductModel == null) {
            cartProductModel = new CartProductModel();
        }
        return cartProductModel;
    }

    private ArrayList<ProductModel> productModelArrayList;

    public ArrayList<ProductModel> getProductModelArrayList() {
        return productModelArrayList;
    }

    public void setProductModelArrayList(ArrayList<ProductModel> productModelArrayList) {
        this.productModelArrayList = productModelArrayList;
    }
}

package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class ProductModel implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("cat_name")
    private String cat;
    private String scat;
    @SerializedName("image")
    private String productImage;
    @SerializedName("price")
    private String amount;
    @SerializedName("service")
    private String productName;
    private String discount;
    private String duration;
    @SerializedName("short_desc")
    private String shortDesc;
    private String description;
    @SerializedName("product_cost")
    private String productCost;
    @SerializedName("service_cost")
    private String serviceCost;
    @SerializedName("product_qty")
    private int productQty;
    @SerializedName("service_qty")
    private int serviceQty;

    public ProductModel() {

    }


    protected ProductModel(Parcel in) {
        id = in.readString();
        cat = in.readString();
        scat = in.readString();
        productImage = in.readString();
        amount = in.readString();
        productName = in.readString();
        discount = in.readString();
        duration = in.readString();
        shortDesc = in.readString();
        description = in.readString();
        productCost = in.readString();
        serviceCost = in.readString();
        productQty = in.readInt();
        serviceQty = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(cat);
        dest.writeString(scat);
        dest.writeString(productImage);
        dest.writeString(amount);
        dest.writeString(productName);
        dest.writeString(discount);
        dest.writeString(duration);
        dest.writeString(shortDesc);
        dest.writeString(description);
        dest.writeString(productCost);
        dest.writeString(serviceCost);
        dest.writeInt(productQty);
        dest.writeInt(serviceQty);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getScat() {
        return scat;
    }

    public void setScat(String scat) {
        this.scat = scat;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductCost() {
        return productCost;
    }

    public void setProductCost(String productCost) {
        this.productCost = productCost;
    }

    public String getServiceCost() {
        return serviceCost;
    }

    public void setServiceCost(String serviceCost) {
        this.serviceCost = serviceCost;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }

    public int getServiceQty() {
        return serviceQty;
    }

    public void setServiceQty(int serviceQty) {
        this.serviceQty = serviceQty;
    }
}

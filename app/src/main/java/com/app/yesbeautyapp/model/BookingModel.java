package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingModel implements Parcelable {
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("discount_amount")
    private String discountAmount;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("service_list")
    private List<CartModel> serviceList;
    @SerializedName("booking_id")
    private String bookingId;
    @SerializedName("timestamp")
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }



    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }




    public BookingModel(Parcel in) {
        totalAmount = in.readString();
        discountAmount = in.readString();
        userId = in.readInt();
        paymentType = in.readString();
        serviceList = in.createTypedArrayList(CartModel.CREATOR);
        bookingId = in.readString();
        timestamp = in.readString();
    }

    public BookingModel() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(totalAmount);
        dest.writeString(discountAmount);
        dest.writeInt(userId);
        dest.writeString(paymentType);
        dest.writeTypedList(serviceList);
        dest.writeString(bookingId);
        dest.writeString(timestamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingModel> CREATOR = new Creator<BookingModel>() {
        @Override
        public BookingModel createFromParcel(Parcel in) {
            return new BookingModel(in);
        }

        @Override
        public BookingModel[] newArray(int size) {
            return new BookingModel[size];
        }
    };

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public List<CartModel> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<CartModel> serviceList) {
        this.serviceList = serviceList;
    }




}

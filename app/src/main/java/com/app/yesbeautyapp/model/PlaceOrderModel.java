package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PlaceOrderModel implements Parcelable {

    @SerializedName("authid")
    private String userId;
    @SerializedName("total_amount")
    private String totalAmount;
    private String discount;
    private String discode;
    @SerializedName("schedule_date")
    private String scheduleDate;
    @SerializedName("schedule_time")
    private String scheduleTime;
    @SerializedName("payment_mode")
    private String paymentMode;
    @SerializedName("payment_status")
    private String paymentStatus;
    @SerializedName("transaction_id")
    private String tnxId;
    @SerializedName("sub_total")
    private String subTotalAmt;
    @SerializedName("gst_amount")
    private String gstAmt;
    @SerializedName("products")
    ArrayList<ProductModel> productModelArrayList;

    public PlaceOrderModel() {

    }

    protected PlaceOrderModel(Parcel in) {
        userId = in.readString();
        totalAmount = in.readString();
        discount = in.readString();
        discode = in.readString();
        scheduleDate = in.readString();
        scheduleTime = in.readString();
        paymentMode = in.readString();
        paymentStatus = in.readString();
        tnxId = in.readString();
        subTotalAmt = in.readString();
        gstAmt = in.readString();
        productModelArrayList = in.createTypedArrayList(ProductModel.CREATOR);
    }

    public static final Creator<PlaceOrderModel> CREATOR = new Creator<PlaceOrderModel>() {
        @Override
        public PlaceOrderModel createFromParcel(Parcel in) {
            return new PlaceOrderModel(in);
        }

        @Override
        public PlaceOrderModel[] newArray(int size) {
            return new PlaceOrderModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userId);
        parcel.writeString(totalAmount);
        parcel.writeString(discount);
        parcel.writeString(discode);
        parcel.writeString(scheduleDate);
        parcel.writeString(scheduleTime);
        parcel.writeString(paymentMode);
        parcel.writeString(paymentStatus);
        parcel.writeString(tnxId);
        parcel.writeString(subTotalAmt);
        parcel.writeString(gstAmt);
        parcel.writeTypedList(productModelArrayList);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscode() {
        return discode;
    }

    public void setDiscode(String discode) {
        this.discode = discode;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTnxId() {
        return tnxId;
    }

    public void setTnxId(String tnxId) {
        this.tnxId = tnxId;
    }

    public String getSubTotalAmt() {
        return subTotalAmt;
    }

    public void setSubTotalAmt(String subTotalAmt) {
        this.subTotalAmt = subTotalAmt;
    }

    public String getGstAmt() {
        return gstAmt;
    }

    public void setGstAmt(String gstAmt) {
        this.gstAmt = gstAmt;
    }

    public ArrayList<ProductModel> getProductModelArrayList() {
        return productModelArrayList;
    }

    public void setProductModelArrayList(ArrayList<ProductModel> productModelArrayList) {
        this.productModelArrayList = productModelArrayList;
    }
}

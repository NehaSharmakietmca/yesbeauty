package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ravi on 2/13/2018.
 */

public class CartModel implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("service_id")
    private String serviceId;
    @SerializedName("service_name")
    private String serviceName;
    @SerializedName("service_image")
    private String serviceImage;
    @SerializedName("service_type")
    private String serviceTypeLabel;
    @SerializedName("service_date")
    private String serviceDate;
    @SerializedName("service_time")
    private String serviceTime;
    @SerializedName("phone_number")
    private String phoneNumber;
    @SerializedName("email_id")
    private String emailId;
    @SerializedName("address")
    private String address;
    @SerializedName("comments")
    private String comments;
    @SerializedName("amount")
    private String totalAmount;
    @SerializedName("quantity")
    private String quantity ;
    private String serviceType;
    @SerializedName("status")
    private int status;
    @SerializedName("modification_date")
    private String modificationDate;
    @SerializedName("timestamp")
    private String timestamp;

    public String getActionNext() {
        return actionNext;
    }

    public void setActionNext(String actionNext) {
        this.actionNext = actionNext;
    }

    @SerializedName("actionNext")
    private String actionNext;
    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }



    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    private String bookingId;

    public CartModel() {

    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public static Creator<CartModel> getCREATOR() {
        return CREATOR;
    }
    protected CartModel(Parcel in) {
        id = in.readInt();
        serviceId = in.readString();
        serviceName = in.readString();
        serviceImage = in.readString();
        serviceTypeLabel = in.readString();
        serviceDate = in.readString();
        serviceTime = in.readString();
        phoneNumber = in.readString();
        emailId = in.readString();
        address = in.readString();
        comments = in.readString();
        totalAmount = in.readString();
        quantity = in.readString();
        bookingId = in.readString();
        serviceType = in.readString();
        status = in.readInt();
        modificationDate = in.readString();
        timestamp = in.readString();
        actionNext = in.readString();
    }

    public static final Creator<CartModel> CREATOR = new Creator<CartModel>() {
        @Override
        public CartModel createFromParcel(Parcel in) {
            return new CartModel(in);
        }

        @Override
        public CartModel[] newArray(int size) {
            return new CartModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(serviceId);
        parcel.writeString(serviceName);
        parcel.writeString(serviceImage);
        parcel.writeString(serviceTypeLabel);
        parcel.writeString(serviceDate);
        parcel.writeString(serviceTime);
        parcel.writeString(phoneNumber);
        parcel.writeString(emailId);
        parcel.writeString(address);
        parcel.writeString(comments);
        parcel.writeString(totalAmount);
        parcel.writeString(quantity);
        parcel.writeString(bookingId);
        parcel.writeString(serviceType);
        parcel.writeInt(status);
        parcel.writeString(modificationDate);
        parcel.writeString(timestamp);
        parcel.writeString(actionNext);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }

    public String getServiceTypeLabel() {
        return serviceTypeLabel;
    }

    public void setServiceTypeLabel(String serviceTypeLabel) {
        this.serviceTypeLabel = serviceTypeLabel;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}

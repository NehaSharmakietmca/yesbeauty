package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PromocodeModel implements Parcelable {
    private int  id;
    private String promocode;
    private String amount;
    private String description;
    private String is_expired;
    private boolean creation_date;
    private String timestamp;

    public PromocodeModel(Parcel in) {
        id = in.readInt();
        promocode = in.readString();
        amount = in.readString();
        description = in.readString();
        is_expired = in.readString();
        creation_date = in.readByte() != 0;
        timestamp = in.readString();
    }

    public static final Creator<PromocodeModel> CREATOR = new Creator<PromocodeModel>() {
        @Override
        public PromocodeModel createFromParcel(Parcel in) {
            return new PromocodeModel(in);
        }

        @Override
        public PromocodeModel[] newArray(int size) {
            return new PromocodeModel[size];
        }
    };

    public PromocodeModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_expired() {
        return is_expired;
    }

    public void setIs_expired(String is_expired) {
        this.is_expired = is_expired;
    }

    public boolean isCreation_date() {
        return creation_date;
    }

    public void setCreation_date(boolean creation_date) {
        this.creation_date = creation_date;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(promocode);
        dest.writeString(amount);
        dest.writeString(description);
        dest.writeString(is_expired);
        dest.writeByte((byte) (creation_date ? 1 : 0));
        dest.writeString(timestamp);
    }
}

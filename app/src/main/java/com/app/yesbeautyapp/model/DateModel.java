package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DateModel implements Parcelable {
    private String day;
    private String date;
    private String month;
    private String intMonth;
    private String year;
    private boolean isSelected;

    public DateModel() {

    }


    protected DateModel(Parcel in) {
        day = in.readString();
        date = in.readString();
        month = in.readString();
        intMonth = in.readString();
        year = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(day);
        dest.writeString(date);
        dest.writeString(month);
        dest.writeString(intMonth);
        dest.writeString(year);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DateModel> CREATOR = new Creator<DateModel>() {
        @Override
        public DateModel createFromParcel(Parcel in) {
            return new DateModel(in);
        }

        @Override
        public DateModel[] newArray(int size) {
            return new DateModel[size];
        }
    };

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getDay() {
        return day;
    }

    public String getIntMonth() {
        return intMonth;
    }

    public void setIntMonth(String intMonth) {
        this.intMonth = intMonth;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}

package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScheduleDateTimeModel implements Parcelable{

    @SerializedName("schedule_date")
    private String scheduleDate;
    @SerializedName("schedule_time")
    private List<ScheduleTimeModel> scheduleTimeModelList;

    protected ScheduleDateTimeModel(Parcel in) {
        scheduleDate = in.readString();
        scheduleTimeModelList = in.createTypedArrayList(ScheduleTimeModel.CREATOR);
    }

    public static final Creator<ScheduleDateTimeModel> CREATOR = new Creator<ScheduleDateTimeModel>() {
        @Override
        public ScheduleDateTimeModel createFromParcel(Parcel in) {
            return new ScheduleDateTimeModel(in);
        }

        @Override
        public ScheduleDateTimeModel[] newArray(int size) {
            return new ScheduleDateTimeModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(scheduleDate);
        parcel.writeTypedList(scheduleTimeModelList);
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public List<ScheduleTimeModel> getScheduleTimeModelList() {
        return scheduleTimeModelList;
    }

    public void setScheduleTimeModelList(List<ScheduleTimeModel> scheduleTimeModelList) {
        this.scheduleTimeModelList = scheduleTimeModelList;
    }
}

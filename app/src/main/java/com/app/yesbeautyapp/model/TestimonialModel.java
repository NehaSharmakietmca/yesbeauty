package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TestimonialModel implements Parcelable {
    @SerializedName("name")
    private String customerName;
    @SerializedName("testimonial")
    private String customerReview;
    @SerializedName("profile_image")
    private String customerImage;
    @SerializedName("rating")
    private String customerRating;

    public TestimonialModel(Parcel in) {
        customerName = in.readString();
        customerReview = in.readString();
        customerImage = in.readString();
        customerRating = in.readString();
    }

    public TestimonialModel() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerName);
        dest.writeString(customerReview);
        dest.writeString(customerImage);
        dest.writeString(customerRating);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TestimonialModel> CREATOR = new Creator<TestimonialModel>() {
        @Override
        public TestimonialModel createFromParcel(Parcel in) {
            return new TestimonialModel(in);
        }

        @Override
        public TestimonialModel[] newArray(int size) {
            return new TestimonialModel[size];
        }
    };

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerReview() {
        return customerReview;
    }

    public void setCustomerReview(String customerReview) {
        this.customerReview = customerReview;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }

    public String getCustomerRating() {
        return customerRating;
    }

    public void setCustomerRating(String customerRating) {
        this.customerRating = customerRating;
    }


}

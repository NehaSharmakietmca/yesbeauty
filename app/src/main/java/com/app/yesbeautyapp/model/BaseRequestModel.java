package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Codeslay-03 on 11/16/2017.
 */

public class BaseRequestModel implements Parcelable{

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("ip_address")
    private String ipAddress;
    @SerializedName("login_id")
    private String loginId;
    @SerializedName("device_type")
    private String deviceType;

    public BaseRequestModel(){

    }

    protected BaseRequestModel(Parcel in) {
        accessToken = in.readString();
        ipAddress = in.readString();
        loginId = in.readString();
        deviceType = in.readString();
    }

    public static final Creator<BaseRequestModel> CREATOR = new Creator<BaseRequestModel>() {
        @Override
        public BaseRequestModel createFromParcel(Parcel in) {
            return new BaseRequestModel(in);
        }

        @Override
        public BaseRequestModel[] newArray(int size) {
            return new BaseRequestModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accessToken);
        dest.writeString(ipAddress);
        dest.writeString(loginId);
        dest.writeString(deviceType);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}

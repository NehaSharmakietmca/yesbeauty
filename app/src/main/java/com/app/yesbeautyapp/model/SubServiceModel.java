package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubServiceModel implements Parcelable {
    @SerializedName("id")
    private String id;
    private String name;
    private String image;
    @SerializedName("product")
    private List<ProductModel> productModels;
    @SerializedName("subcategory")
    private List<SubSubServiceModel> subSubServiceModels;


    protected SubServiceModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        image = in.readString();
        productModels = in.createTypedArrayList(ProductModel.CREATOR);
        subSubServiceModels = in.createTypedArrayList(SubSubServiceModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeTypedList(productModels);
        dest.writeTypedList(subSubServiceModels);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubServiceModel> CREATOR = new Creator<SubServiceModel>() {
        @Override
        public SubServiceModel createFromParcel(Parcel in) {
            return new SubServiceModel(in);
        }

        @Override
        public SubServiceModel[] newArray(int size) {
            return new SubServiceModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductModel> getProductModels() {
        return productModels;
    }

    public void setProductModels(List<ProductModel> productModels) {
        this.productModels = productModels;
    }

    public List<SubSubServiceModel> getSubSubServiceModels() {
        return subSubServiceModels;
    }

    public void setSubSubServiceModels(List<SubSubServiceModel> subSubServiceModels) {
        this.subSubServiceModels = subSubServiceModels;
    }
}

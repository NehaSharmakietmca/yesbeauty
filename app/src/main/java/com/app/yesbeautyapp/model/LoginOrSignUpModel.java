package com.app.yesbeautyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by GARIMA on 9/15/2017.
 */

public class LoginOrSignUpModel extends BaseRequestModel implements Parcelable {

    private static LoginOrSignUpModel loginOrSignUpModel;
    private String email;
    private String phone;
    private String fname;
    private String lname;
    @SerializedName("refcode")
    private String refCode;
    private String id;
    private String image;
    @SerializedName("device_id")
    private String deviceId;
    @SerializedName("fcm_token")
    private String fcmToken;

    private LoginOrSignUpModel() {
        super();
    }

    public static LoginOrSignUpModel getInstance() {
        if (loginOrSignUpModel == null) {
            loginOrSignUpModel = new LoginOrSignUpModel();
        }
        return loginOrSignUpModel;
    }

    protected LoginOrSignUpModel(Parcel in) {
        email = in.readString();
        phone = in.readString();
        fname = in.readString();
        lname = in.readString();
        refCode = in.readString();
        id = in.readString();
        image = in.readString();
        deviceId = in.readString();
        fcmToken = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(fname);
        dest.writeString(lname);
        dest.writeString(refCode);
        dest.writeString(id);
        dest.writeString(image);
        dest.writeString(deviceId);
        dest.writeString(fcmToken);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginOrSignUpModel> CREATOR = new Creator<LoginOrSignUpModel>() {
        @Override
        public LoginOrSignUpModel createFromParcel(Parcel in) {
            return new LoginOrSignUpModel(in);
        }

        @Override
        public LoginOrSignUpModel[] newArray(int size) {
            return new LoginOrSignUpModel[size];
        }
    };

    public static LoginOrSignUpModel getLoginOrSignUpModel() {
        return loginOrSignUpModel;
    }

    public static void setLoginOrSignUpModel(LoginOrSignUpModel loginOrSignUpModel) {
        LoginOrSignUpModel.loginOrSignUpModel = loginOrSignUpModel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

}

package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ScheduleTimeModel;
import com.app.yesbeautyapp.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;


public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.ViewHolder> implements AppConstants {
    Activity currentActivity;
    List<ScheduleTimeModel> timeModelList;
    private RecyclerClickListner recyclerClickListner;
    private List<ScheduleTimeModel> tempScheduleTimeModel = new ArrayList<>();
    private int selectedItems = 0;

    Bundle bundle;

    public TimeAdapter(Activity currentActivity, List<ScheduleTimeModel> timeModelList) {
        this.currentActivity = currentActivity;
        this.timeModelList = timeModelList;
        tempScheduleTimeModel.addAll(timeModelList);
    }

    @Override
    public TimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.item_time_slot, parent, false);
        int height = parent.getMeasuredHeight() / 2;
        view.setMinimumHeight(height);
        TimeAdapter.ViewHolder holder = new TimeAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(TimeAdapter.ViewHolder holder, int position) {


        if (timeModelList != null && timeModelList.size() > position) {
            final ScheduleTimeModel scheduleTimeModel = timeModelList.get(position);
            if (selectedItems == position) {
                holder.timeSlotLinearLayout.setBackgroundResource(R.drawable.red_fill_rectangle);
                holder.tvTimeSlot.setTextColor(currentActivity.getResources().getColor(R.color.white));
            } else {
                holder.timeSlotLinearLayout.setBackgroundResource(R.drawable.white_rectangle);
                holder.tvTimeSlot.setTextColor(currentActivity.getResources().getColor(R.color.black_9));
            }
            if (scheduleTimeModel == null) return;
            holder.tvTimeSlot.setText(scheduleTimeModel.getTimeSlot().trim());
            FontUtils.changeFont(currentActivity, holder.tvTimeSlot, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);

        }

    }

    @Override
    public int getItemCount() {
        if (timeModelList == null) return 0;
        return timeModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTimeSlot;
        LinearLayout timeSlotLinearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTimeSlot = (TextView) itemView.findViewById(R.id.tv_time_slot);
            timeSlotLinearLayout = (LinearLayout) itemView.findViewById(R.id.lin_time_slot);
            timeSlotLinearLayout.setOnClickListener(this);
            FontUtils.changeFont(currentActivity, tvTimeSlot, FONT_OPEN_SANS_REGULAR_TTF);
        }

        @Override
        public void onClick(View v) {
            selectedItems = getAdapterPosition();
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
            timeSlotLinearLayout.setBackgroundResource(R.drawable.red_fill_rectangle);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;
    }

    public void updateAdapter(List<ScheduleTimeModel> arrayList) {
        timeModelList = arrayList;
        tempScheduleTimeModel.addAll(timeModelList);
        notifyDataSetChanged();
    }

    public void selectedItem() {
        notifyDataSetChanged();
    }
}

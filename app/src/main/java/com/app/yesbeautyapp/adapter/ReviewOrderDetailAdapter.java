package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.model.CartModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class ReviewOrderDetailAdapter extends RecyclerView.Adapter<ReviewOrderDetailAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<CartModel> cartModelList;

    Bundle bundle;

    public ReviewOrderDetailAdapter(Activity currentActivity, List<CartModel> cartModelList) {
        this.currentActivity = currentActivity;
        this.cartModelList = cartModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.item_review_order_details, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (cartModelList != null && cartModelList.size() > position) {
            final CartModel cartModel = cartModelList.get(position);
            if (cartModel == null) return;

            holder.serviceTypeTV.setText(cartModel.getServiceName());
            holder.quantityTV.setText(String.valueOf(cartModel.getQuantity()));
          String amount =   String.format("%.1f",Double.parseDouble(cartModel.getTotalAmount()));
            String totalAmt = currentActivity.getString(R.string.rupees_symbol) + " " + amount + "/-";
            holder.amountTV.setText(totalAmt);
            holder.dateTv.setText(cartModel.getServiceDate());
            holder.timeTV.setText(cartModel.getServiceTime());
            holder.mobileNumberTv.setText(cartModel.getPhoneNumber());
            holder.emailIdTv.setText(cartModel.getEmailId());
            holder.additionalCommentTv.setText(cartModel.getComments());
            holder.addressTV.setText(cartModel.getAddress());
            //  String imageUrl = AppConstants.BASE_URL_IMAGES + cartModel.getServiceImage();
            //  Picasso.with(currentActivity).load(imageUrl).into(holder.imageProduct);
        }

    }

    @Override
    public int getItemCount() {
        if (cartModelList == null) return 0;
        return cartModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView editIconImageVIew;
        ImageView cancelImageView;
        TextView serviceTypeLbl;
        TextView serviceTypeTV;
        TextView quantityLBL;
        TextView quantityTV;
        TextView dateLBl;
        TextView dateTv;
        TextView timeLbl;
        TextView timeTV;
        TextView mobileNumberLbl;
        TextView mobileNumberTv;
        TextView emailIdLbl;
        TextView emailIdTv;
        TextView addressLBl;
        TextView addressTV;
        TextView additionalCommentTv;
        TextView additionalCommentLBL;
        TextView amountLBL;
        TextView amountTV;
        LinearLayout containerProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            editIconImageVIew = (ImageView) itemView.findViewById(R.id.editCartImageview);
            //cancelImageView = (ImageView) itemView.findViewById(R.id.cancelCartImageVIew);
            serviceTypeLbl = (TextView) itemView.findViewById(R.id.serviceTypeLbl);
            serviceTypeTV = (TextView) itemView.findViewById(R.id.serviceTypeTV);
            quantityLBL = (TextView) itemView.findViewById(R.id.quantityLbl);
            quantityTV = (TextView) itemView.findViewById(R.id.quantityTV);
            dateLBl = (TextView) itemView.findViewById(R.id.dateLBl);
            timeLbl = (TextView) itemView.findViewById(R.id.lblTime);
            timeTV = (TextView) itemView.findViewById(R.id.timeTV);
            mobileNumberLbl = (TextView) itemView.findViewById(R.id.mobileLbl);
            mobileNumberTv = (TextView) itemView.findViewById(R.id.mobileNoTV);
            emailIdLbl = (TextView) itemView.findViewById(R.id.emailIdLbl);
            emailIdTv = (TextView) itemView.findViewById(R.id.emailIdTextView);
            addressLBl = (TextView) itemView.findViewById(R.id.addressLbl);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            additionalCommentLBL = (TextView) itemView.findViewById(R.id.addtionalCommentlbl);
            additionalCommentTv = (TextView) itemView.findViewById(R.id.addCommentTV);
            amountLBL = (TextView) itemView.findViewById(R.id.amountLbl);
            amountTV = (TextView) itemView.findViewById(R.id.amountTextView);
            dateTv = (TextView) itemView.findViewById(R.id.dateTV);
            FontUtils.changeFont(currentActivity, serviceTypeLbl, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, serviceTypeTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, quantityLBL, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, quantityTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, dateLBl, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, dateTv, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, timeLbl, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, timeTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, mobileNumberLbl, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, mobileNumberTv, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, emailIdLbl, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, emailIdTv, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, addressLBl, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, addressTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, additionalCommentLBL, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, additionalCommentTv, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, amountLBL, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, amountTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
        }
    }
}

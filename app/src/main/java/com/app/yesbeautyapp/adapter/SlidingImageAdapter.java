package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.model.BannerImageModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class SlidingImageAdapter extends PagerAdapter {
    private LayoutInflater inflater;
    Activity currentActivity;
    Bundle bundle;
    List<BannerImageModel> bannerImageModelList;
    //int imgArr[] = {R.drawable.imghome4, R.drawable.imghome1, R.drawable.imghome2, R.drawable.imghome3, R.drawable.imghome5};

    public SlidingImageAdapter(Activity currentActivity, List<BannerImageModel> bannerImageModelList) {
        this.currentActivity = currentActivity;
        this.bannerImageModelList = bannerImageModelList;
        inflater = LayoutInflater.from(currentActivity);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (bannerImageModelList == null) return 0;
        return bannerImageModelList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);

        view.addView(imageLayout, 0);
        if (bannerImageModelList != null && bannerImageModelList.size() > position) {
            BannerImageModel bannerImageModel = bannerImageModelList.get(position);
            if (bannerImageModel.getImage() == null) {
                bannerImageModel.setImage("");
            }
            String imageUrl = AppConstants.BASE_URL_IMAGES + bannerImageModel.getImage().trim();
            Picasso.with(currentActivity).load(imageUrl).error(R.drawable.imghome4).placeholder(R.drawable.imghome4).into(imageView);
        }

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}

package com.app.yesbeautyapp.adapter;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.yesbeautyapp.model.TestimonialModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ServiceModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class TestimonialAdapter extends RecyclerView.Adapter<TestimonialAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<TestimonialModel> testimonialModelList;
    private RecyclerClickListner recyclerClickListner;
    private List<TestimonialModel> tempTestimonialList = new ArrayList<>();

    Bundle bundle;

    public TestimonialAdapter(Activity currentActivity, List<TestimonialModel> testimonialModelList) {
        this.currentActivity = currentActivity;
        this.testimonialModelList = testimonialModelList;
        tempTestimonialList.addAll(testimonialModelList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.item_customer_testimonials, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (testimonialModelList != null && testimonialModelList.size() > position) {
            final TestimonialModel testimonialModel = testimonialModelList.get(position);
            if (testimonialModel == null) return;
            holder.customerName.setText(testimonialModel.getCustomerName());
            holder.customerRatingBar.setRating(Integer.parseInt(testimonialModel.getCustomerRating()));
            holder.customerReview.setText(testimonialModel.getCustomerReview());
            FontUtils.changeFont(currentActivity,holder.customerName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity,holder.customerReview, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            String imageUrl = AppConstants.BASE_URL_IMAGES + testimonialModel.getCustomerImage();
            Picasso.with(currentActivity).load(imageUrl).error(R.drawable.imghome2).placeholder(R.drawable.imghome2).into(holder.customerImage);

        }

    }

    @Override
    public int getItemCount() {
        if (testimonialModelList == null) return 0;
        return testimonialModelList.size();
    }

    public void filter(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());
        testimonialModelList.clear();
        if (newText.length() == 0) {
            testimonialModelList.addAll(tempTestimonialList);
        } else {
            for (TestimonialModel wp : tempTestimonialList) {
                if (wp.getCustomerName().toLowerCase(Locale.getDefault()).contains(newText)) {
                    testimonialModelList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView customerImage;
        TextView customerName;
        RatingBar customerRatingBar;
        TextView customerReview;

        public ViewHolder(View itemView) {
            super(itemView);

            customerImage = (ImageView) itemView.findViewById(R.id.tv_customer_profile_image);
            customerName = (TextView) itemView.findViewById(R.id.text_customer_name);
            customerRatingBar = (RatingBar) itemView.findViewById(R.id.rating_customer);
            customerReview = (TextView) itemView.findViewById(R.id.tv_customer_review);
        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;

    }

    public void updateAdapter(List<TestimonialModel> arrayList) {
        testimonialModelList = arrayList;
        tempTestimonialList.clear();
        tempTestimonialList.addAll(testimonialModelList);
        notifyDataSetChanged();
    }



    }


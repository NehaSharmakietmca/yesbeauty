package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.model.OrderModel;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.DateTimeUtils;
import com.app.yesbeautyapp.utils.FontUtils;

import java.util.Iterator;
import java.util.List;

public class AppointmentProductListAdapter extends RecyclerView.Adapter<AppointmentProductListAdapter.ViewHolder> implements AppConstants {
    Activity currentActivity;
    List<ProductModel> productModelList;
    private RecyclerClickListner recyclerClickListner;

    Bundle bundle;

    public AppointmentProductListAdapter(Activity currentActivity, List<ProductModel> productModelList) {
        this.currentActivity = currentActivity;
        this.productModelList = productModelList;
    }

    @Override
    public AppointmentProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.row_item_product_list, parent, false);
        AppointmentProductListAdapter.ViewHolder holder = new AppointmentProductListAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(AppointmentProductListAdapter.ViewHolder holder, int position) {

        if (productModelList != null && productModelList.size() > position) {
            final ProductModel productModel = productModelList.get(position);
            if (productModel == null) return;
            holder.productName.setText(productModel.getProductName());
            holder.productDescription.setText(productModel.getDescription());
            holder.serviceMinutesValue.setText(productModel.getDuration() + " Minutes");
            String productCost = productModel.getProductCost();
            String serviceCost = productModel.getServiceCost();
            if (TextUtils.isEmpty(productCost)) productCost = "0";
            if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
            holder.productCostValue.setText(currentActivity.getString(R.string.rupees_symbol) + " " + String.format("%.1f", (productModel.getProductQty() * Double.parseDouble(productCost))) + "/-");
            holder.serviceCostValue.setText(currentActivity.getString(R.string.rupees_symbol) + " " + String.format("%.1f", (productModel.getServiceQty() * Double.parseDouble(serviceCost))) + "/-");

            holder.categoryValue.setText(productModel.getCat());
            holder.subcategoryValue.setText(productModel.getScat());
            holder.headingValue.setText(productModel.getShortDesc());

            float totalAmt = 0;
            totalAmt = (productModel.getProductQty() * Float.parseFloat(productCost)) + (productModel.getServiceQty() * Float.parseFloat(serviceCost));
            holder.totalValue.setText(currentActivity.getString(R.string.rupees_symbol) + " " + String.format("%.1f", totalAmt) + "/-");
        }

    }

    @Override
    public int getItemCount() {
        if (productModelList == null) return 0;
        return productModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView productName;
        TextView productDescription;
        TextView serviceMinutesLabel;
        TextView serviceMinutesValue;
        TextView serviceCostLabel;
        TextView serviceCostValue;
        TextView productCostLabel;
        TextView productCostValue;
        TextView categoryLabel;
        TextView categoryValue;
        TextView subcategoryLabel;
        TextView subcategoryValue;
        TextView headingLabel;
        TextView headingValue;
        TextView totalLabel;
        TextView totalValue;

        public ViewHolder(View itemView) {
            super(itemView);

            productName = itemView.findViewById(R.id.text_product_name);
            productDescription = itemView.findViewById(R.id.text_product_description);
            serviceMinutesLabel = itemView.findViewById(R.id.text_product_service_minutes_Label);
            serviceMinutesValue = itemView.findViewById(R.id.text_product_service_minutes_value);
            serviceCostLabel = itemView.findViewById(R.id.text_product_service_service_cost_Label);
            serviceCostValue = itemView.findViewById(R.id.text_product_service_service_cost_value);
            productCostLabel = itemView.findViewById(R.id.text_product_cost_Label);
            productCostValue = itemView.findViewById(R.id.text_product_cost_value);
            categoryLabel = itemView.findViewById(R.id.text_product_category_Label);
            categoryValue = itemView.findViewById(R.id.text_product_category_value);
            subcategoryLabel = itemView.findViewById(R.id.text_product_subcategory_Label);
            subcategoryValue = itemView.findViewById(R.id.text_product_subcategory_Value);
            headingLabel = itemView.findViewById(R.id.text_product_headings_Label);
            headingValue = itemView.findViewById(R.id.text_product_headings_value);
            totalLabel = itemView.findViewById(R.id.text_product_total_Label);
            totalValue = itemView.findViewById(R.id.text_product_total_Value);
            FontUtils.changeFont(currentActivity, productName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, productDescription, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, serviceMinutesLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, serviceMinutesValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, serviceCostLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, serviceCostValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, productCostLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, productCostValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, categoryLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, categoryValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, subcategoryLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, subcategoryValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, headingLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, headingValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, totalLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, totalValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            //   recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;

    }

    private double totalProductAmount() {
        double totalAmount = 0.0;
        Iterator<ProductModel> productModelIterator = productModelList.iterator();
        while (productModelIterator.hasNext()) {
            ProductModel productModel = productModelIterator.next();
            if (productModel != null && productModel.getAmount() != null) {
                double totalAmt = 0;
                String productCost = productModel.getProductCost();
                String serviceCost = productModel.getServiceCost();
                if (TextUtils.isEmpty(productCost)) productCost = "0";
                if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";
                totalAmt = (productModel.getProductQty() * Double.parseDouble(productCost))
                        + (productModel.getServiceQty() * Double.parseDouble(serviceCost));
                totalAmount += totalAmt;
            }
        }
        return totalAmount;
    }
}

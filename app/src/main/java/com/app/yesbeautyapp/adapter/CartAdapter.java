package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.CartDetailsActivity;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<ProductModel> cartModelList;
    private RecyclerClickListner recyclerClickListner;

    public CartAdapter(Activity currentActivity, List<ProductModel> cartModelList) {
        this.currentActivity = currentActivity;
        this.cartModelList = cartModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.items_cart, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (cartModelList != null && cartModelList.size() > position) {
            final ProductModel cartModel = cartModelList.get(position);
            if (cartModel == null) return;
            holder.productTitleTV.setText(cartModel.getProductName());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.productDescTV.setText(Html.fromHtml(cartModel.getDescription().trim(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.productDescTV.setText(Html.fromHtml(cartModel.getDescription().trim()));
            }

            holder.durationTV.setText("(" + cartModel.getDuration().trim() + " mins)");
            holder.serviceQtyTV.setText(cartModel.getServiceQty() + "");
            holder.productQtyTV.setText(cartModel.getProductQty() + "");

            float totalAmt = 0;
            String productCost = cartModel.getProductCost();
            String serviceCost = cartModel.getServiceCost();
            if (TextUtils.isEmpty(productCost)) productCost = "0";
            if (TextUtils.isEmpty(serviceCost)) serviceCost = "0";

            totalAmt = (cartModel.getProductQty() * Float.parseFloat(productCost)) + (cartModel.getServiceQty() * Float.parseFloat(serviceCost));

            holder.totalAmtTV.setText("Rs. " + (int) totalAmt + "/-");
            holder.serviceCostTV.setText("Rs. " + (cartModel.getServiceQty() * (int) Float.parseFloat(serviceCost)) + "/-");
            holder.productCostTV.setText("Rs. " + (cartModel.getProductQty() * (int) Float.parseFloat(productCost)) + "/-");


            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((CartDetailsActivity) currentActivity).removeItem(cartModel);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (cartModelList == null) return 0;
        return cartModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView productTitleTV;
        TextView totalAmtTV;
        TextView productDescTV;
        TextView durationTV;
        TextView lblServiceCostTV;
        TextView serviceCostTV;
        TextView lblProductCostTV;
        TextView productCostTV;
        ImageView addServiceIV;
        ImageView minusServiceIV;
        ImageView addProductIV;
        ImageView minusProductIV;
        TextView productQtyTV;
        TextView serviceQtyTV;
        Button btnRemove;

        public ViewHolder(View itemView) {
            super(itemView);

            productTitleTV = itemView.findViewById(R.id.productTitleTV);
            totalAmtTV = itemView.findViewById(R.id.totalAmtTV);
            productDescTV = itemView.findViewById(R.id.productDescTV);
            btnRemove = itemView.findViewById(R.id.btnRemove);
            durationTV = itemView.findViewById(R.id.durationTV);
            lblServiceCostTV = itemView.findViewById(R.id.lblServiceCostTV);
            serviceCostTV = itemView.findViewById(R.id.serviceCostTV);
            lblProductCostTV = itemView.findViewById(R.id.lblProductCostTV);
            productCostTV = itemView.findViewById(R.id.productCostTV);
            addServiceIV = itemView.findViewById(R.id.addServiceIV);
            minusServiceIV = itemView.findViewById(R.id.minusServiceIV);
            addProductIV = itemView.findViewById(R.id.addProductIV);
            minusProductIV = itemView.findViewById(R.id.minusProductIV);
            productQtyTV = itemView.findViewById(R.id.productQtyTV);
            serviceQtyTV = itemView.findViewById(R.id.serviceQtyTV);

            addProductIV.setOnClickListener(this);
            minusProductIV.setOnClickListener(this);
            addServiceIV.setOnClickListener(this);
            minusServiceIV.setOnClickListener(this);

            FontUtils.changeFont(currentActivity, productTitleTV, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, totalAmtTV, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, productDescTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, btnRemove, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, durationTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, lblServiceCostTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, serviceCostTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, lblProductCostTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, productCostTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, productQtyTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, serviceQtyTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);

        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;
    }
}

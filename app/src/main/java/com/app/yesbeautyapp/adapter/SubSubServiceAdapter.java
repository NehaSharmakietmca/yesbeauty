package com.app.yesbeautyapp.adapter;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.SubSubServiceModel;
import com.app.yesbeautyapp.model.TestimonialModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SubSubServiceAdapter extends RecyclerView.Adapter<SubSubServiceAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<SubSubServiceModel> subSubServiceModels;
    private RecyclerClickListner recyclerClickListner;
    Bundle bundle;

    public SubSubServiceAdapter(Activity currentActivity, List<SubSubServiceModel> subSubServiceModels) {
        this.currentActivity = currentActivity;
        this.subSubServiceModels = subSubServiceModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.item_sub_sub_service, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (subSubServiceModels != null && subSubServiceModels.size() > position) {
            final SubSubServiceModel subSubServiceModel = subSubServiceModels.get(position);
            if (subSubServiceModel == null) return;
            holder.serviceName.setText(subSubServiceModel.getName());

            if (subSubServiceModel.isSelected()) {
                holder.timeSlotLinearLayout.setBackgroundResource(R.drawable.red_fill_rectangle);
                holder.serviceName.setTextColor(currentActivity.getResources().getColor(R.color.white));
            } else {
                holder.timeSlotLinearLayout.setBackgroundResource(R.drawable.white_rectangle);
                holder.serviceName.setTextColor(currentActivity.getResources().getColor(R.color.black_9));
            }
        }

    }

    @Override
    public int getItemCount() {
        if (subSubServiceModels == null) return 0;
        return subSubServiceModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView serviceName;
        LinearLayout timeSlotLinearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            serviceName = (TextView) itemView.findViewById(R.id.tv_time_slot);
            timeSlotLinearLayout = (LinearLayout) itemView.findViewById(R.id.lin_time_slot);
            timeSlotLinearLayout.setOnClickListener(this);
            FontUtils.changeFont(currentActivity, serviceName, FONT_OPEN_SANS_REGULAR_TTF);
        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;
    }

}


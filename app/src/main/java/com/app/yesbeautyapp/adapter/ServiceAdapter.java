package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ServiceModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<ServiceModel> serviceModelList;
    private RecyclerClickListner recyclerClickListner;
    private List<ServiceModel> tempServiceModelList = new ArrayList<>();

    Bundle bundle;

    public ServiceAdapter(Activity currentActivity, List<ServiceModel> serviceModelList) {
        this.currentActivity = currentActivity;
        this.serviceModelList = serviceModelList;
        tempServiceModelList.addAll(serviceModelList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.items_service, parent, false);
        int height = parent.getMeasuredHeight() / 2;
        view.setMinimumHeight(height);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (serviceModelList != null && serviceModelList.size() > position) {
            final ServiceModel serviceModel = serviceModelList.get(position);
            if (serviceModel == null) return;
            if (serviceModel.getName() == null) {
                serviceModel.setName("");
            }
            holder.textProductName.setText(serviceModel.getName().trim());
            FontUtils.changeFont(currentActivity, holder.textProductName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            if (serviceModel != null) {
                String imageUrl = AppConstants.BASE_URL_IMAGES + serviceModel.getImage();
                Picasso.with(currentActivity).load(imageUrl).error(R.drawable.imghome2).placeholder(R.drawable.imghome2).into(holder.imageProduct);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (serviceModelList == null) return 0;
        return serviceModelList.size();
    }

    public void filter(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());
        serviceModelList.clear();
        if (newText.length() == 0) {
            serviceModelList.addAll(tempServiceModelList);
        } else {
            for (ServiceModel wp : tempServiceModelList) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(newText)) {
                    serviceModelList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageProduct;
        TextView textProductName;
        View viewVerticalLine;
        View viewHorizintalLine;
        LinearLayout containerProduct;
        CardView productCardView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageProduct = (ImageView) itemView.findViewById(R.id.imageProduct);
            textProductName = (TextView) itemView.findViewById(R.id.textProductName);
            containerProduct = (LinearLayout) itemView.findViewById(R.id.containerProduct);
            productCardView = (CardView) itemView.findViewById(R.id.product_card);
            productCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;

    }

    public void updateAdapter(List<ServiceModel> arrayList) {
        serviceModelList = arrayList;
        tempServiceModelList.addAll(serviceModelList);
        notifyDataSetChanged();
    }

    private void setImage(String imageName, ImageView imageProduct) {
        switch (imageName) {
            case "AC":
                imageProduct.setImageDrawable(currentActivity.getResources().getDrawable(R.drawable.ac_icon));
                break;
            case "CA":
                imageProduct.setImageDrawable(currentActivity.getResources().getDrawable(R.drawable.carpenter_icon));
                break;
            case "EC":
                imageProduct.setImageDrawable(currentActivity.getResources().getDrawable(R.drawable.electrician_icon));
                break;
            case "PL":
                imageProduct.setImageDrawable(currentActivity.getResources().getDrawable(R.drawable.plumber_icon));
                break;
            case "HC":
                imageProduct.setImageDrawable(currentActivity.getResources().getDrawable(R.drawable.house_cleaning_icon));
                break;
        }

    }
}

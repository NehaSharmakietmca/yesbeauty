package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.SubServiceActivity;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ProductModel;
import com.app.yesbeautyapp.utils.FontUtils;

import java.util.List;

public class SearchServiceAdapter extends RecyclerView.Adapter<SearchServiceAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<ProductModel> productModelList;
    private RecyclerClickListner recyclerClickListner;

    Bundle bundle;

    public SearchServiceAdapter(Activity currentActivity, List<ProductModel> productModelList) {
        this.currentActivity = currentActivity;
        this.productModelList = productModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.item_searchservice, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int pos = position;
        if (productModelList != null && productModelList.size() > position) {
            final ProductModel productModel = productModelList.get(position);
            if (productModel == null) return;
            holder.textProductName.setText(productModel.getProductName().trim());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.productDescription.setText(Html.fromHtml(productModel.getDescription().trim(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.productDescription.setText(Html.fromHtml(productModel.getDescription().trim()));
            }

            FontUtils.changeFont(currentActivity, holder.textProductName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, holder.productDescription, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
        }

    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;

    }

    @Override
    public int getItemCount() {
        if (productModelList == null) return 0;
        return productModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout linProduct;
        TextView textProductName;
        TextView productDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            linProduct = itemView.findViewById(R.id.lin_product);
            textProductName = itemView.findViewById(R.id.tv_product_name);
            productDescription = itemView.findViewById(R.id.tv_product_description);
            linProduct.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }
}
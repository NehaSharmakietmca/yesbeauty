package com.app.yesbeautyapp.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.Dashboard;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.utils.FontUtils;
import com.app.yesbeautyapp.utils.LoginUtils;


public class SideMenuListAdapter extends RecyclerView.Adapter<SideMenuListAdapter.ViewHolder> {

    String[] sideMenuArray;
    int[] sideMenuImages = {R.drawable.iconhome, R.drawable.iconbooking, R.drawable.iconprofile, R.drawable.iconwallet, R.drawable.about_us_icon, R.drawable.contact_us_icon, R.drawable.terms_icon, R.drawable.privacy_icon, R.mipmap.ic_faq, R.drawable.log_out_icon};
    Activity currentActivty;

    public SideMenuListAdapter(Activity currentActivty) {
        this.currentActivty = currentActivty;
        sideMenuArray = currentActivty.getResources().getStringArray(R.array.sideBarItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivty).inflate(R.layout.items_side_menu, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int pos = position;
        holder.textSideBarItem.setText(sideMenuArray[pos]);
        holder.imageSideBarItem.setImageDrawable(currentActivty.getResources().getDrawable(sideMenuImages[pos]));
        holder.itemLL.setSelected(holder.itemLL.isSelected() ? true : false);
        FontUtils.changeFont(currentActivty, holder.textSideBarItem, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);

        if (sideMenuArray[pos].equals("Logout")) {
            if (!LoginUtils.isLogin(currentActivty)) {
                holder.textSideBarItem.setText("Log In");
                holder.imageSideBarItem.setImageDrawable(currentActivty.getResources().getDrawable(R.drawable.log_in_icon));
            } else {
                holder.textSideBarItem.setText("Log Out");
                holder.imageSideBarItem.setImageDrawable(currentActivty.getResources().getDrawable(R.drawable.log_out_icon));
            }
        }

        if (sideMenuArray[pos].equalsIgnoreCase(currentActivty.getString(R.string.title_wallet))) {
            holder.view.setVisibility(View.VISIBLE);
        } else {
            holder.view.setVisibility(View.GONE);
        }
        holder.containerSideMenuItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) currentActivty).setSelection(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sideMenuImages.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageSideBarItem;
        TextView textSideBarItem;
        LinearLayout containerSideMenuItems;
        LinearLayout itemLL;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);

            imageSideBarItem = (ImageView) itemView.findViewById(R.id.imageSideBarItem);
            itemLL = (LinearLayout) itemView.findViewById(R.id.listView);
            textSideBarItem = (TextView) itemView.findViewById(R.id.textSideBarItem);
            view = itemView.findViewById(R.id.view);
            containerSideMenuItems = (LinearLayout) itemView.findViewById(R.id.containerSideMenuItems);
        }
    }
}
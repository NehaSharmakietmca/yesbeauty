package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.model.NotificationModel;
import com.app.yesbeautyapp.utils.FontUtils;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<NotificationModel> notificationList;


    public NotificationAdapter(Activity currentActivity, List<NotificationModel> notificationList) {
        this.currentActivity = currentActivity;
        this.notificationList = notificationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.items_notifications, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (notificationList != null && notificationList.size() > position) {
            final NotificationModel notificationModel = notificationList.get(position);
            if (notificationModel == null) return;
            holder.titleTV.setText(notificationModel.getTitle());
            holder.messageTV.setText(notificationModel.getMessage());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.messageTV.setText(Html.fromHtml(notificationModel.getMessage().trim(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.messageTV.setText(Html.fromHtml(notificationModel.getMessage().trim()));
            }
        }

    }

    @Override
    public int getItemCount() {
        if (notificationList == null) return 0;
        return notificationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView messageTV;
        TextView titleTV;

        public ViewHolder(View itemView) {
            super(itemView);
            messageTV = itemView.findViewById(R.id.messageTV);
            titleTV = itemView.findViewById(R.id.titleTV);

            FontUtils.changeFont(currentActivity, messageTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, titleTV, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
        }
    }
}

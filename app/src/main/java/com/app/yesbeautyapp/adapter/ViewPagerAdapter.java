package com.app.yesbeautyapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.yesbeautyapp.fragment.SubServiceFragment;
import com.app.yesbeautyapp.model.SubServiceModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private Map<Integer, Fragment> fragmentMap;
    private List<SubServiceModel> serviceModelList;

    public ViewPagerAdapter(FragmentManager fm, List<SubServiceModel> serviceModelList) {
        super(fm);
        this.serviceModelList = serviceModelList;
        fragmentMap = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = SubServiceFragment.newInstance(position, serviceModelList.get(position));
        fragmentMap.put(position, frag);
        return frag;
    }

    @Override
    public int getCount() {
        return serviceModelList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return serviceModelList.get(position).getName();
    }

    public Fragment getFragment(int pos) {
        return fragmentMap.get(pos);
    }
}

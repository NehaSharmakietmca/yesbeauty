package com.app.yesbeautyapp.adapter;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.DateModel;
import com.app.yesbeautyapp.model.TestimonialModel;
import com.app.yesbeautyapp.utils.FontUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<DateModel> dateList;
    private RecyclerClickListner recyclerClickListner;
    private static int sSelected = 0;

    public DateAdapter(Activity currentActivity, List<DateModel> dateList) {
        this.currentActivity = currentActivity;
        this.dateList = dateList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.itemsdate, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (dateList != null && dateList.size() > position) {
            DateModel dateModel = dateList.get(position);
            if (dateModel == null) return;
            if (sSelected == position) {
                holder.dateTV.setBackgroundResource(R.drawable.round_red_fill);
                holder.dateTV.setTextColor(currentActivity.getResources().getColor(R.color.white));
            } else {
                holder.dateTV.setBackgroundColor(currentActivity.getResources().getColor(android.R.color.transparent));
                holder.dateTV.setTextColor(currentActivity.getResources().getColor(R.color.black_9));
            }
            holder.dayTV.setText(dateModel.getDay());
            holder.dateTV.setText(dateModel.getDate());
            holder.monthTV.setText(dateModel.getMonth());

        }
    }

    @Override
    public int getItemCount() {
        if (dateList == null) return 0;
        return dateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView dayTV;
        TextView dateTV;
        TextView monthTV;

        public ViewHolder(View itemView) {
            super(itemView);
            dayTV = (TextView) itemView.findViewById(R.id.dayTV);
            dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            monthTV = (TextView) itemView.findViewById(R.id.monthTV);
            dateTV.setOnClickListener(this);
            monthTV.setOnClickListener(this);
            dayTV.setOnClickListener(this);

            FontUtils.changeFont(currentActivity, dateTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, dayTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, monthTV, FONT_OPEN_SANS_SEMIBOLD_TTF);
        }

        @Override
        public void onClick(View v) {
            sSelected = getAdapterPosition();
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;


    }
    public void selectedItem() {
        notifyDataSetChanged();
    }

}


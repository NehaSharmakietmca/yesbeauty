package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.ServiceModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class SubServiceAdapter extends RecyclerView.Adapter<SubServiceAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<ServiceModel> serviceModelList;
    private RecyclerClickListner recyclerClickListner;

    Bundle bundle;

    public SubServiceAdapter(Activity currentActivity, List<ServiceModel> serviceModelList) {
        this.currentActivity = currentActivity;
        this.serviceModelList = serviceModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.items_sub_service, parent, false);
        int height = parent.getMeasuredHeight() / 3;
        view.setMinimumHeight(height);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int pos = position;
        if (serviceModelList != null && serviceModelList.size() > position) {
            final ServiceModel serviceModel = serviceModelList.get(position);
            if (serviceModel == null) return;
            holder.textProductName.setText(serviceModel.getName());
            FontUtils.changeFont(currentActivity, holder.textProductName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            if (serviceModel != null) {
                String imageUrl = AppConstants.BASE_URL_IMAGES + serviceModel.getImage();
                Picasso.with(currentActivity).load(imageUrl).error(R.drawable.imghome5).placeholder(R.drawable.imghome5).into(holder.imageSubCategory);
            }
        }


    }
    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner){
        this.recyclerClickListner = recyclerClickListner;

    }

    @Override
    public int getItemCount() {
        if (serviceModelList == null) return 0;
        return serviceModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        ImageView imageSubCategory;
        TextView textProductName;
        View viewVerticalLine;
        View viewHorizintalLine;
        LinearLayout containerProduct;
        CardView mainCardView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageSubCategory = (ImageView) itemView.findViewById(R.id.imageSubCategory);
            mainCardView = (CardView) itemView.findViewById(R.id.product_card);
            textProductName = (TextView) itemView.findViewById(R.id.textProductName);
            containerProduct = (LinearLayout) itemView.findViewById(R.id.containerProduct);
            mainCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(),v);
        }
    }


}

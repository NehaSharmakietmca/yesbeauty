package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.utils.DateTimeUtils;
import com.app.yesbeautyapp.utils.FontUtils;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.ViewHolder> implements AppConstants {

    Activity currentActivity;
    List<BookingModel> bookingModelList;
    private RecyclerClickListner recyclerClickListner;

    Bundle bundle;

    public BookingAdapter(Activity currentActivity, List<BookingModel> bookingModelList) {
        this.currentActivity = currentActivity;
        this.bookingModelList = bookingModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.row_item_booking_history, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (bookingModelList != null && bookingModelList.size() > position) {
            final BookingModel bookingModel = bookingModelList.get(position);
            if (bookingModel == null) return;
            holder.bookingIdValue.setText("#"+bookingModel.getBookingId());
            holder.bookingDateValue.setText(String.valueOf(DateTimeUtils.getDateByTimestamp(bookingModel.getTimestamp())));
            String totalAmt = currentActivity.getString(R.string.rupees_symbol) + " " + String.format("%.1f", Double.parseDouble(bookingModel.getTotalAmount())) + "/-";
            holder.bookingTotalAmountValue.setText(totalAmt);
            holder.paymentTypeValue.setText(bookingModel.getPaymentType());
            holder.discountAmountTextView.setText(bookingModel.getDiscountAmount());
            holder.payableAmountTextView.setText(currentActivity.getString(R.string.rupees_symbol)+" " + String.valueOf(Double.parseDouble(bookingModel.getTotalAmount()) - Double.parseDouble(bookingModel.getDiscountAmount())) + "/-");
            //  String imageUrl = AppConstants.BASE_URL_IMAGES + cartModel.getServiceImage();
            //  Picasso.with(currentActivity).load(imageUrl).into(holder.imageProduct);


        }

    }

    @Override
    public int getItemCount() {
        if (bookingModelList == null) return 0;
        return bookingModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView bookingIdLabel;
        TextView bookingIdValue;
        TextView bookingDateLabel;
        TextView bookingDateValue;
        TextView bookingTotalAmountLabel;
        TextView bookingTotalAmountValue;
        TextView paymentTypeLabel;
        TextView paymentTypeValue;
        TextView discountLabelTextView;
        TextView discountAmountTextView;
        TextView payableLabelTextView;
        TextView payableAmountTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            bookingIdLabel = itemView.findViewById(R.id.text_booking_history_id_label);
            bookingIdValue = itemView.findViewById(R.id.text_booking_history_id_value);
            bookingDateLabel = itemView.findViewById(R.id.text_booking_history_date_label);
            bookingDateValue = itemView.findViewById(R.id.text_booking_history_date_value);
            bookingTotalAmountLabel = itemView.findViewById(R.id.text_booking_history_total_amount_label);
            bookingTotalAmountValue = itemView.findViewById(R.id.text_booking_history_total_amount_value);
            paymentTypeLabel = itemView.findViewById(R.id.text_booking_history_payment_type_label);
            paymentTypeValue = itemView.findViewById(R.id.text_booking_history_payment_type_value);
            discountLabelTextView = itemView.findViewById(R.id.text_booking_history_discount_amount_label);
            discountAmountTextView = itemView.findViewById(R.id.text_booking_history_discount_amount_value);
            payableLabelTextView = itemView.findViewById(R.id.text_booking_history_payable_amount_label);
            payableAmountTextView = itemView.findViewById(R.id.text_booking_history_payable_amount_value);
            FontUtils.changeFont(currentActivity, bookingIdLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, bookingIdValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, bookingDateLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, bookingDateValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, bookingTotalAmountLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, bookingTotalAmountValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, paymentTypeLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, paymentTypeValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, discountLabelTextView, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, discountAmountTextView, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(currentActivity, payableLabelTextView, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, payableAmountTextView, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;

    }
}



package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.BookingModel;
import com.app.yesbeautyapp.model.OrderModel;
import com.app.yesbeautyapp.utils.DateTimeUtils;
import com.app.yesbeautyapp.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

public class AppointmentHistoryAdapter extends RecyclerView.Adapter<AppointmentHistoryAdapter.ViewHolder> implements AppConstants {

    Context context;
    ArrayList<OrderModel> orderModelList;
    private RecyclerClickListner recyclerClickListner;
    Bundle bundle;

    public AppointmentHistoryAdapter(Context context, ArrayList<OrderModel> bookingModelList) {
        this.context = context;
        this.orderModelList = bookingModelList;
    }

    @Override
    public AppointmentHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_appointment_history, parent, false);
        AppointmentHistoryAdapter.ViewHolder holder = new AppointmentHistoryAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(AppointmentHistoryAdapter.ViewHolder holder, int position) {

        if (orderModelList != null && orderModelList.size() > position) {
            final OrderModel orderModel = orderModelList.get(position);
            if (orderModel == null) return;
            holder.orderDateValue.setText(DateTimeUtils.getDateInFormat(orderModel.getOrderDate()));
            String orderId = orderModel.getOrderId();
            if (orderId.length() == 1) {
                orderId = "000" + orderId;
            } else if (orderId.length() == 2) {
                orderId = "00" + orderId;
            } else if (orderId.length() == 3) {
                orderId = "0" + orderId;
            }
            holder.orderIdValue.setText(orderId);
            holder.productName.setText(orderModel.getProductName());
            holder.bookingDateValue.setText(DateTimeUtils.getDateInFormat(orderModel.getBookingDate()));
            holder.bookingTimeValue.setText(orderModel.getBookingTime());
            String totalAmt = context.getString(R.string.rupees_symbol) + " " + String.format("%.1f", Double.parseDouble(orderModel.getOrderAmount())) + "/-";
            holder.totalAmountValue.setText(totalAmt);
            holder.paymentModeValue.setText(orderModel.getPaymentMode());
            String status = orderModel.getOrderStatus();
            if (status != null) {
                if (status.equalsIgnoreCase("0")) {
                    holder.orderStatusValue.setText("Pending");
                } else if (status.equalsIgnoreCase("1")) {
                    holder.orderStatusValue.setText("Progress");
                } else if (status.equalsIgnoreCase("2")) {
                    holder.orderStatusValue.setText("Completed");
                } else if (status.equalsIgnoreCase("3")) {
                    holder.orderStatusValue.setText("Cancelled");
                }else {
                    holder.orderStatusValue.setText("Pending");
                }
                holder.cancelTextView.setText(holder.orderStatusValue.getText());
            }

            holder.serviceTypeValue.setText(orderModel.getServiceType());
            //  String imageUrl = AppConstants.BASE_URL_IMAGES + cartModel.getServiceImage();
            //  Picasso.with(currentActivity).load(imageUrl).into(holder.imageProduct);


        }

    }

    @Override
    public int getItemCount() {
        if (orderModelList == null) return 0;
        return orderModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView orderDateLabel;
        TextView orderDateValue;
        TextView orderIdLabel;
        TextView orderIdValue;
        TextView productName;
        TextView bookingDateLabel;
        TextView bookingDateValue;
        TextView bookingTimeLabel;
        TextView bookingTimeValue;
        TextView paymentModeLabel;
        TextView paymentModeValue;
        TextView orderStatusLabel;
        TextView orderStatusValue;
        TextView serviceTypeLabel;
        TextView serviceTypeValue;
        TextView totalAmountValue;
        TextView cancelTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            orderDateLabel = itemView.findViewById(R.id.text_order_date_label);
            orderDateValue = itemView.findViewById(R.id.text_order_date_value);
            orderIdLabel = itemView.findViewById(R.id.text_order_id_label);
            orderIdValue = itemView.findViewById(R.id.text_order_id_value);
            productName = itemView.findViewById(R.id.text_product_description);
            bookingDateLabel = itemView.findViewById(R.id.text_booking_date_label);
            bookingDateValue = itemView.findViewById(R.id.text_booking_date_value);
            bookingTimeLabel = itemView.findViewById(R.id.text_booking_time_label);
            bookingTimeValue = itemView.findViewById(R.id.text_booking_time_value);
            paymentModeLabel = itemView.findViewById(R.id.text_payment_mode_label);
            paymentModeValue = itemView.findViewById(R.id.text_payment_mode_value);
            orderStatusLabel = itemView.findViewById(R.id.text_order_status_label);
            orderStatusValue = itemView.findViewById(R.id.text_order_status_value);
            serviceTypeLabel = itemView.findViewById(R.id.text_service_type_label);
            serviceTypeValue = itemView.findViewById(R.id.text_service_type_value);
            totalAmountValue = itemView.findViewById(R.id.text_order_amount);
            cancelTextView = itemView.findViewById(R.id.text_order_cancel);

            FontUtils.changeFont(context, orderDateLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, orderDateValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, orderIdLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, orderIdValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, productName, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, bookingDateLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, bookingDateValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, bookingTimeLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, bookingTimeValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, paymentModeLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, paymentModeValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, orderStatusLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, orderStatusValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, serviceTypeLabel, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(context, serviceTypeValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, totalAmountValue, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            FontUtils.changeFont(context, cancelTextView, AppConstants.FONT_OPEN_SANS_SEMIBOLD_TTF);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner) {
        this.recyclerClickListner = recyclerClickListner;

    }
}
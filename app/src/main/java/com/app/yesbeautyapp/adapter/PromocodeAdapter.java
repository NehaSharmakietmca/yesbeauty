package com.app.yesbeautyapp.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.interfaces.RecyclerClickListner;
import com.app.yesbeautyapp.model.PromocodeModel;
import com.app.yesbeautyapp.utils.FontUtils;

public class PromocodeAdapter extends RecyclerView.Adapter<PromocodeAdapter.ViewHolder> implements AppConstants{

    Activity currentActivity;
    List<PromocodeModel> promocodeModelList;
    private RecyclerClickListner recyclerClickListner;
    private List<PromocodeModel> promocodeModelArrayList = new ArrayList<>();

    Bundle bundle;

    public PromocodeAdapter(Activity currentActivity, List<PromocodeModel> serviceModelList) {
        this.currentActivity = currentActivity;
        this.promocodeModelList = serviceModelList;
        promocodeModelArrayList.addAll(serviceModelList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(currentActivity).inflate(R.layout.item_promocode, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (promocodeModelList != null && promocodeModelList.size() > position) {
            final PromocodeModel promocodeModel = promocodeModelList.get(position);
            if (promocodeModel == null) return;
            holder.promocodeNameTextView.setText(promocodeModel.getPromocode());
            holder.promocodeDescriptionTextView.setText(promocodeModel.getDescription());
            FontUtils.changeFont(currentActivity, holder.promocodeDescriptionTextView, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
            FontUtils.changeFont(currentActivity, holder.promocodeApplyTextView, AppConstants.FONT_OPEN_SANS_REGULAR_TTF);
        }

    }

    @Override
    public int getItemCount() {
        if (promocodeModelList == null) return 0;
        return promocodeModelList.size();
    }

    public void filter(String newText) {
        newText = newText.toLowerCase(Locale.getDefault());
        promocodeModelList.clear();
        if (newText.length() == 0) {
            promocodeModelList.addAll(promocodeModelArrayList);
        } else {
            for (PromocodeModel wp : promocodeModelArrayList) {
                if (wp.getPromocode().toLowerCase(Locale.getDefault()).contains(newText)) {
                    promocodeModelList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView promocodeNameTextView;
        TextView promocodeDescriptionTextView;
        TextView promocodeApplyTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            promocodeNameTextView = (TextView) itemView.findViewById(R.id.promocodeNameTV);
            promocodeDescriptionTextView = (TextView) itemView.findViewById(R.id.promocodeDescriptionTV);
            promocodeApplyTextView = (TextView) itemView.findViewById(R.id.applyTV);
            promocodeApplyTextView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerClickListner.onItemClick(getAdapterPosition(),v);
        }
    }
    public void setOnItemClickListner(RecyclerClickListner recyclerClickListner){
        this.recyclerClickListner = recyclerClickListner;

    }
    public void updateAdapter(List<PromocodeModel> arrayList){
       // promocodeModelList = arrayList;
        promocodeModelArrayList.clear();
        notifyDataSetChanged();
        promocodeModelArrayList.addAll(promocodeModelList);
        notifyDataSetChanged();
    }

}

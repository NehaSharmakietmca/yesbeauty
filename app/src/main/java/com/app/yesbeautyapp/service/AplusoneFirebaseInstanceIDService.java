package com.app.yesbeautyapp.service;

import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.utils.SharedPreferenceUtils;

import static com.app.yesbeautyapp.constants.AppConstants.FCM_TOKEN;


/**
 * Created by admin on 15-11-2016.
 */
public class AplusoneFirebaseInstanceIDService extends FirebaseInstanceIdService implements AppConstants {
    private static final String TAG = AplusoneFirebaseInstanceIDService.class.getSimpleName();


    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        FirebaseApp.initializeApp(this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("refreshedToken = ", refreshedToken);
        FirebaseMessaging.getInstance().subscribeToTopic("Aplusone");
        SharedPreferenceUtils.getInstance(getApplicationContext()).putString(FCM_TOKEN, refreshedToken);
    }
}

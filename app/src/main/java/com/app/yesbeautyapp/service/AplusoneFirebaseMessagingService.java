package com.app.yesbeautyapp.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Map;

import com.app.yesbeautyapp.R;
import com.app.yesbeautyapp.activity.SplashActivity;
import com.app.yesbeautyapp.constants.AppConstants;
import com.app.yesbeautyapp.model.NotificationModel;

import static android.media.RingtoneManager.getDefaultUri;


/**
 * Created by admin on 1-2-2017.
 */
public class AplusoneFirebaseMessagingService extends FirebaseMessagingService implements AppConstants {

    private static final String TAG = "FirebaseMessagService";
    private String channelId ="Default";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
       // Log.d(TAG, "onMessageReceived() enter");
        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "DataPayload: " + remoteMessage.getData().toString());
            try {

                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                NotificationModel notificationModel = gson.fromJson(object.toString(), NotificationModel.class);
                handleDataMessage(notificationModel);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleDataMessage(NotificationModel notificationModel) {
        Context context = getApplicationContext();
        if (context == null || notificationModel == null) {
            return;
        }
        try {
            String title = notificationModel.getTitle();
            String message = notificationModel.getMessage();
            String type = notificationModel.getType();
            long timestamp = notificationModel.getTimestamp();

            final int icon = R.mipmap.ic_launcher;
            Intent intent = new Intent(context, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            final PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

            Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.bigText(message);
            bigTextStyle.setBigContentTitle(title);

            Notification notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                    .setStyle(bigTextStyle)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(resultPendingIntent)
                    .setSound(alarmSound)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setWhen(timestamp)
                    .setSmallIcon(getNotificationIcon())
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setColor(ContextCompat.getColor(context,R.color.colorPrimary))
                    .setContentText(message)
                    .setChannelId(channelId)
                    .build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
         //   NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            if (notificationManager != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);
                }
                notificationManager.notify((int) System.currentTimeMillis(), notification);
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_launcher : R.mipmap.ic_launcher;
    }
}
